USE [accionistas]
GO
/****** Object:  User [marcelo]    Script Date: 11/24/2017 14:57:31 ******/
CREATE USER [marcelo] FOR LOGIN [marcelo] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Schema [accionistas]    Script Date: 11/24/2017 14:57:30 ******/
CREATE SCHEMA [accionistas] AUTHORIZATION [dbo]
GO
/****** Object:  Table [dbo].[Tipogirocomercial]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipogirocomercial](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tipoaccionista]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipoaccionista](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion_tipo_accionista] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Testigo]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Testigo](
	[rut] [varchar](12) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido_paterno] [varchar](50) NOT NULL,
	[apellido_materno] [varchar](50) NULL,
	[mail] [varchar](50) NOT NULL,
	[fono] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[rut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Representantelegal]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Representantelegal](
	[rut] [varchar](12) NOT NULL,
	[nombre] [varchar](20) NOT NULL,
	[apellido_paterno] [varchar](20) NOT NULL,
	[apellido_materno] [varchar](20) NULL,
	[direccion] [varchar](100) NOT NULL,
	[mail] [varchar](100) NOT NULL,
	[telefono] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[rut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Nacionalidad]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Nacionalidad](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nacionalidad] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Juntaaccionista]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Juntaaccionista](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ubicacion] [varchar](100) NOT NULL,
	[fecha_realizacion] [date] NOT NULL,
	[hora_inicio] [time](0) NULL,
	[hora_termino] [time](0) NULL,
	[direccion] [varchar](100) NOT NULL,
	[fecha_difusion] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](15) NOT NULL,
	[password] [varchar](15) NOT NULL,
	[nivel] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[users]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[users](
	[username] [varchar](45) NOT NULL,
	[enabled] [bit] NOT NULL,
	[password] [varchar](60) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Votacionjunta]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Votacionjunta](
	[id_votacion] [int] IDENTITY(1,1) NOT NULL,
	[juntaAccionistaid_junta] [int] NOT NULL,
	[cantidad_votantes] [int] NOT NULL,
	[votos_positivos] [int] NOT NULL,
	[votos_negativos] [int] NOT NULL,
	[observacion] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_votacion] ASC,
	[juntaAccionistaid_junta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user_roles]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_roles](
	[user_role_id] [int] IDENTITY(1,1) NOT NULL,
	[role] [varchar](45) NOT NULL,
	[username] [varchar](45) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[user_role_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UKstlxfukw77ov5w1wo1tm3omca] UNIQUE NONCLUSTERED 
(
	[role] ASC,
	[username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Empresa]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empresa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rut] [varchar](12) NOT NULL,
	[razon_social] [varchar](70) NOT NULL,
	[logo] [varchar](255) NULL,
	[web] [varchar](100) NOT NULL,
	[cantidad_acciones_seriea] [int] NOT NULL,
	[cantidad_acciones_serieb] [int] NOT NULL,
	[cantidad_accionistas] [int] NOT NULL,
	[mail] [varchar](100) NOT NULL,
	[direccion] [varchar](100) NOT NULL,
	[telefono] [varchar](20) NULL,
	[representante_legalrut] [varchar](12) NOT NULL,
	[tipo_giro_comercialid] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [representante_legalrut] UNIQUE NONCLUSTERED 
(
	[representante_legalrut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [tipo_giro_comercialid] UNIQUE NONCLUSTERED 
(
	[tipo_giro_comercialid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Acta]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Acta](
	[id_acta] [int] IDENTITY(1,1) NOT NULL,
	[juntaAccionistaid_junta] [int] NOT NULL,
	[titulo_acta] [varchar](255) NOT NULL,
	[detalle_acta] [varchar](255) NOT NULL,
	[url_documento] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_acta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Accion]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[valora] [real] NOT NULL,
	[valorb] [real] NULL,
	[empresaid_empresa] [int] NOT NULL,
	[estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Accionista]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Accionista](
	[rut] [varchar](12) NOT NULL,
	[empresaid_empresa] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido_paterno] [varchar](50) NOT NULL,
	[apellido_materno] [varchar](50) NULL,
	[cantidad_acciones_serie_a] [int] NOT NULL,
	[cantidad_acciones_serie_b] [int] NOT NULL,
	[porcentaje_participacion] [real] NOT NULL,
	[fecha_nacimiento] [date] NOT NULL,
	[es_valido] [varchar](10) NOT NULL,
	[mail] [varchar](150) NOT NULL,
	[direccion] [varchar](100) NULL,
	[fono] [varchar](20) NULL,
	[nacionalidadid] [int] NOT NULL,
	[tipo_accionistaid] [int] NOT NULL,
	[ciudad] [varchar](100) NULL,
	[comuna] [varchar](100) NULL,
	[estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[rut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Traspasoacciones]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Traspasoacciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rut_accionista_vendedor] [varchar](12) NOT NULL,
	[rut_accionista_comprador] [varchar](12) NOT NULL,
	[fecha_venta] [date] NOT NULL,
	[cantidad_acciones_vendidas_a] [int] NOT NULL,
	[cantidad_acciones_vendidas_b] [int] NOT NULL,
	[monto] [real] NOT NULL,
	[es_sucesion] [varchar](10) NOT NULL,
	[testigorut] [varchar](12) NULL,
	[observacion] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dividendo]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dividendo](
	[accionistarut] [varchar](12) NOT NULL,
	[fecha_declaracion] [date] NOT NULL,
	[fecha_registro] [date] NOT NULL,
	[dividendo_por_accion_a] [real] NOT NULL,
	[dividendo_por_accion_b] [real] NOT NULL,
	[dividendo_total_a] [real] NULL,
	[dividendo_total_b] [real] NULL,
	[estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[accionistarut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cuentacorriente]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cuentacorriente](
	[numero_cuenta] [varchar](20) NOT NULL,
	[banco] [varchar](30) NOT NULL,
	[accionistarut] [varchar](12) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[numero_cuenta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Apoderado]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Apoderado](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rut] [varchar](12) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido_paterno] [varchar](50) NOT NULL,
	[apellido_materno] [varchar](50) NULL,
	[mail] [varchar](100) NOT NULL,
	[fono] [varchar](20) NULL,
	[accionistarut] [varchar](12) NOT NULL,
	[estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Accionesconprohibicion]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Accionesconprohibicion](
	[accionistarut] [varchar](12) NOT NULL,
	[cantidad_acciones_prohibicion] [int] NOT NULL,
	[cantidad_acciones_prohibicion_b] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[accionistarut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Heredero]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Heredero](
	[rut] [varchar](12) NOT NULL,
	[Empresaid_empresa] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido_paterno] [varchar](50) NOT NULL,
	[apellido_materno] [varchar](50) NULL,
	[cantidad_acciones_serie_a] [int] NULL,
	[cantidad_acciones_serie_b] [int] NULL,
	[porcentaje_participacion] [real] NULL,
	[fecha_nacimiento] [date] NOT NULL,
	[es_valido] [varchar](10) NOT NULL,
	[mail] [varchar](150) NOT NULL,
	[direccion] [varchar](100) NULL,
	[fono] [varchar](20) NULL,
	[ciudad] [varchar](100) NOT NULL,
	[comuna] [varchar](100) NOT NULL,
	[nacionalidad] [int] NOT NULL,
	[tipo_accionista] [int] NOT NULL,
	[accionistarut] [varchar](12) NOT NULL,
	[estado] [int] NULL,
 CONSTRAINT [PK_Heredero] PRIMARY KEY CLUSTERED 
(
	[rut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Accionista_juntaaccionista]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Accionista_juntaaccionista](
	[accionistarut] [varchar](12) NOT NULL,
	[junta_accionista_junta] [int] NOT NULL,
	[asistencia_accionista] [varchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[accionistarut] ASC,
	[junta_accionista_junta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Historialtitulo]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Historialtitulo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[accionistarut] [varchar](12) NOT NULL,
	[detalle_solicitud] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mandatario]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mandatario](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rut_mandatario] [varchar](12) NOT NULL,
	[razon_social] [varchar](150) NULL,
	[fecha_nacimiento] [date] NOT NULL,
	[fecha_ingreso] [date] NOT NULL,
	[fecha_salida] [date] NULL,
	[cantidad_acciones] [int] NOT NULL,
	[porcentaje_participacion] [real] NOT NULL,
	[accionistarut] [varchar](12) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Juntaaccionista_apoderado]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Juntaaccionista_apoderado](
	[juntaAccionistaid_junta] [int] NOT NULL,
	[apoderadoid] [int] NOT NULL,
	[asistencia_apoderado] [varchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[juntaAccionistaid_junta] ASC,
	[apoderadoid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Historialdividendo]    Script Date: 11/24/2017 14:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Historialdividendo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dividendoAccionistarut] [varchar](12) NOT NULL,
	[fecha_delaracion] [timestamp] NOT NULL,
	[fecha_registro] [datetime] NOT NULL,
	[dividendo_por_accion] [real] NOT NULL,
	[dividendo_total] [real] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[dividendoAccionistarut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_empresaid_empresa_id]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Accion]  WITH CHECK ADD  CONSTRAINT [FK_empresaid_empresa_id] FOREIGN KEY([empresaid_empresa])
REFERENCES [dbo].[Empresa] ([id])
GO
ALTER TABLE [dbo].[Accion] CHECK CONSTRAINT [FK_empresaid_empresa_id]
GO
/****** Object:  ForeignKey [FKaccionesCo852416]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Accionesconprohibicion]  WITH CHECK ADD  CONSTRAINT [FKaccionesCo852416] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Accionesconprohibicion] CHECK CONSTRAINT [FKaccionesCo852416]
GO
/****** Object:  ForeignKey [FKaccionista93694]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Accionista]  WITH CHECK ADD  CONSTRAINT [FKaccionista93694] FOREIGN KEY([nacionalidadid])
REFERENCES [dbo].[Nacionalidad] ([id])
GO
ALTER TABLE [dbo].[Accionista] CHECK CONSTRAINT [FKaccionista93694]
GO
/****** Object:  ForeignKey [FKaccionista940287]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Accionista]  WITH CHECK ADD  CONSTRAINT [FKaccionista940287] FOREIGN KEY([empresaid_empresa])
REFERENCES [dbo].[Empresa] ([id])
GO
ALTER TABLE [dbo].[Accionista] CHECK CONSTRAINT [FKaccionista940287]
GO
/****** Object:  ForeignKey [FKaccionista99327]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Accionista]  WITH CHECK ADD  CONSTRAINT [FKaccionista99327] FOREIGN KEY([tipo_accionistaid])
REFERENCES [dbo].[Tipoaccionista] ([id])
GO
ALTER TABLE [dbo].[Accionista] CHECK CONSTRAINT [FKaccionista99327]
GO
/****** Object:  ForeignKey [FKaccionista489118]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Accionista_juntaaccionista]  WITH CHECK ADD  CONSTRAINT [FKaccionista489118] FOREIGN KEY([junta_accionista_junta])
REFERENCES [dbo].[Juntaaccionista] ([id])
GO
ALTER TABLE [dbo].[Accionista_juntaaccionista] CHECK CONSTRAINT [FKaccionista489118]
GO
/****** Object:  ForeignKey [FKaccionista879535]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Accionista_juntaaccionista]  WITH CHECK ADD  CONSTRAINT [FKaccionista879535] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Accionista_juntaaccionista] CHECK CONSTRAINT [FKaccionista879535]
GO
/****** Object:  ForeignKey [FKacta278928]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Acta]  WITH CHECK ADD  CONSTRAINT [FKacta278928] FOREIGN KEY([juntaAccionistaid_junta])
REFERENCES [dbo].[Juntaaccionista] ([id])
GO
ALTER TABLE [dbo].[Acta] CHECK CONSTRAINT [FKacta278928]
GO
/****** Object:  ForeignKey [FKapoderado107487]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Apoderado]  WITH CHECK ADD  CONSTRAINT [FKapoderado107487] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Apoderado] CHECK CONSTRAINT [FKapoderado107487]
GO
/****** Object:  ForeignKey [FKcuentaCorr897980]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Cuentacorriente]  WITH CHECK ADD  CONSTRAINT [FKcuentaCorr897980] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Cuentacorriente] CHECK CONSTRAINT [FKcuentaCorr897980]
GO
/****** Object:  ForeignKey [FKdividendo866504]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Dividendo]  WITH CHECK ADD  CONSTRAINT [FKdividendo866504] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Dividendo] CHECK CONSTRAINT [FKdividendo866504]
GO
/****** Object:  ForeignKey [FK_representante_legalrut_rut]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Empresa]  WITH CHECK ADD  CONSTRAINT [FK_representante_legalrut_rut] FOREIGN KEY([representante_legalrut])
REFERENCES [dbo].[Representantelegal] ([rut])
GO
ALTER TABLE [dbo].[Empresa] CHECK CONSTRAINT [FK_representante_legalrut_rut]
GO
/****** Object:  ForeignKey [FK_tipo_giro_comercialid_id]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Empresa]  WITH CHECK ADD  CONSTRAINT [FK_tipo_giro_comercialid_id] FOREIGN KEY([tipo_giro_comercialid])
REFERENCES [dbo].[Tipogirocomercial] ([id])
GO
ALTER TABLE [dbo].[Empresa] CHECK CONSTRAINT [FK_tipo_giro_comercialid_id]
GO
/****** Object:  ForeignKey [FK_Heredero_Accionista]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Heredero]  WITH CHECK ADD  CONSTRAINT [FK_Heredero_Accionista] FOREIGN KEY([rut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Heredero] CHECK CONSTRAINT [FK_Heredero_Accionista]
GO
/****** Object:  ForeignKey [FK_Heredero_Nacionalidad]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Heredero]  WITH CHECK ADD  CONSTRAINT [FK_Heredero_Nacionalidad] FOREIGN KEY([nacionalidad])
REFERENCES [dbo].[Nacionalidad] ([id])
GO
ALTER TABLE [dbo].[Heredero] CHECK CONSTRAINT [FK_Heredero_Nacionalidad]
GO
/****** Object:  ForeignKey [FK_Heredero_Tipoaccionista]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Heredero]  WITH CHECK ADD  CONSTRAINT [FK_Heredero_Tipoaccionista] FOREIGN KEY([tipo_accionista])
REFERENCES [dbo].[Tipoaccionista] ([id])
GO
ALTER TABLE [dbo].[Heredero] CHECK CONSTRAINT [FK_Heredero_Tipoaccionista]
GO
/****** Object:  ForeignKey [FKheredero240757]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Heredero]  WITH CHECK ADD  CONSTRAINT [FKheredero240757] FOREIGN KEY([Empresaid_empresa])
REFERENCES [dbo].[Empresa] ([id])
GO
ALTER TABLE [dbo].[Heredero] CHECK CONSTRAINT [FKheredero240757]
GO
/****** Object:  ForeignKey [FKhistorialD2868]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Historialdividendo]  WITH CHECK ADD  CONSTRAINT [FKhistorialD2868] FOREIGN KEY([dividendoAccionistarut])
REFERENCES [dbo].[Dividendo] ([accionistarut])
GO
ALTER TABLE [dbo].[Historialdividendo] CHECK CONSTRAINT [FKhistorialD2868]
GO
/****** Object:  ForeignKey [FKhistorialT736912]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Historialtitulo]  WITH CHECK ADD  CONSTRAINT [FKhistorialT736912] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Historialtitulo] CHECK CONSTRAINT [FKhistorialT736912]
GO
/****** Object:  ForeignKey [FKjuntaAccio71301]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Juntaaccionista_apoderado]  WITH CHECK ADD  CONSTRAINT [FKjuntaAccio71301] FOREIGN KEY([juntaAccionistaid_junta])
REFERENCES [dbo].[Juntaaccionista] ([id])
GO
ALTER TABLE [dbo].[Juntaaccionista_apoderado] CHECK CONSTRAINT [FKjuntaAccio71301]
GO
/****** Object:  ForeignKey [FKjuntaAccio915362]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Juntaaccionista_apoderado]  WITH CHECK ADD  CONSTRAINT [FKjuntaAccio915362] FOREIGN KEY([apoderadoid])
REFERENCES [dbo].[Apoderado] ([id])
GO
ALTER TABLE [dbo].[Juntaaccionista_apoderado] CHECK CONSTRAINT [FKjuntaAccio915362]
GO
/****** Object:  ForeignKey [FKmandatario944291]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Mandatario]  WITH CHECK ADD  CONSTRAINT [FKmandatario944291] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Mandatario] CHECK CONSTRAINT [FKmandatario944291]
GO
/****** Object:  ForeignKey [FK_Traspasoacciones_Accionista]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Traspasoacciones]  WITH CHECK ADD  CONSTRAINT [FK_Traspasoacciones_Accionista] FOREIGN KEY([rut_accionista_vendedor])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Traspasoacciones] CHECK CONSTRAINT [FK_Traspasoacciones_Accionista]
GO
/****** Object:  ForeignKey [FK_Traspasoacciones_Accionista1]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Traspasoacciones]  WITH CHECK ADD  CONSTRAINT [FK_Traspasoacciones_Accionista1] FOREIGN KEY([rut_accionista_comprador])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Traspasoacciones] CHECK CONSTRAINT [FK_Traspasoacciones_Accionista1]
GO
/****** Object:  ForeignKey [FK_Traspasoacciones_Testigo]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Traspasoacciones]  WITH CHECK ADD  CONSTRAINT [FK_Traspasoacciones_Testigo] FOREIGN KEY([testigorut])
REFERENCES [dbo].[Testigo] ([rut])
GO
ALTER TABLE [dbo].[Traspasoacciones] CHECK CONSTRAINT [FK_Traspasoacciones_Testigo]
GO
/****** Object:  ForeignKey [FKcdp2dxqcsdh6rnh6o64rgtcir]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[user_roles]  WITH CHECK ADD  CONSTRAINT [FKcdp2dxqcsdh6rnh6o64rgtcir] FOREIGN KEY([username])
REFERENCES [dbo].[users] ([username])
GO
ALTER TABLE [dbo].[user_roles] CHECK CONSTRAINT [FKcdp2dxqcsdh6rnh6o64rgtcir]
GO
/****** Object:  ForeignKey [FKvotacionJu814254]    Script Date: 11/24/2017 14:57:31 ******/
ALTER TABLE [dbo].[Votacionjunta]  WITH CHECK ADD  CONSTRAINT [FKvotacionJu814254] FOREIGN KEY([juntaAccionistaid_junta])
REFERENCES [dbo].[Juntaaccionista] ([id])
GO
ALTER TABLE [dbo].[Votacionjunta] CHECK CONSTRAINT [FKvotacionJu814254]
GO
