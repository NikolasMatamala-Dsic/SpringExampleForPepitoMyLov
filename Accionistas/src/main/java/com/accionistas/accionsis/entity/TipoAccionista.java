package com.accionistas.accionsis.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
/**
 * 
 * clase Tipoaccionista
 *
 */

@Entity
@Table(name="Tipoaccionista")
public class TipoAccionista {
	
	@OneToMany
	(mappedBy = "tipoAccionista")
	private Set<Accionista> accionista = new HashSet<>();
	
	@OneToMany
	(mappedBy = "tipoAccionista")
	private Set<Heredero> heredero = new HashSet<>();
	
	@Id
	@GeneratedValue
	@Column(name="idTipoAccionista")
	private int idTipoAccionista;
	
	@Column(name="descripcion_tipo_accionista")
	private String descripcion_tipo_accionista;

	public TipoAccionista() {
		super();
	}

	public TipoAccionista(Set<Accionista> accionista, Set<Heredero> heredero, int idTipoAccionista,
			String descripcion_tipo_accionista) {
		super();
		this.accionista = accionista;
		this.heredero = heredero;
		this.idTipoAccionista = idTipoAccionista;
		this.descripcion_tipo_accionista = descripcion_tipo_accionista;
	}

	@Override
	public String toString() {
		return "TipoAccionista [accionista=" + accionista + ", heredero=" + heredero + ", idTipoAccionista="
				+ idTipoAccionista + ", descripcion_tipo_accionista=" + descripcion_tipo_accionista + "]";
	}

	public Set<Accionista> getAccionista() {
		return accionista;
	}

	public void setAccionista(Set<Accionista> accionista) {
		this.accionista = accionista;
	}

	public Set<Heredero> getHeredero() {
		return heredero;
	}

	public void setHeredero(Set<Heredero> heredero) {
		this.heredero = heredero;
	}

	public int getIdTipoAccionista() {
		return idTipoAccionista;
	}

	public void setIdTipoAccionista(int idTipoAccionista) {
		this.idTipoAccionista = idTipoAccionista;
	}

	public String getDescripcion_tipo_accionista() {
		return descripcion_tipo_accionista;
	}

	public void setDescripcion_tipo_accionista(String descripcion_tipo_accionista) {
		this.descripcion_tipo_accionista = descripcion_tipo_accionista;
	}

	
}
