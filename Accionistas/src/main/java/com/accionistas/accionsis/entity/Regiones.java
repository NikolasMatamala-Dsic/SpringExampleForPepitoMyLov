package com.accionistas.accionsis.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "Regiones")
public class Regiones {
	
	@OneToMany
	(mappedBy = "region")
	private Set<Accionista> accionista = new HashSet<>();
	
	@OneToMany
	(mappedBy = "region")
	private Set<Heredero> heredero = new HashSet<>();
	
	@OneToMany
	(mappedBy = "region")
	private Set<Representante> representante = new HashSet<>();

	@Id
	@GeneratedValue
	@Column(name = "idRegion")
	private int idRegion;
	
	@Column(name = "region")
	private String region;

	public Regiones() {
		super();
	}

	public Regiones(Set<Accionista> accionista, Set<Heredero> heredero, Set<Representante> representante, int idRegion,
			String region) {
		super();
		this.accionista = accionista;
		this.heredero = heredero;
		this.representante = representante;
		this.idRegion = idRegion;
		this.region = region;
	}

	@Override
	public String toString() {
		return "Regiones [accionista=" + accionista + ", heredero=" + heredero + ", representante=" + representante
				+ ", idRegion=" + idRegion + ", region=" + region + "]";
	}

	public Set<Accionista> getAccionista() {
		return accionista;
	}

	public void setAccionista(Set<Accionista> accionista) {
		this.accionista = accionista;
	}

	public Set<Heredero> getHeredero() {
		return heredero;
	}

	public void setHeredero(Set<Heredero> heredero) {
		this.heredero = heredero;
	}

	public Set<Representante> getRepresentante() {
		return representante;
	}

	public void setRepresentante(Set<Representante> representante) {
		this.representante = representante;
	}

	public int getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(int idRegion) {
		this.idRegion = idRegion;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	
	
}
