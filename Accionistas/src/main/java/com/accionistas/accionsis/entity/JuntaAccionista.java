package com.accionistas.accionsis.entity;

import java.sql.Date;
import java.sql.Time;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Juntaaccionista")
public class JuntaAccionista {
	
	@OneToMany
	(mappedBy = "juntaAccionista")
	private Set<AsistenciaJuntaAccionista> asistenciaJunta = new HashSet<>();
	
	@OneToMany
	(mappedBy = "juntaAccionista")
	private Set<Acta> acta = new HashSet<>();
	
	@OneToMany
	(mappedBy = "juntaAccionista")
	private Set<VotacionJunta> votacionJunta = new HashSet<>();
	
	@OneToMany
	(mappedBy = "juntaAccionista")
	private Set<Dividendo> dividendo = new HashSet<>();
	
	@Id
	@GeneratedValue
	@Column(name="idJuntaAccionista")
	private int idJuntaAccionista;
	
	@Column(name="ubicacion")
	private String ubicacion;
	
	@Column(name="fecha_realizacion")
	private Date fecha_realizacion;
	
	@Column(name="hora_inicio")
	private Time hora_inicio;
	
	@Column(name="hora_termino")
	private Time hora_termino;
	
	@Column(name="direccion")
	private String direccion;
	
	@Column(name="fecha_difusion")
	private Date fecha_difusion;
	
	@Column(name = "asistenciaTotal")
	private double asistenciaTotal;
	
	@Column(name="estado")
	private int estado;

	public JuntaAccionista() {
		super();
	}

	public JuntaAccionista(Set<AsistenciaJuntaAccionista> asistenciaJunta, Set<Acta> acta,
			Set<VotacionJunta> votacionJunta, Set<Dividendo> dividendo, int idJuntaAccionista, String ubicacion,
			Date fecha_realizacion, Time hora_inicio, Time hora_termino, String direccion, Date fecha_difusion,
			double asistenciaTotal, int estado) {
		super();
		this.asistenciaJunta = asistenciaJunta;
		this.acta = acta;
		this.votacionJunta = votacionJunta;
		this.dividendo = dividendo;
		this.idJuntaAccionista = idJuntaAccionista;
		this.ubicacion = ubicacion;
		this.fecha_realizacion = fecha_realizacion;
		this.hora_inicio = hora_inicio;
		this.hora_termino = hora_termino;
		this.direccion = direccion;
		this.fecha_difusion = fecha_difusion;
		this.asistenciaTotal = asistenciaTotal;
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "JuntaAccionista [asistenciaJunta=" + asistenciaJunta + ", acta=" + acta + ", votacionJunta="
				+ votacionJunta + ", dividendo=" + dividendo + ", idJuntaAccionista=" + idJuntaAccionista
				+ ", ubicacion=" + ubicacion + ", fecha_realizacion=" + fecha_realizacion + ", hora_inicio="
				+ hora_inicio + ", hora_termino=" + hora_termino + ", direccion=" + direccion + ", fecha_difusion="
				+ fecha_difusion + ", asistenciaTotal=" + asistenciaTotal + ", estado=" + estado + "]";
	}

	public Set<AsistenciaJuntaAccionista> getAsistenciaJunta() {
		return asistenciaJunta;
	}

	public void setAsistenciaJunta(Set<AsistenciaJuntaAccionista> asistenciaJunta) {
		this.asistenciaJunta = asistenciaJunta;
	}

	public Set<Acta> getActa() {
		return acta;
	}

	public void setActa(Set<Acta> acta) {
		this.acta = acta;
	}

	public Set<VotacionJunta> getVotacionJunta() {
		return votacionJunta;
	}

	public void setVotacionJunta(Set<VotacionJunta> votacionJunta) {
		this.votacionJunta = votacionJunta;
	}

	public Set<Dividendo> getDividendo() {
		return dividendo;
	}

	public void setDividendo(Set<Dividendo> dividendo) {
		this.dividendo = dividendo;
	}

	public int getIdJuntaAccionista() {
		return idJuntaAccionista;
	}

	public void setIdJuntaAccionista(int idJuntaAccionista) {
		this.idJuntaAccionista = idJuntaAccionista;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Date getFecha_realizacion() {
		return fecha_realizacion;
	}

	public void setFecha_realizacion(Date fecha_realizacion) {
		this.fecha_realizacion = fecha_realizacion;
	}

	public Time getHora_inicio() {
		return hora_inicio;
	}

	public void setHora_inicio(Time hora_inicio) {
		this.hora_inicio = hora_inicio;
	}

	public Time getHora_termino() {
		return hora_termino;
	}

	public void setHora_termino(Time hora_termino) {
		this.hora_termino = hora_termino;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFecha_difusion() {
		return fecha_difusion;
	}

	public void setFecha_difusion(Date fecha_difusion) {
		this.fecha_difusion = fecha_difusion;
	}

	public double getAsistenciaTotal() {
		return asistenciaTotal;
	}

	public void setAsistenciaTotal(double asistenciaTotal) {
		this.asistenciaTotal = asistenciaTotal;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	
	
}
