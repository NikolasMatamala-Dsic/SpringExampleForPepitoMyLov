package com.accionistas.accionsis.entity;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Dividendo")
public class Dividendo {

	@OneToMany
	(mappedBy = "dividendo")
	private Set<HistorialDividendo> historialDividendo = new HashSet<>();
	
	/*idDividendo*/
	@Id
	@GeneratedValue
	@Column(name = "idDividendo")
	private int idDividendo;
		
	@ManyToOne
	private JuntaAccionista juntaAccionista;
	
	/*AccionistasRut*/
	@ManyToOne
	private Accionista accionista;
	
	/*FechaDdelaracion*/
	@Column(name="fecha_declaracion")
	private Date fechaDeclaracion;
	
	/*FechaRegistro*/
	@Column(name="fecha_registro")
	private Date fechaRegistro;
	
	/*DividendoPorAccionA*/
	@Column(name="dividendo_accion_a")
	private double dividendoAccionA;
	
	/*DividendoPorAccionB*/
	@Column(name="dividendo_accion_b")
	private double dividendoAccionB;
	
	/*DividendoTotalA*/
	@Column(name="dividendo_total")
	private double dividendoTotal;
	
	/*Estado*/
	@Column(name="estado")
	private int Estado;

	public Dividendo() {
		super();
	}

	public Dividendo(Set<HistorialDividendo> historialDividendo, int idDividendo, JuntaAccionista juntaAccionista,
			Accionista accionista, Date fechaDeclaracion, Date fechaRegistro, double dividendoAccionA,
			double dividendoAccionB, double dividendoTotal, int estado) {
		super();
		this.historialDividendo = historialDividendo;
		this.idDividendo = idDividendo;
		this.juntaAccionista = juntaAccionista;
		this.accionista = accionista;
		this.fechaDeclaracion = fechaDeclaracion;
		this.fechaRegistro = fechaRegistro;
		this.dividendoAccionA = dividendoAccionA;
		this.dividendoAccionB = dividendoAccionB;
		this.dividendoTotal = dividendoTotal;
		Estado = estado;
	}

	@Override
	public String toString() {
		return "Dividendo [historialDividendo=" + historialDividendo + ", idDividendo=" + idDividendo
				+ ", juntaAccionista=" + juntaAccionista + ", accionista=" + accionista + ", fechaDeclaracion="
				+ fechaDeclaracion + ", fechaRegistro=" + fechaRegistro + ", dividendoAccionA=" + dividendoAccionA
				+ ", dividendoAccionB=" + dividendoAccionB + ", dividendoTotal=" + dividendoTotal + ", Estado=" + Estado
				+ "]";
	}

	public Set<HistorialDividendo> getHistorialDividendo() {
		return historialDividendo;
	}

	public void setHistorialDividendo(Set<HistorialDividendo> historialDividendo) {
		this.historialDividendo = historialDividendo;
	}

	public int getIdDividendo() {
		return idDividendo;
	}

	public void setIdDividendo(int idDividendo) {
		this.idDividendo = idDividendo;
	}

	public JuntaAccionista getJuntaAccionista() {
		return juntaAccionista;
	}

	public void setJuntaAccionista(JuntaAccionista juntaAccionista) {
		this.juntaAccionista = juntaAccionista;
	}

	public Accionista getAccionista() {
		return accionista;
	}

	public void setAccionista(Accionista accionista) {
		this.accionista = accionista;
	}

	public Date getFechaDeclaracion() {
		return fechaDeclaracion;
	}

	public void setFechaDeclaracion(Date fechaDeclaracion) {
		this.fechaDeclaracion = fechaDeclaracion;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public double getDividendoAccionA() {
		return dividendoAccionA;
	}

	public void setDividendoAccionA(double dividendoAccionA) {
		this.dividendoAccionA = dividendoAccionA;
	}

	public double getDividendoAccionB() {
		return dividendoAccionB;
	}

	public void setDividendoAccionB(double dividendoAccionB) {
		this.dividendoAccionB = dividendoAccionB;
	}

	public double getDividendoTotal() {
		return dividendoTotal;
	}

	public void setDividendoTotal(double dividendoTotal) {
		this.dividendoTotal = dividendoTotal;
	}

	public int getEstado() {
		return Estado;
	}

	public void setEstado(int estado) {
		Estado = estado;
	}
	
	
}
