package com.accionistas.accionsis.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "HistorialDividendo")
public class HistorialDividendo {

	@Id
	@GeneratedValue
	@Column(name = "idHistorialDividendo")
	private int idHistorialDividendo;
	
	@ManyToOne
	private Dividendo dividendo;
	
	@Column(name = "fechaDeclaracion")
	private Date fechaDeclaracion;
	
	@Column(name  = "fechaRegristro")
	private Date fechaRegristro;
	
	@Column(name = "dividendoPorAccion")
	private double dividendoPorAccion;
	
	@Column(name = "dividendoTotal")
	private double dividendoTotal;

	public HistorialDividendo() {
		super();
	}

	public HistorialDividendo(int idHistorialDividendo, Dividendo dividendo, Date fechaDeclaracion, Date fechaRegristro,
			double dividendoPorAccion, double dividendoTotal) {
		super();
		this.idHistorialDividendo = idHistorialDividendo;
		this.dividendo = dividendo;
		this.fechaDeclaracion = fechaDeclaracion;
		this.fechaRegristro = fechaRegristro;
		this.dividendoPorAccion = dividendoPorAccion;
		this.dividendoTotal = dividendoTotal;
	}

	@Override
	public String toString() {
		return "HistorialDividendo [idHistorialDividendo=" + idHistorialDividendo + ", dividendo=" + dividendo
				+ ", fechaDeclaracion=" + fechaDeclaracion + ", fechaRegristro=" + fechaRegristro
				+ ", dividendoPorAccion=" + dividendoPorAccion + ", dividendoTotal=" + dividendoTotal + "]";
	}

	public int getIdHistorialDividendo() {
		return idHistorialDividendo;
	}

	public void setIdHistorialDividendo(int idHistorialDividendo) {
		this.idHistorialDividendo = idHistorialDividendo;
	}

	public Dividendo getDividendo() {
		return dividendo;
	}

	public void setDividendo(Dividendo dividendo) {
		this.dividendo = dividendo;
	}

	public Date getFechaDeclaracion() {
		return fechaDeclaracion;
	}

	public void setFechaDeclaracion(Date fechaDeclaracion) {
		this.fechaDeclaracion = fechaDeclaracion;
	}

	public Date getFechaRegristro() {
		return fechaRegristro;
	}

	public void setFechaRegristro(Date fechaRegristro) {
		this.fechaRegristro = fechaRegristro;
	}

	public double getDividendoPorAccion() {
		return dividendoPorAccion;
	}

	public void setDividendoPorAccion(double dividendoPorAccion) {
		this.dividendoPorAccion = dividendoPorAccion;
	}

	public double getDividendoTotal() {
		return dividendoTotal;
	}

	public void setDividendoTotal(double dividendoTotal) {
		this.dividendoTotal = dividendoTotal;
	}

	
}
