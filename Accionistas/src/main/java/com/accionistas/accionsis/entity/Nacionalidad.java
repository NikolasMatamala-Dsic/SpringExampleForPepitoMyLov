package com.accionistas.accionsis.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Nacionalidad")
public class Nacionalidad {
	
	@OneToMany
	(mappedBy = "nacionalidad")
	private Set<Accionista> accionista = new HashSet<>();
	
	@OneToMany
	(mappedBy = "nacionalidad")
	private Set<Heredero> heredero = new HashSet<>();
	
	@Id
	@GeneratedValue
	@Column(name="idNacionalidad")
	private int idNacionalidad;
	
	@Column(name="nacionalidad")
	private String nacionalidad;

	public Nacionalidad() {
		super();
	}

	public Nacionalidad(Set<Accionista> accionista, Set<Heredero> heredero, int idNacionalidad, String nacionalidad) {
		super();
		this.accionista = accionista;
		this.heredero = heredero;
		this.idNacionalidad = idNacionalidad;
		this.nacionalidad = nacionalidad;
	}

	@Override
	public String toString() {
		return "Nacionalidad [accionista=" + accionista + ", heredero=" + heredero + ", idNacionalidad="
				+ idNacionalidad + ", nacionalidad=" + nacionalidad + "]";
	}

	public Set<Accionista> getAccionista() {
		return accionista;
	}

	public void setAccionista(Set<Accionista> accionista) {
		this.accionista = accionista;
	}

	public Set<Heredero> getHeredero() {
		return heredero;
	}

	public void setHeredero(Set<Heredero> heredero) {
		this.heredero = heredero;
	}

	public int getIdNacionalidad() {
		return idNacionalidad;
	}

	public void setIdNacionalidad(int idNacionalidad) {
		this.idNacionalidad = idNacionalidad;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	
}
