package com.accionistas.accionsis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "AccionesConProhibicion")
public class AccionesConProhibicion {
	
	@Id
	@GeneratedValue
	@Column(name = "idAccionesProhibicion")
	private int idAccionesProhibicion;
	
	@ManyToOne
	private Accionista accionista;
	
	@Column(name = "cantidadAccionesConProhibicionTipoA")
	private int cantidadAccionesConProhibicionTipoA;
	
	@Column(name = "cantidadAccionesConProhibicionTipoB")
	private int cantidadAccionesConProhibicionTipoB;

	public AccionesConProhibicion() {
		super();
	}

	public AccionesConProhibicion(int idAccionesProhibicion, Accionista accionista,
			int cantidadAccionesConProhibicionTipoA, int cantidadAccionesConProhibicionTipoB) {
		super();
		this.idAccionesProhibicion = idAccionesProhibicion;
		this.accionista = accionista;
		this.cantidadAccionesConProhibicionTipoA = cantidadAccionesConProhibicionTipoA;
		this.cantidadAccionesConProhibicionTipoB = cantidadAccionesConProhibicionTipoB;
	}

	@Override
	public String toString() {
		return "AccionesConProhibicion [idAccionesProhibicion=" + idAccionesProhibicion + ", accionista=" + accionista
				+ ", cantidadAccionesConProhibicionTipoA=" + cantidadAccionesConProhibicionTipoA
				+ ", cantidadAccionesConProhibicionTipoB=" + cantidadAccionesConProhibicionTipoB + "]";
	}

	public int getIdAccionesProhibicion() {
		return idAccionesProhibicion;
	}

	public void setIdAccionesProhibicion(int idAccionesProhibicion) {
		this.idAccionesProhibicion = idAccionesProhibicion;
	}

	public Accionista getAccionista() {
		return accionista;
	}

	public void setAccionista(Accionista accionista) {
		this.accionista = accionista;
	}

	public int getCantidadAccionesConProhibicionTipoA() {
		return cantidadAccionesConProhibicionTipoA;
	}

	public void setCantidadAccionesConProhibicionTipoA(int cantidadAccionesConProhibicionTipoA) {
		this.cantidadAccionesConProhibicionTipoA = cantidadAccionesConProhibicionTipoA;
	}

	public int getCantidadAccionesConProhibicionTipoB() {
		return cantidadAccionesConProhibicionTipoB;
	}

	public void setCantidadAccionesConProhibicionTipoB(int cantidadAccionesConProhibicionTipoB) {
		this.cantidadAccionesConProhibicionTipoB = cantidadAccionesConProhibicionTipoB;
	}

	
}
