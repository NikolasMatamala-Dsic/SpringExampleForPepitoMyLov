package com.accionistas.accionsis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "VotacionJunta")
public class VotacionJunta {
	
	@Id
	@GeneratedValue
	@Column(name = "idVotacion")
	private int idVotacion;
	
	@ManyToOne 
	private JuntaAccionista juntaAccionista;
	
	@Column(name = "cantidadVotaciones")
	private int cantidadVotaciones;
	
	@Column(name = "votosPositivos")
	private int votosPositivos;
	
	@Column(name = "votosNegativos")
	private int votosNegativos;
	
	@Column(name = "observacion")
	private String observacion;

	public VotacionJunta() {
		super();
	}

	public VotacionJunta(int idVotacion, JuntaAccionista juntaAccionista, int cantidadVotaciones, int votosPositivos,
			int votosNegativos, String observacion) {
		super();
		this.idVotacion = idVotacion;
		this.juntaAccionista = juntaAccionista;
		this.cantidadVotaciones = cantidadVotaciones;
		this.votosPositivos = votosPositivos;
		this.votosNegativos = votosNegativos;
		this.observacion = observacion;
	}

	@Override
	public String toString() {
		return "VotacionJunta [idVotacion=" + idVotacion + ", juntaAccionista=" + juntaAccionista
				+ ", cantidadVotaciones=" + cantidadVotaciones + ", votosPositivos=" + votosPositivos
				+ ", votosNegativos=" + votosNegativos + ", observacion=" + observacion + "]";
	}

	public int getIdVotacion() {
		return idVotacion;
	}

	public void setIdVotacion(int idVotacion) {
		this.idVotacion = idVotacion;
	}

	public JuntaAccionista getJuntaAccionista() {
		return juntaAccionista;
	}

	public void setJuntaAccionista(JuntaAccionista juntaAccionista) {
		this.juntaAccionista = juntaAccionista;
	}

	public int getCantidadVotaciones() {
		return cantidadVotaciones;
	}

	public void setCantidadVotaciones(int cantidadVotaciones) {
		this.cantidadVotaciones = cantidadVotaciones;
	}

	public int getVotosPositivos() {
		return votosPositivos;
	}

	public void setVotosPositivos(int votosPositivos) {
		this.votosPositivos = votosPositivos;
	}

	public int getVotosNegativos() {
		return votosNegativos;
	}

	public void setVotosNegativos(int votosNegativos) {
		this.votosNegativos = votosNegativos;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	
}
