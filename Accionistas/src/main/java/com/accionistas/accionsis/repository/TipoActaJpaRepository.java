package com.accionistas.accionsis.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.TipoActa;

@Repository("tipoActaJpaRepository")
public interface TipoActaJpaRepository extends JpaRepository<TipoActa, Serializable>{

	public abstract TipoActa findByidTipoActa(int id);
}
