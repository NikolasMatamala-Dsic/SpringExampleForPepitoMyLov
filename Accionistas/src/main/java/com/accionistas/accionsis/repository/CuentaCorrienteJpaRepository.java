package com.accionistas.accionsis.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.entity.CuentaCorriente;


@Repository("cuentaCorrienteJpaRepository")
public interface CuentaCorrienteJpaRepository extends JpaRepository<CuentaCorriente, Serializable>{

	
	public abstract CuentaCorriente findCuentaCorrienteByAccionista(Accionista accionista);
}
