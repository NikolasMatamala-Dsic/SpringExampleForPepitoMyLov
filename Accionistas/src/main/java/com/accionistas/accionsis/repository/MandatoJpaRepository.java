package com.accionistas.accionsis.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accionistas.accionsis.entity.Mandato;

public interface MandatoJpaRepository extends JpaRepository<Mandato, Serializable> {

	/*
	 * buscar mandatario por id
	 */
	
	public abstract Mandato findByidMandato(int idMandato);
}
