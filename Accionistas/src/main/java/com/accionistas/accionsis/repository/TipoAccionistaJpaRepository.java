package com.accionistas.accionsis.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.TipoAccionista;

@Repository("tipoAccionistaJpaRepository")
public interface TipoAccionistaJpaRepository extends JpaRepository<TipoAccionista, Serializable>{
	
	/**
	 * Encontrar TipoAccionista por id.
	 * @param id
	 * @return tipoAccionista
	 */
	public abstract TipoAccionista findByIdTipoAccionista(int id);
}
