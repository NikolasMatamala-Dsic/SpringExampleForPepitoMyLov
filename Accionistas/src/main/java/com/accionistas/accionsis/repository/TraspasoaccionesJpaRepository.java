package com.accionistas.accionsis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.Traspasoacciones;

import java.io.Serializable;

@Repository("traspasoaccionesJpaRepository")
public interface TraspasoaccionesJpaRepository extends  JpaRepository<Traspasoacciones, Serializable>{
	
	Traspasoacciones findTraspasoaccionesByIdidTraspasoAcciones(int id);
}
