package com.accionistas.accionsis.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.JuntaAccionista;

@Repository("juntaAccionistaJpaRepository")
public interface JuntaAccionistaJpaRepository extends JpaRepository<JuntaAccionista, Serializable> {
	
	/**
	 * Encontrar junta de accionista por id.
	 * @param id
	 * @return giro comercial
	 */
	public abstract JuntaAccionista findByidJuntaAccionista(int id);
	
}
