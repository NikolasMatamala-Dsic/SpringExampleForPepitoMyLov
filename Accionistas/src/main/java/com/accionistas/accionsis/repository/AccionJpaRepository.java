package com.accionistas.accionsis.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.Accion;

@Repository("accionJpaRepository")
public interface AccionJpaRepository extends JpaRepository<Accion, Serializable>{	
	
	public abstract Accion findByidAccion(int id);
}
