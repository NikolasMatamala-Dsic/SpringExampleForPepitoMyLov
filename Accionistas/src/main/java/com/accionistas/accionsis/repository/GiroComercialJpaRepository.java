package com.accionistas.accionsis.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.GiroComercial;

@Repository("giroComercialJpaRepository")
public interface GiroComercialJpaRepository extends JpaRepository<GiroComercial, Serializable>{
	
	/**
	 * Encontrar giro comercial por id.
	 * @param id
	 * @return giro comercial
	 */
	public abstract GiroComercial findByidGiroComercial(int id);

}
