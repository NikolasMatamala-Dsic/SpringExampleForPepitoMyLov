package com.accionistas.accionsis.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.AsistenciaJuntaAccionista;
import com.accionistas.accionsis.entity.JuntaAccionista;



/*Interface AsistenciaJuntaAccionistaJpaRepository*/
@Repository("AsistenciaJuntaAccionistaJpaRepository")
public interface AsistenciaJuntaAccionistaJpaRepository extends JpaRepository<AsistenciaJuntaAccionista, Serializable>{
	
	/*
	 * Buscar AsistenciaJunta por idJunta
	 * @param idJunta
	 * @return AsistenciaJuntaAccionista
	 */
	public abstract AsistenciaJuntaAccionista findByJuntaAccionista(JuntaAccionista juntaAccionista);
	
	public abstract List<AsistenciaJuntaAccionista> findAsistenciaJuntaAccionistaByJuntaAccionista(JuntaAccionista juntaAccionista);
	
	public abstract AsistenciaJuntaAccionista findAsistenciaJuntaAccionistaByidAsistencia(int id);
		
	

}
