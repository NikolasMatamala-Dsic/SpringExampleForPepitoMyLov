package com.accionistas.accionsis.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.Empresa;
import com.accionistas.accionsis.entity.Representante;

/**
 * Interface EmpresaJpaRepository.
 */
@Repository("empresaJpaRepository")
public interface EmpresaJpaRepository extends JpaRepository<Empresa, Serializable>{

	/**
	 * Encontrar empresa por id.
	 * @param id
	 * @return empresa
	 */
	public abstract Empresa findEmpresaByidEmpresa(int id);
	
	public abstract List<Empresa> findEmpresaByRepresentante(Representante representante);
	
	@Query("select e from Empresa e ")
	Empresa findTop();
}
