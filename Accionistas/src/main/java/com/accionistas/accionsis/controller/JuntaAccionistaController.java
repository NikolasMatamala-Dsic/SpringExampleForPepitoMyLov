package com.accionistas.accionsis.controller;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import com.accionistas.accionsis.utility.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.component.AccionistaConverter;
import com.accionistas.accionsis.component.ApoderadoConverter;
import com.accionistas.accionsis.component.AsistenciaJuntaAccionistaConverter;
import com.accionistas.accionsis.component.DividendoConverter;
import com.accionistas.accionsis.component.JuntaAccionistaConverter;
import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.entity.AsistenciaJuntaAccionista;
import com.accionistas.accionsis.entity.Dividendo;
import com.accionistas.accionsis.entity.Empresa;
import com.accionistas.accionsis.entity.JuntaAccionista;
import com.accionistas.accionsis.model.AccionistaModel;
import com.accionistas.accionsis.model.AsistenciaJuntaAccionistaModel;
import com.accionistas.accionsis.model.JuntaAccionistaModel;
import com.accionistas.accionsis.repository.QueryDslRepository;
import com.accionistas.accionsis.service.AccionistaService;
import com.accionistas.accionsis.service.ApoderadoService;
import com.accionistas.accionsis.service.AsistenciaJuntaAccionistaService;
import com.accionistas.accionsis.service.DividendoService;
import com.accionistas.accionsis.service.EmpresaService;
import com.accionistas.accionsis.service.JuntaAccionistaService;

/**
 * Clase AccionistaController.
 */
@Controller
@RequestMapping("juntaaccionista")
public class JuntaAccionistaController {
	
	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(JuntaAccionistaController.class);
	
	@Autowired
	@Qualifier("queryDslRepository")
	private QueryDslRepository queryDslRepository;
	
	/** The junta accionista service. */
	@Autowired
	@Qualifier("juntaAccionistaServiceImp")
	private JuntaAccionistaService juntaAccionistaService;
	
	/** The asistencia junta accionista service. */
	@Autowired
	@Qualifier("AsistenciaJuntaAccionistaServiceImp")
	private AsistenciaJuntaAccionistaService asistenciaJuntaAccionistaService;
	
	@Autowired
	@Qualifier("AsistenciaJuntaAccionistaConverter")
	private AsistenciaJuntaAccionistaConverter asistenciaJuntaAccionistaConverter;
	
	@Autowired
	@Qualifier("accionistaServiceImp")
	private AccionistaService accionistaService;
	
	@Autowired
	@Qualifier("accionistaConverter")
	private AccionistaConverter accionistaConverter;
	

	@Autowired
	@Qualifier("apoderadoServiceImp")
	private ApoderadoService apoderadoService;
	
	@Autowired
	@Qualifier("apoderadoConverter")
	private ApoderadoConverter apoderadoConverter;
	
	@Autowired
	@Qualifier("juntaAccionistaConverter")
	private JuntaAccionistaConverter juntaAccionistaConverter;
	
	@Autowired
	@Qualifier("empresaServiceImp")
	private EmpresaService empresaService;
	
	@Autowired
	@Qualifier("dividendoServiceImp")
	private DividendoService dividendoService;
	
	@Autowired
	@Qualifier("DividendoConverter")
	private DividendoConverter dividendoConverter;
	
	/**
	 * Lista todas las empresas.
	 * @return the model and view
	 */
	@GetMapping("/listjuntas")
	public ModelAndView listAllJuntas() {
		LOG.info("Call: "+ "listJuntas() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.JUNTA_ACCIONISTAS_VIEW);
		mav.addObject("juntasAccionistas", juntaAccionistaService.listAllJuntasAccionistas());
		return mav;
	}
	
	@GetMapping("/cancel")
	public ModelAndView cancel() {
		LOG.info("Call: "+ "listAllJunta() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.JUNTA_ACCIONISTAS_ADMIN);
		mav.addObject("juntasAccionistasAdmin", juntaAccionistaService.listAllJuntasAccionistas());
		return mav;
	}
	
	@GetMapping("/listjuntasAdmin")
	public ModelAndView listAllJuntasAdmin() {
		LOG.info("Call: "+ "listJuntas() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.JUNTA_ACCIONISTAS_ADMIN);
		List<JuntaAccionistaModel> jam = juntaAccionistaService.listAllJuntasAccionistas();
		mav.addObject("juntasAccionistasAdmin", jam);
					
		return mav;
	}

	@GetMapping("/addjuntaaccionistasform")
	private String redirectToAddJuntaAccionistasForm(@RequestParam(name="id", required=false) int id, Model model) {
		LOG.info("Call: "+ "listJuntas() -- desde controller");
		JuntaAccionistaModel juntaAccionistaModel = new JuntaAccionistaModel();
		if(id != 0 ) {
			juntaAccionistaModel = juntaAccionistaService.findJuntaAccionistaByIdModel(id);
		}
		model.addAttribute(juntaAccionistaModel);
		return ViewConstant.JUNTA_ACCIONISTAS_FORM;	
	}
	
	/**
	 * Agrega nuevas juntas de accionistas.
	 * @param juntaAccionistaModel 
	 * @param model 
	 * @return String
	 */	
	@RequestMapping(value="/addjuntaaccionista", method=RequestMethod.GET)
	public @ResponseBody String addJuntaAccionista(@RequestParam(name="id", required=false) String id, @RequestParam String ubicacionModel, @RequestParam String fecha_realizacionModel,
			@RequestParam String hora_inicioModel, @RequestParam String hora_terminoModel, @RequestParam String direccionModel, @RequestParam String fecha_difusionModel){
		
		LOG.info("Call: "+ "addUsuario()");
		
		String respuesta = "";
		JuntaAccionistaModel juntaAccionistaModel = null;
		
		if(!id.equalsIgnoreCase("0")) {
			int idJunta = Integer.parseInt(id);
			juntaAccionistaModel = juntaAccionistaService.findJuntaAccionistaByIdModel(idJunta);
		}else {
			juntaAccionistaModel = new JuntaAccionistaModel();
		}
		
		juntaAccionistaModel.setUbicacionModel(ubicacionModel);
		juntaAccionistaModel.setFecha_realizacionModel(Date.valueOf(fecha_realizacionModel));
		juntaAccionistaModel.setHora_inicioModel(Time.valueOf(hora_inicioModel));
		juntaAccionistaModel.setHora_terminoModel(Time.valueOf(hora_terminoModel));
		juntaAccionistaModel.setDireccionModel(direccionModel);
		juntaAccionistaModel.setFecha_difusionModel(Date.valueOf(fecha_difusionModel));
		
		if(ubicacionModel != "" && fecha_realizacionModel != "" && hora_inicioModel != "" && hora_terminoModel != "" && direccionModel != "" &&
				fecha_difusionModel != "") {
			JuntaAccionistaModel junta = juntaAccionistaService.addJuntaAcconista(juntaAccionistaModel);
			if(null != junta) {
				// si idjunta es = 0 quiere decir que es una nueva junta, por lo tanto se agregan los accionistas y apoderados a la asistencia.
				if(juntaAccionistaModel.getIdJuntaAccionistaModel() == 0) {
					List<AccionistaModel> accList = accionistaService.listAllAccionista_active();				
					AsistenciaJuntaAccionistaModel juntaActual = new AsistenciaJuntaAccionistaModel();
					for(AccionistaModel acc : accList) {					
						juntaActual.setAccionistaModel(acc);					
						juntaActual.setApoderadoModel(acc.getApoderadoModel());										
						juntaActual.setJuntaAccionistaModel(junta);
						juntaActual.setEstadoAsistenciaJuntaModel(0);
						
						asistenciaJuntaAccionistaService.addAsistenciaJuntaAccionista(juntaActual);
						
					}
					JuntaAccionista juntaEntity = juntaAccionistaService.findJuntaAccionistaById(junta.getIdJuntaAccionistaModel());
					List<AsistenciaJuntaAccionista> asistenciaJuntaAcc = asistenciaJuntaAccionistaService.findAsistenciaJuntaAccionistaEntityByJuntaAccionista(juntaAccionistaConverter.entidad2Model(juntaEntity));
					Dividendo div = new Dividendo();
					Empresa emp = empresaService.findEmpresaActiva();
					double valorAccionA = emp.getAccion().getValorA();
					double valorAccionB = emp.getAccion().getValorB();
					double valorDivA = 0;
					double valorDivB = 0; 
					for(AsistenciaJuntaAccionista asistencia : asistenciaJuntaAcc) {
						div.setJuntaAccionista(juntaEntity);
						div.setAccionista(asistencia.getAccionista());
						div.setFechaDeclaracion(juntaEntity.getFecha_difusion());
						div.setFechaRegistro(juntaEntity.getFecha_realizacion());
						div.setEstado(1);
						if(asistencia.getAccionista().getCantidad_acciones_serie_a() > 0 && asistencia.getAccionista().getCantidad_acciones_serie_b() > 0){
							valorDivA = asistencia.getAccionista().getCantidad_acciones_serie_a() * valorAccionA;
							valorDivB = asistencia.getAccionista().getCantidad_acciones_serie_b() * valorAccionB;
							div.setDividendoAccionA(valorDivA);
							div.setDividendoAccionB(valorDivB);
							div.setDividendoTotal(valorDivA+valorAccionB);
						}else if(asistencia.getAccionista().getCantidad_acciones_serie_a() > 0 && asistencia.getAccionista().getCantidad_acciones_serie_b() < 1) {
							valorDivA = asistencia.getAccionista().getCantidad_acciones_serie_a() * valorAccionA;
							div.setDividendoAccionA(valorDivA);
							div.setDividendoAccionB(0);
							div.setDividendoTotal(valorDivA);
						}else if(asistencia.getAccionista().getCantidad_acciones_serie_a() < 1 && asistencia.getAccionista().getCantidad_acciones_serie_b() > 0) {
							valorDivB = asistencia.getAccionista().getCantidad_acciones_serie_b() * valorAccionB;						
							div.setDividendoAccionA(0);
							div.setDividendoAccionB(valorDivB);
							div.setDividendoTotal(valorAccionB);
						}
						
						dividendoService.addDividendo(dividendoConverter.entidad2Model(div));
					}								
				}
				
				
//				model.addAttribute("result", 1);
				respuesta = "1";
				
				return respuesta;
			} else {
//				model.addAttribute("result", 0);
				respuesta = "2";
				return respuesta;
			}
		}else {
			respuesta = "3";
		}
		
		
		return respuesta; 
	}
	
	@RequestMapping(value="del", method=RequestMethod.GET)
	public @ResponseBody String removeJunta(@RequestParam int id) {
		LOG.info("Call: "+ "removeJunta()" + "-- Param: "+ id);
								
			JuntaAccionistaModel junta = new JuntaAccionistaModel();		
			junta.setIdJuntaAccionistaModel(id);
			junta = juntaAccionistaService.findJuntaAccionistaByIdModel(id);
			junta.setEstadoModel(0);		
			String msj = juntaAccionistaService.updateEstadoJunta(junta);
			
		return msj;
	}
	
	@RequestMapping(value="act", method=RequestMethod.GET)
	public @ResponseBody String updateJunta(@RequestParam int id) {
		LOG.info("Call: "+ "updateJunta()" + "-- Param: "+ id);
							
		JuntaAccionistaModel junta = new JuntaAccionistaModel();		
		junta.setIdJuntaAccionistaModel(id);
		junta = juntaAccionistaService.findJuntaAccionistaByIdModel(id);
		junta.setEstadoModel(1);		
		String msj = juntaAccionistaService.updateEstadoJunta(junta);
			
		return msj;
	}
	
	@RequestMapping(value="getAsistenciaJunta", method=RequestMethod.GET)
	public @ResponseBody String getAsistenciaJunta(@RequestParam int id) {
		LOG.info("Call: "+ "getAsistenciaJunta()" + "-- Param: "+ id);
		String msj = "";
		JuntaAccionistaModel junta = new JuntaAccionistaModel();		
		junta.setIdJuntaAccionistaModel(id);
		junta = juntaAccionistaService.findJuntaAccionistaByIdModel(id);
		
		List<AsistenciaJuntaAccionistaModel> asistencia = asistenciaJuntaAccionistaService.findAsistenciaJuntaAccionistaByJuntaAccionista(junta);
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			msj = mapper.writeValueAsString(asistencia);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			msj = "error";
			e.printStackTrace();
		}			
		
		return msj;
	}
	
	@RequestMapping(value="actAsistencia", method=RequestMethod.GET)
	public @ResponseBody String updateAsistencia(@RequestParam int id, @RequestParam boolean estadoActual, @RequestParam int idJunta) {
		LOG.info("Call: "+ "updateAsistenciaJunta()" + "-- Param:id= "+ id+" estadoActual= "+estadoActual);
		String msj = "error";
		AsistenciaJuntaAccionistaModel asistencia = new AsistenciaJuntaAccionistaModel();		
		asistencia.setIdAsistenciaModel(id);
		LOG.info("Call: "+ "updateAsistenciaJunta()" + "-- Param:id= "+ asistencia.toString());
		asistencia = asistenciaJuntaAccionistaService.findAsistenciaJuntaAccionistaByIdAsistenciaJuntaAccionista(id);
		LOG.info("Call: "+ "updateAsistenciaJunta()" + "-- Param:id= "+ asistencia.toString());
		
		if(estadoActual == true) {
			asistencia.setEstadoAsistenciaJuntaModel(1);
		}else {
			asistencia.setEstadoAsistenciaJuntaModel(0);
			}
				
		asistencia = asistenciaJuntaAccionistaService.updateAsistencia(asistenciaJuntaAccionistaConverter.modelo2Entidad(asistencia));
		
		if(asistencia.getEstadoAsistenciaJuntaModel() == 0) {
			msj = "false";
		}else { msj = "true";}
		
		
		JuntaAccionistaModel junta = new JuntaAccionistaModel();		
		junta.setIdJuntaAccionistaModel(idJunta);
		junta = juntaAccionistaService.findJuntaAccionistaByIdModel(idJunta);
		
		List<AsistenciaJuntaAccionistaModel> asistenciaList = asistenciaJuntaAccionistaService.findAsistenciaJuntaAccionistaByJuntaAccionista(junta);
		
		int totalAsistencia = 0, asistidos = 0;	
		
		for(AsistenciaJuntaAccionistaModel row : asistenciaList) {
			if(row.getEstadoAsistenciaJuntaModel() == 1) {
				asistidos++;
			}
			totalAsistencia++;
		}
		
		double porcentajeDeAsistencia = 0;
		porcentajeDeAsistencia = (100*asistidos)/totalAsistencia;
					
		List resultado = new ArrayList<JSONResponse>();
		resultado.add(new JSONResponse(JSONResponse.estado, msj));
		resultado.add(new JSONResponse(JSONResponse.porcentajeDeAsistencia, porcentajeDeAsistencia+""));
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			junta.setAsistenciaTotalModel(porcentajeDeAsistencia);
			junta =juntaAccionistaService.updateJunta(junta);
			LOG.info("Call: "+ "updateAsistenciaJunta()" + "-- Param:id= "+ junta.toString());
			if(junta != null) {
				resultado.add(new JSONResponse(JSONResponse.porcentajeDeAsistenciaTotalJunta, porcentajeDeAsistencia+""));
				
			}
			msj = mapper.writeValueAsString(resultado);
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			msj = "error";
			e.printStackTrace();
		}			
		
		return msj;
	}
	
	
}
