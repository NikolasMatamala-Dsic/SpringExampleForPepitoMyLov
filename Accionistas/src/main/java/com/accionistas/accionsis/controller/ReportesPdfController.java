package com.accionistas.accionsis.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.accionistas.accionsis.entity.Accion;
import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.entity.CuentaCorriente;
import com.accionistas.accionsis.entity.Dividendo;
import com.accionistas.accionsis.entity.Empresa;
import com.accionistas.accionsis.model.ApoderadoModel;
import com.accionistas.accionsis.model.HerederoModel;
import com.accionistas.accionsis.model.JuntaAccionistaModel;
import com.accionistas.accionsis.model.TraspasoaccionesModel;
import com.accionistas.accionsis.repository.CuentaCorrienteJpaRepository;
import com.accionistas.accionsis.service.AccionService;
import com.accionistas.accionsis.service.AccionistaService;
import com.accionistas.accionsis.service.ApoderadoService;
import com.accionistas.accionsis.service.CuentaCorrienteService;
import com.accionistas.accionsis.service.DividendoService;
import com.accionistas.accionsis.service.EmpresaService;
import com.accionistas.accionsis.service.HerederoService;
import com.accionistas.accionsis.service.JuntaAccionistaService;
import com.accionistas.accionsis.service.TraspasoaccionesService;
import com.accionistas.accionsis.utility.RutaReportes;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Controller
@RequestMapping("reportePfd")
public class ReportesPdfController {

	private static final Log LOG = LogFactory.getLog(ReportesPdfController.class);
	
	@Autowired
	@Qualifier("accionistaServiceImp")
	private AccionistaService accionistaService;
	
	@Autowired
	@Qualifier("cuentaCorrienteServiceImp")
	private CuentaCorrienteService cuentaCorrienteService;
	
	@Autowired
	@Qualifier("cuentaCorrienteJpaRepository")
	private CuentaCorrienteJpaRepository cuentaCorrienteJpaRepository;
	
	@Autowired
	@Qualifier("apoderadoServiceImp")
	private ApoderadoService apoderadoService;

	@Autowired
	@Qualifier("empresaServiceImp")
	private EmpresaService empresaService;
	
	@Autowired
	@Qualifier("herederoServiceImp")
	private HerederoService herederoService;
	
	@Autowired
	@Qualifier("accionServiceImp")
	private AccionService accionService;
	
	@Autowired
	@Qualifier("juntaAccionistaServiceImp")
	private JuntaAccionistaService juntaAccionistaService;
	
	@Autowired
	@Qualifier("traspasoaccionesServiceImp")
	private TraspasoaccionesService traspasoaccionesService;
	
	@Autowired
	@Qualifier("dividendoServiceImp")
	private DividendoService dividendoService;
	
	@Autowired
	@Qualifier("cuentaCorrienteServiceImp")
	private CuentaCorrienteService corrienteService;
	
	@RequestMapping(value = "/Filtro", method = RequestMethod.GET)
	public @ResponseBody String FiltroAccionista(@RequestParam(name="filtro") String filtro) {		
		String respuesta = "";
		switch(filtro) {
			case "TodosAccionista":
				respuesta = reporteAccionistas();
				break;
			case "TodosAccionistaActivos":
				respuesta = reporteAccionistasActivos();
				break;
			case "10AccionistasTipoA":
				respuesta = reporteAccionistasMayoritariosTipoA();
				break;
			case "10AccionistasTipoB":
				respuesta = reporteAccionistasMayoritariosTipoB();
				break;
			default:
				respuesta = "2";
				break;
		}
		
		return respuesta;
	}
	
	@RequestMapping(value="/reporteApoderado", method=RequestMethod.GET)
	public @ResponseBody String reporteListaApoderado() {
		return reporteApoderado();
	}
	
	@RequestMapping(value="/reporteHerederos", method=RequestMethod.GET)
	public @ResponseBody String reporteListaHerederos() {
		return reporteHerederos();
	}
	
	@RequestMapping(value="/reporteJunta", method=RequestMethod.GET)
	public @ResponseBody String reporteListaJuntaAccionistas() {
		return reporteJunta();
	}
	
	@RequestMapping(value="/reporteTraspasoAcciones", method=RequestMethod.GET)
	public @ResponseBody String reporteListaTraspaso() {
		return reporteTraspaso();
	}
	
	@RequestMapping(value="/reporteDividendo", method=RequestMethod.GET)
	public @ResponseBody String reporteListaDividendo() {
		return reporteDividendo();
	}
	
	public String reporteTraspaso() {
		String respuesta = "";

		// ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_VIEW);
		LOG.info("Call: " + "Reporte PDF TraspasoAcciones -- desde controller");
		List<TraspasoaccionesModel> traspaso = traspasoaccionesService.listAllTraspasosacciones();
		Empresa em = empresaService.findEmpresaActiva();
		Accion acciones = em.getAccion();
				

		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaPDF);
		ruta += "\\TraspasoAcciones\\Reporte_Traspaso_Acciones_" + FechaActual + "_" + HoraActual + ".pdf";
		
		File file = new File(ruta);
		file.getParentFile().mkdirs();
		Rectangle small = new Rectangle(700, 300);
		Font font = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYBLACK);
		Font letra = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYWHITE);
		Document document = new Document(small, 5, 5, 50, 5);
		try {
			PdfWriter.getInstance(document, new FileOutputStream(ruta));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.open();

		float[] columnWidths = { 2, 5, 7, 7, 7, 7, 5, 5, 5, 5, 7, 5, 7 , 7, 10};
		PdfPTable table = new PdfPTable(columnWidths);
		table.setWidthPercentage(100);
		table.getDefaultCell().setUseAscender(true);
		table.getDefaultCell().setUseDescender(true);

		PdfPCell fecha = new PdfPCell(new Phrase(
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd " + " / " + " HH:mm:ss")), font));
		fecha.setBorder(Rectangle.NO_BORDER);
		fecha.setColspan(15);
		table.addCell(fecha);

		try {
			Image img = Image.getInstance(em.getLogo());
			img.scaleToFit(70, 70);
			PdfPCell encabezado = new PdfPCell(img);
			encabezado.setBorder(Rectangle.NO_BORDER);
			encabezado.setHorizontalAlignment(Element.ALIGN_CENTER);
			encabezado.setColspan(15);
			table.addCell(encabezado);
		} catch (BadElementException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		PdfPCell cell = new PdfPCell(new Phrase("Reporte de Traspaso Acciones", letra));
		cell.setBackgroundColor(GrayColor.GRAYBLACK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setColspan(15);
		table.addCell(cell);

		table.addCell(addcell("#", font, new GrayColor(0.75f)));
		table.addCell(addcell("ID Traspaso", font, new GrayColor(0.75f)));
		table.addCell(addcell("Rut Accionista Comprador", font, new GrayColor(0.75f)));
		table.addCell(addcell("Accionista Comprador", font, new GrayColor(0.75f)));
		table.addCell(addcell("Rut Accionista Vendedor", font, new GrayColor(0.75f)));
		table.addCell(addcell("Accionista Vendedor", font, new GrayColor(0.75f)));
		table.addCell(addcell("Cantidad Acciones Tipo A", font, new GrayColor(0.75f)));
		table.addCell(addcell("Valor Accion A", font, new GrayColor(0.75f)));
		table.addCell(addcell("Cantidad Acciones Tipo B", font, new GrayColor(0.75f)));
		table.addCell(addcell("Valor Accion B", font, new GrayColor(0.75f)));
		table.addCell(addcell("Fecha Venta", font, new GrayColor(0.75f)));
		table.addCell(addcell("Monto Total Venta", font, new GrayColor(0.75f)));
		table.addCell(addcell("Rut Testigo", font, new GrayColor(0.75f)));
		table.addCell(addcell("Testigo", font, new GrayColor(0.75f)));
		table.addCell(addcell("Observacion", font, new GrayColor(0.75f)));
		table.setHeaderRows(2);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

		for (int i = 0; i < traspaso.size(); i++) {
			table.addCell(new PdfPCell(new Phrase((i + 1) + "", font)));
			table.addCell(new PdfPCell(new Phrase(traspaso.get(i).getIdTraspasoAccionesModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(traspaso.get(i).getRut_accionista_compradorModel().getRutModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(traspaso.get(i).getRut_accionista_compradorModel().getNombreModel() + "" 
					+ traspaso.get(i).getRut_accionista_compradorModel().getApellido_paternoModel() + "" + traspaso.get(i).getRut_accionista_compradorModel().getApellido_maternoModel(), font)));
			table.addCell(new PdfPCell(new Phrase(traspaso.get(i).getRut_accionista_vendedorModel().getRutModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(traspaso.get(i).getRut_accionista_vendedorModel().getNombreModel() + "" 
					+ traspaso.get(i).getRut_accionista_vendedorModel().getApellido_paternoModel() + "" + traspaso.get(i).getRut_accionista_vendedorModel().getApellido_maternoModel(), font)));
			table.addCell(new PdfPCell(new Phrase(traspaso.get(i).getCantidad_acciones_vendidas_aModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(acciones.getValorA() + "", font)));
			table.addCell(new PdfPCell(new Phrase(traspaso.get(i).getCantidad_acciones_vendidas_bModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(acciones.getValorB() + "", font)));
			table.addCell(new PdfPCell(new Phrase(traspaso.get(i).getFecha_ventaModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(traspaso.get(i).getMontoModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(traspaso.get(i).getTestigoModel().getRutModel(), font)));
			table.addCell(new PdfPCell(new Phrase(traspaso.get(i).getTestigoModel().getNombreModel() + "" 
					+ traspaso.get(i).getTestigoModel().getApellido_paternoModel() + "" + traspaso.get(i).getTestigoModel().getApellido_maternoModel(), font)));
			table.addCell(new PdfPCell(new Phrase(traspaso.get(i).getObservacionModel(), font)));
		}

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();

		try {
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			respuesta = "1";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respuesta;
	}
	
	public String reporteJunta() {
		String respuesta = "";

		// ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_VIEW);
		LOG.info("Call: " + "Reporte PDF Junta Accionista -- desde controller");
		List<JuntaAccionistaModel> junta = juntaAccionistaService.listAllJuntasAccionistas();
		Empresa em = empresaService.findEmpresaActiva();

		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaPDF);
		ruta += "\\JuntaAccionistas\\Reporte_Junta_Accionista_" + FechaActual + "_" + HoraActual + ".pdf";
		
		File file = new File(ruta);
		file.getParentFile().mkdirs();
		Rectangle small = new Rectangle(700, 300);
		Font font = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYBLACK);
		Font letra = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYWHITE);
		Document document = new Document(small, 5, 5, 50, 5);
		try {
			PdfWriter.getInstance(document, new FileOutputStream(ruta));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.open();

		float[] columnWidths = { 2, 5, 7, 5, 7, 7, 7, 7, 5, 4};
		PdfPTable table = new PdfPTable(columnWidths);
		table.setWidthPercentage(100);
		table.getDefaultCell().setUseAscender(true);
		table.getDefaultCell().setUseDescender(true);

		PdfPCell fecha = new PdfPCell(new Phrase(
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd " + " / " + " HH:mm:ss")), font));
		fecha.setBorder(Rectangle.NO_BORDER);
		fecha.setColspan(10);
		table.addCell(fecha);

		try {
			Image img = Image.getInstance(em.getLogo());
			img.scaleToFit(70, 70);
			PdfPCell encabezado = new PdfPCell(img);
			encabezado.setBorder(Rectangle.NO_BORDER);
			encabezado.setHorizontalAlignment(Element.ALIGN_CENTER);
			encabezado.setColspan(10);
			table.addCell(encabezado);
		} catch (BadElementException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		PdfPCell cell = new PdfPCell(new Phrase("Reporte de Junta Accionistas", letra));
		cell.setBackgroundColor(GrayColor.GRAYBLACK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setColspan(10);
		table.addCell(cell);

		table.addCell(addcell("#", font, new GrayColor(0.75f)));
		table.addCell(addcell("ID Junta", font, new GrayColor(0.75f)));
		table.addCell(addcell("Ubicacion", font, new GrayColor(0.75f)));
		table.addCell(addcell("Direccion", font, new GrayColor(0.75f)));
		table.addCell(addcell("Fecha Difusion", font, new GrayColor(0.75f)));
		table.addCell(addcell("Fecha realizada", font, new GrayColor(0.75f)));
		table.addCell(addcell("Hora de Inicio", font, new GrayColor(0.75f)));
		table.addCell(addcell("Hora de Termino", font, new GrayColor(0.75f)));
		table.addCell(addcell("Aistencia Total", font, new GrayColor(0.75f)));
		table.addCell(addcell("Estado", font, new GrayColor(0.75f)));
		table.setHeaderRows(2);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

		for (int i = 0; i < junta.size(); i++) {
			table.addCell(new PdfPCell(new Phrase((i + 1) + "", font)));
			table.addCell(new PdfPCell(new Phrase(junta.get(i).getIdJuntaAccionistaModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(junta.get(i).getUbicacionModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(junta.get(i).getDireccionModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(junta.get(i).getFecha_difusionModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(junta.get(i).getFecha_realizacionModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(junta.get(i).getHora_inicioModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(junta.get(i).getHora_terminoModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(junta.get(i).getAsistenciaTotalModel() + "", font)));
			if (junta.get(i).getEstadoModel() == 0) {
				table.addCell(new PdfPCell(new Phrase("Eliminado", font)));
			} else {
				table.addCell(new PdfPCell(new Phrase("Activo", font)));
			}
		}

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();

		try {
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			respuesta = "1";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respuesta;
	}

	public String reporteHerederos() {
		String respuesta = "";

		// ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_VIEW);
		LOG.info("Call: " + "Reporte PDF Herederos -- desde controller");
		List<HerederoModel> her = herederoService.listAllHeredero();
		Empresa em = empresaService.findEmpresaActiva();

		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaPDF);
		ruta += "\\Herederos\\Reporte_Herederos_" + FechaActual + "_" + HoraActual + ".pdf";
		
		File file = new File(ruta);
		file.getParentFile().mkdirs();
		Rectangle small = new Rectangle(1000, 300);
		Font font = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYBLACK);
		Font letra = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYWHITE);
		Document document = new Document(small, 5, 5, 50, 5);
		try {
			PdfWriter.getInstance(document, new FileOutputStream(ruta));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.open();

		float[] columnWidths = { 2, 5, 7, 5, 7, 7, 7, 7, 5, 7, 7, 2, 7, 5, 4};
		PdfPTable table = new PdfPTable(columnWidths);
		table.setWidthPercentage(100);
		table.getDefaultCell().setUseAscender(true);
		table.getDefaultCell().setUseDescender(true);

		PdfPCell fecha = new PdfPCell(new Phrase(
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd " + " / " + " HH:mm:ss")), font));
		fecha.setBorder(Rectangle.NO_BORDER);
		fecha.setColspan(15);
		table.addCell(fecha);

		try {
			Image img = Image.getInstance(em.getLogo());
			img.scaleToFit(70, 70);
			PdfPCell encabezado = new PdfPCell(img);
			encabezado.setBorder(Rectangle.NO_BORDER);
			encabezado.setHorizontalAlignment(Element.ALIGN_CENTER);
			encabezado.setColspan(15);
			table.addCell(encabezado);
		} catch (BadElementException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		PdfPCell cell = new PdfPCell(new Phrase("Reporte de Herederos", letra));
		cell.setBackgroundColor(GrayColor.GRAYBLACK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setColspan(15);
		table.addCell(cell);

		table.addCell(addcell("#", font, new GrayColor(0.75f)));
		table.addCell(addcell("Rut", font, new GrayColor(0.75f)));
		table.addCell(addcell("Nombre Herederos", font, new GrayColor(0.75f)));
		table.addCell(addcell("Rut Accionista", font, new GrayColor(0.75f)));
		table.addCell(addcell("Nombre Accionista", font, new GrayColor(0.75f)));
		table.addCell(addcell("Acciones Tipo A", font, new GrayColor(0.75f)));
		table.addCell(addcell("Acciones Tipo B", font, new GrayColor(0.75f)));
		table.addCell(addcell("Direccion", font, new GrayColor(0.75f)));
		table.addCell(addcell("Nacionalidad", font, new GrayColor(0.75f)));
		table.addCell(addcell("Empresa", font, new GrayColor(0.75f)));
		table.addCell(addcell("Tipo Accionista", font, new GrayColor(0.75f)));
		table.addCell(addcell("% de Participacion", font, new GrayColor(0.75f)));
		table.addCell(addcell("E-Mail", font, new GrayColor(0.75f)));
		table.addCell(addcell("Telefono", font, new GrayColor(0.75f)));
		table.addCell(addcell("Estado", font, new GrayColor(0.75f)));
		table.setHeaderRows(2);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

		for (int i = 0; i < her.size(); i++) {
			table.addCell(new PdfPCell(new Phrase((i + 1) + "", font)));
			table.addCell(new PdfPCell(new Phrase(her.get(i).getRutModel(), font)));
			table.addCell(new PdfPCell(new Phrase(her.get(i).getNombreModel() + " "
					+ her.get(i).getApellido_paternoModel() + " " + her.get(i).getApellido_maternoModel(), font)));
			table.addCell(new PdfPCell(new Phrase(her.get(i).getAccionistaModel().getRutModel(), font)));
			table.addCell(new PdfPCell(new Phrase(her.get(i).getAccionistaModel().getNombreModel() + " "
					+ her.get(i).getAccionistaModel().getApellido_paternoModel() + " " + her.get(i).getAccionistaModel().getApellido_maternoModel(), font)));
			table.addCell(new PdfPCell(new Phrase(her.get(i).getCantidad_acciones_serie_aModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(her.get(i).getCantidad_acciones_serie_bModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(her.get(i).getDireccionModel() + ", " + her.get(i).getComunaModel()
					+ ", " + her.get(i).getCiudadModel(), font)));
			table.addCell(new PdfPCell(new Phrase(her.get(i).getNacionalidadModel().getNacionalidadModel(), font)));
			table.addCell(new PdfPCell(new Phrase(her.get(i).getEmpresaModel().getRazonSocialModel(), font)));
			table.addCell(new PdfPCell(new Phrase(her.get(i).getTipoAccionistaModel().getDescripcion_tipo_accionistaModel(), font)));
			table.addCell(new PdfPCell(new Phrase(her.get(i).getPorcentaje_participacionModel() + "%", font)));
			table.addCell(new PdfPCell(new Phrase(her.get(i).getEmailModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(her.get(i).getFonoModel() + "", font)));
			if (her.get(i).getEstadoModel() == 0) {
				table.addCell(new PdfPCell(new Phrase("Eliminado", font)));
			} else {
				table.addCell(new PdfPCell(new Phrase("Activo", font)));
			}
		}

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();

		try {
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			respuesta = "1";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respuesta;
	}
	
	public String reporteApoderado() {
		String respuesta = "";

		// ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_VIEW);
		LOG.info("Call: " + "Reporte PDF Apoderado -- desde controller");
		List<ApoderadoModel> ap = apoderadoService.listAllApoderados();
		Empresa em = empresaService.findEmpresaActiva();

		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaPDF);
		ruta += "\\Apoderados\\Reporte_Apoderado_" + FechaActual + "_" + HoraActual + ".pdf";
		
		File file = new File(ruta);
		file.getParentFile().mkdirs();
		Rectangle small = new Rectangle(400, 300);
		Font font = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYBLACK);
		Font letra = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYWHITE);
		Document document = new Document(small, 5, 5, 50, 5);
		try {
			PdfWriter.getInstance(document, new FileOutputStream(ruta));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.open();

		float[] columnWidths = { 2, 5, 7, 7, 5, 4};
		PdfPTable table = new PdfPTable(columnWidths);
		table.setWidthPercentage(100);
		table.getDefaultCell().setUseAscender(true);
		table.getDefaultCell().setUseDescender(true);

		PdfPCell fecha = new PdfPCell(new Phrase(
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd " + " / " + " HH:mm:ss")), font));
		fecha.setBorder(Rectangle.NO_BORDER);
		fecha.setColspan(6);
		table.addCell(fecha);

		try {
			Image img = Image.getInstance(em.getLogo());
			img.scaleToFit(70, 70);
			PdfPCell encabezado = new PdfPCell(img);
			encabezado.setBorder(Rectangle.NO_BORDER);
			encabezado.setHorizontalAlignment(Element.ALIGN_CENTER);
			encabezado.setColspan(6);
			table.addCell(encabezado);
		} catch (BadElementException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		PdfPCell cell = new PdfPCell(new Phrase("Reporte de Apoderados", letra));
		cell.setBackgroundColor(GrayColor.GRAYBLACK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setColspan(6);
		table.addCell(cell);

		table.addCell(addcell("#", font, new GrayColor(0.75f)));
		table.addCell(addcell("Rut", font, new GrayColor(0.75f)));
		table.addCell(addcell("Nombre Apoderado", font, new GrayColor(0.75f)));
		table.addCell(addcell("E-Mail", font, new GrayColor(0.75f)));
		table.addCell(addcell("Telefono", font, new GrayColor(0.75f)));
		table.addCell(addcell("Estado", font, new GrayColor(0.75f)));
		table.setHeaderRows(2);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

		for (int i = 0; i < ap.size(); i++) {
			table.addCell(new PdfPCell(new Phrase((i + 1) + "", font)));
			table.addCell(new PdfPCell(new Phrase(ap.get(i).getRutModel(), font)));
			table.addCell(new PdfPCell(new Phrase(ap.get(i).getNombreModel() + " "
					+ ap.get(i).getApellido_paternoModel() + " " + ap.get(i).getApellido_maternoModel(), font)));
			table.addCell(new PdfPCell(new Phrase(ap.get(i).getEmailModel() + "", font)));
			table.addCell(new PdfPCell(new Phrase(ap.get(i).getFonoModel() + "", font)));
			if (ap.get(i).getEstadoModel() == 0) {
				table.addCell(new PdfPCell(new Phrase("Eliminado", font)));
			} else {
				table.addCell(new PdfPCell(new Phrase("Activo", font)));
			}
		}

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();

		try {
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			respuesta = "1";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respuesta;
	}

	public String reporteAccionistas() {
		String respuesta = "";

		// ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_VIEW);
		LOG.info("Call: " + "Reporte PDF Accionista -- desde controller");
//		List<AccionistaModel> acc = accionistaService.listAllAccionista();
		List<Accionista> acc = accionistaService.listAllAccionistaEntity();
		Empresa em = empresaService.findEmpresaActiva();
		
		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaPDF);
		ruta += "\\Acionistas\\Accionistas_Todos\\Reporte_Accionistas_" + FechaActual + "_" + HoraActual + ".pdf";
		
		File file = new File(ruta);
		file.getParentFile().mkdirs();
		Rectangle small = new Rectangle(1000, 300);
		Font font = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYBLACK);
		Font letra = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYWHITE);
		Document document = new Document(small, 5, 5, 50, 5);
		try {
			PdfWriter.getInstance(document, new FileOutputStream(ruta));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.open();

		float[] columnWidths = { 2, 5, 7, 5, 5, 5, 10, 5, 7, 7, 7, 4, 5, 5, 4 };
		PdfPTable table = new PdfPTable(columnWidths);
		table.setWidthPercentage(100);
		table.getDefaultCell().setUseAscender(true);
		table.getDefaultCell().setUseDescender(true);

		PdfPCell fecha = new PdfPCell(new Phrase(
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd " + " / " + " HH:mm:ss")), font));
		fecha.setBorder(Rectangle.NO_BORDER);
		fecha.setColspan(15);
		table.addCell(fecha);

		try {
			Image img = Image.getInstance(em.getLogo());
			img.scaleToFit(70, 70);
			PdfPCell encabezado = new PdfPCell(img);
			encabezado.setBorder(Rectangle.NO_BORDER);
			encabezado.setHorizontalAlignment(Element.ALIGN_CENTER);
			encabezado.setColspan(15);
			table.addCell(encabezado);
		} catch (BadElementException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		PdfPCell cell = new PdfPCell(new Phrase("Reporte de Accionistas", letra));
		cell.setBackgroundColor(GrayColor.GRAYBLACK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setColspan(15);
		table.addCell(cell);

		table.addCell(addcell("#", font, new GrayColor(0.75f)));
		table.addCell(addcell("Rut", font, new GrayColor(0.75f)));
		table.addCell(addcell("Nombre", font, new GrayColor(0.75f)));
		table.addCell(addcell("Acciones Tipo A", font, new GrayColor(0.75f)));
		table.addCell(addcell("Acciones Tipo B", font, new GrayColor(0.75f)));
		table.addCell(addcell("Total Acciones", font, new GrayColor(0.75f)));
		table.addCell(addcell("Direccion", font, new GrayColor(0.75f)));
		table.addCell(addcell("Nacionalidad", font, new GrayColor(0.75f)));
		table.addCell(addcell("Apoderado", font, new GrayColor(0.75f)));
		table.addCell(addcell("Empresa", font, new GrayColor(0.75f)));
		table.addCell(addcell("Tipo Accionista", font, new GrayColor(0.75f)));
		table.addCell(addcell("% de Participacion", font, new GrayColor(0.75f)));
		table.addCell(addcell("Cta. Corriente", font, new GrayColor(0.75f)));
		table.addCell(addcell("Banco", font, new GrayColor(0.75f)));
		table.addCell(addcell("Estado", font, new GrayColor(0.75f)));
		table.setHeaderRows(2);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		int totalAccionesA = 0, totalAccionesB = 0, totalTotal = 0;
		for (int i = 0; i < acc.size(); i++) {
			table.addCell(new PdfPCell(new Phrase((i + 1) + "", font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getRut(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getNombre() + " "
					+ acc.get(i).getApellido_paterno() + " " + acc.get(i).getApellido_materno(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getCantidad_acciones_serie_a() + "", font)));
			totalAccionesA += acc.get(i).getCantidad_acciones_serie_a();
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getCantidad_acciones_serie_b() + "", font)));
			totalAccionesB += acc.get(i).getCantidad_acciones_serie_b();
			table.addCell(new PdfPCell(new Phrase((acc.get(i).getCantidad_acciones_serie_b() + acc.get(i).getCantidad_acciones_serie_a()) + "", font)));
			totalTotal += acc.get(i).getCantidad_acciones_serie_b() + acc.get(i).getCantidad_acciones_serie_a();
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getDireccion() + ", " + acc.get(i).getComuna()
					+ ", " + acc.get(i).getCiudad(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getNacionalidad().getNacionalidad(), font)));
			if (acc.get(i).getApoderado() == null) {
				table.addCell(new PdfPCell(new Phrase("/*Sin Apoderado*/", font)));
			} else {
				table.addCell(new PdfPCell(new Phrase(acc.get(i).getApoderado().getNombre() + " "
						+ acc.get(i).getApoderado().getApellido_paterno() + " "
						+ acc.get(i).getApoderado().getApellido_materno(), font)));
			}
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getEmpresa().getRazonSocial(), font)));
			table.addCell(new PdfPCell(
					new Phrase(acc.get(i).getTipoAccionista().getDescripcion_tipo_accionista(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getPorcentaje_participacion() + "%", font)));
			String numeroC = "/*Sin Cta. Coriente*/", nombreB = "/*Sin Banco*/";
			for(CuentaCorriente c : acc.get(i).getCuentaCorriente()) {
				numeroC = c.getNumeroCuenta()+"";
				nombreB = c.getNombreBanco();
				break;
			}
			table.addCell(new PdfPCell(new Phrase(numeroC, font)));
			table.addCell(new PdfPCell(new Phrase(nombreB, font)));
			if (acc.get(i).getEstado() == 0) {
				table.addCell(new PdfPCell(new Phrase("Eliminado", font)));
			} else {
				table.addCell(new PdfPCell(new Phrase("Activo", font)));
			}
			
		}
		
		PdfPCell total = new PdfPCell();
		total.setBorder(Rectangle.NO_BORDER);
		total.setColspan(15);
		table.addCell(addcell("", font, new GrayColor(0.57f)));
		table.addCell(addcell("", font, new GrayColor(0.57f)));
		table.addCell(addcell("Total: ", font, new GrayColor(0.57f)));
		table.addCell(addcell(totalAccionesA+"",font , new GrayColor(0.57f)));
		table.addCell(addcell(totalAccionesB+"",font , new GrayColor(0.57f)));
		table.addCell(addcell(totalTotal+"",font , new GrayColor(0.57f)));
		table.addCell(total);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();

		try {
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			respuesta = "1";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respuesta;
	}

	public String reporteAccionistasActivos() {
		String respuesta = "";

		// ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_VIEW);
		LOG.info("Call: " + "Reporte PDF Accionista -- desde controller");
//		List<AccionistaModel> acc = accionistaService.listAllAccionista();
		List<Accionista> acc = accionistaService.listAllAccionistaEntity_active();
		Empresa em = empresaService.findEmpresaActiva();
		
		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaPDF);
		ruta += "\\Acionistas\\Accionistas_Todos_Activos\\Reporte_Accionistas_Activos" + FechaActual + "_" + HoraActual + ".pdf";
		
		File file = new File(ruta);
		file.getParentFile().mkdirs();
		Rectangle small = new Rectangle(1000, 300);
		Font font = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYBLACK);
		Font letra = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYWHITE);
		Document document = new Document(small, 5, 5, 50, 5);
		try {
			PdfWriter.getInstance(document, new FileOutputStream(ruta));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.open();

		float[] columnWidths = { 2, 5, 7, 5, 5, 5, 10, 5, 7, 7, 7, 4, 5, 5, 4 };
		PdfPTable table = new PdfPTable(columnWidths);
		table.setWidthPercentage(100);
		table.getDefaultCell().setUseAscender(true);
		table.getDefaultCell().setUseDescender(true);

		PdfPCell fecha = new PdfPCell(new Phrase(
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd " + " / " + " HH:mm:ss")), font));
		fecha.setBorder(Rectangle.NO_BORDER);
		fecha.setColspan(15);
		table.addCell(fecha);

		try {
			Image img = Image.getInstance(em.getLogo());
			img.scaleToFit(70, 70);
			PdfPCell encabezado = new PdfPCell(img);
			encabezado.setBorder(Rectangle.NO_BORDER);
			encabezado.setHorizontalAlignment(Element.ALIGN_CENTER);
			encabezado.setColspan(15);
			table.addCell(encabezado);
		} catch (BadElementException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		PdfPCell cell = new PdfPCell(new Phrase("Reporte de Accionistas", letra));
		cell.setBackgroundColor(GrayColor.GRAYBLACK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setColspan(15);
		table.addCell(cell);

		table.addCell(addcell("#", font, new GrayColor(0.75f)));
		table.addCell(addcell("Rut", font, new GrayColor(0.75f)));
		table.addCell(addcell("Nombre", font, new GrayColor(0.75f)));
		table.addCell(addcell("Acciones Tipo A", font, new GrayColor(0.75f)));
		table.addCell(addcell("Acciones Tipo B", font, new GrayColor(0.75f)));
		table.addCell(addcell("Total Acciones", font, new GrayColor(0.75f)));
		table.addCell(addcell("Direccion", font, new GrayColor(0.75f)));
		table.addCell(addcell("Nacionalidad", font, new GrayColor(0.75f)));
		table.addCell(addcell("Apoderado", font, new GrayColor(0.75f)));
		table.addCell(addcell("Empresa", font, new GrayColor(0.75f)));
		table.addCell(addcell("Tipo Accionista", font, new GrayColor(0.75f)));
		table.addCell(addcell("% de Participacion", font, new GrayColor(0.75f)));
		table.addCell(addcell("Cta. Corriente", font, new GrayColor(0.75f)));
		table.addCell(addcell("Banco", font, new GrayColor(0.75f)));
		table.addCell(addcell("Estado", font, new GrayColor(0.75f)));
		table.setHeaderRows(2);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		int totalAccionesA = 0, totalAccionesB = 0, totalTotal = 0;
		for (int i = 0; i < acc.size(); i++) {
			table.addCell(new PdfPCell(new Phrase((i + 1) + "", font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getRut(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getNombre() + " "
					+ acc.get(i).getApellido_paterno() + " " + acc.get(i).getApellido_materno(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getCantidad_acciones_serie_a() + "", font)));
			totalAccionesA += acc.get(i).getCantidad_acciones_serie_a();
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getCantidad_acciones_serie_b() + "", font)));
			totalAccionesB += acc.get(i).getCantidad_acciones_serie_b();
			table.addCell(new PdfPCell(new Phrase((acc.get(i).getCantidad_acciones_serie_b() + acc.get(i).getCantidad_acciones_serie_a()) + "", font)));
			totalTotal += acc.get(i).getCantidad_acciones_serie_b() + acc.get(i).getCantidad_acciones_serie_a();
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getDireccion() + ", " + acc.get(i).getComuna()
					+ ", " + acc.get(i).getCiudad(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getNacionalidad().getNacionalidad(), font)));
			if (acc.get(i).getApoderado() == null) {
				table.addCell(new PdfPCell(new Phrase("/*Sin Apoderado*/", font)));
			} else {
				table.addCell(new PdfPCell(new Phrase(acc.get(i).getApoderado().getNombre() + " "
						+ acc.get(i).getApoderado().getApellido_paterno() + " "
						+ acc.get(i).getApoderado().getApellido_materno(), font)));
			}
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getEmpresa().getRazonSocial(), font)));
			table.addCell(new PdfPCell(
					new Phrase(acc.get(i).getTipoAccionista().getDescripcion_tipo_accionista(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getPorcentaje_participacion() + "%", font)));
			String numeroC = "/*Sin Cta. Coriente*/", nombreB = "/*Sin Banco*/";
			for(CuentaCorriente c : acc.get(i).getCuentaCorriente()) {
				numeroC = c.getNumeroCuenta()+"";
				nombreB = c.getNombreBanco();
				break;
			}
			table.addCell(new PdfPCell(new Phrase(numeroC, font)));
			table.addCell(new PdfPCell(new Phrase(nombreB, font)));
			if (acc.get(i).getEstado() == 0) {
				table.addCell(new PdfPCell(new Phrase("Eliminado", font)));
			} else {
				table.addCell(new PdfPCell(new Phrase("Activo", font)));
			}
			
		}
		
		PdfPCell total = new PdfPCell();
		total.setBorder(Rectangle.NO_BORDER);
		total.setColspan(15);
		table.addCell(addcell("", font, new GrayColor(0.57f)));
		table.addCell(addcell("", font, new GrayColor(0.57f)));
		table.addCell(addcell("Total: ", font, new GrayColor(0.57f)));
		table.addCell(addcell(totalAccionesA+"",font , new GrayColor(0.57f)));
		table.addCell(addcell(totalAccionesB+"",font , new GrayColor(0.57f)));
		table.addCell(addcell(totalTotal+"",font , new GrayColor(0.57f)));
		table.addCell(total);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();

		try {
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			respuesta = "1";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respuesta;
	}
	
	public String reporteAccionistasMayoritariosTipoA() {
		String respuesta = "";

		// ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_VIEW);
		LOG.info("Call: " + "Reporte PDF Accionista -- desde controller");
//		List<AccionistaModel> acc = accionistaService.listAllAccionista();
		List<Accionista> acc = accionistaService.listAccionistasForTop10AccionTypeA();
		Empresa em = empresaService.findEmpresaActiva();
		
		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaPDF);
		ruta += "\\Acionistas\\Accionistas_AccionesTipoA_TOP10\\Reporte_Accionistas_" + FechaActual + "_" + HoraActual + ".pdf";
		
		File file = new File(ruta);
		file.getParentFile().mkdirs();
		Rectangle small = new Rectangle(1000, 300);
		Font font = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYBLACK);
		Font letra = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYWHITE);
		Document document = new Document(small, 5, 5, 50, 5);
		try {
			PdfWriter.getInstance(document, new FileOutputStream(ruta));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.open();

		float[] columnWidths = { 2, 5, 7, 5, 10, 5, 7, 7, 7, 4, 5, 5, 4};
		PdfPTable table = new PdfPTable(columnWidths);
		table.setWidthPercentage(100);
		table.getDefaultCell().setUseAscender(true);
		table.getDefaultCell().setUseDescender(true);

		PdfPCell fecha = new PdfPCell(new Phrase(
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd " + " / " + " HH:mm:ss")), font));
		fecha.setBorder(Rectangle.NO_BORDER);
		fecha.setColspan(13);
		table.addCell(fecha);

		try {
			Image img = Image.getInstance(em.getLogo());
			img.scaleToFit(70, 70);
			PdfPCell encabezado = new PdfPCell(img);
			encabezado.setBorder(Rectangle.NO_BORDER);
			encabezado.setHorizontalAlignment(Element.ALIGN_CENTER);
			encabezado.setColspan(13);
			table.addCell(encabezado);
		} catch (BadElementException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		PdfPCell cell = new PdfPCell(new Phrase("Reporte de Accionistas Mayoritarios Acciones Tipo A", letra));
		cell.setBackgroundColor(GrayColor.GRAYBLACK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setColspan(13);
		table.addCell(cell);

		table.addCell(addcell("#", font, new GrayColor(0.75f)));
		table.addCell(addcell("Rut", font, new GrayColor(0.75f)));
		table.addCell(addcell("Nombre", font, new GrayColor(0.75f)));
		table.addCell(addcell("Acciones Tipo A", font, new GrayColor(0.75f)));
		table.addCell(addcell("Direccion", font, new GrayColor(0.75f)));
		table.addCell(addcell("Nacionalidad", font, new GrayColor(0.75f)));
		table.addCell(addcell("Apoderado", font, new GrayColor(0.75f)));
		table.addCell(addcell("Empresa", font, new GrayColor(0.75f)));
		table.addCell(addcell("Tipo Accionista", font, new GrayColor(0.75f)));
		table.addCell(addcell("% de Participacion", font, new GrayColor(0.75f)));
		table.addCell(addcell("Cta. Corriente", font, new GrayColor(0.75f)));
		table.addCell(addcell("Banco", font, new GrayColor(0.75f)));
		table.addCell(addcell("Estado", font, new GrayColor(0.75f)));
		table.setHeaderRows(2);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		int totalAccionesA = 0;
		for (int i = 0; i < acc.size(); i++) {
			table.addCell(new PdfPCell(new Phrase((i + 1) + "", font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getRut(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getNombre() + " "
					+ acc.get(i).getApellido_paterno() + " " + acc.get(i).getApellido_materno(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getCantidad_acciones_serie_a() + "", font)));
			totalAccionesA += acc.get(i).getCantidad_acciones_serie_a();
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getDireccion() + ", " + acc.get(i).getComuna()
					+ ", " + acc.get(i).getCiudad(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getNacionalidad().getNacionalidad(), font)));
			if (acc.get(i).getApoderado() == null) {
				table.addCell(new PdfPCell(new Phrase("/*Sin Apoderado*/", font)));
			} else {
				table.addCell(new PdfPCell(new Phrase(acc.get(i).getApoderado().getNombre() + " "
						+ acc.get(i).getApoderado().getApellido_paterno() + " "
						+ acc.get(i).getApoderado().getApellido_materno(), font)));
			}
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getEmpresa().getRazonSocial(), font)));
			table.addCell(new PdfPCell(
					new Phrase(acc.get(i).getTipoAccionista().getDescripcion_tipo_accionista(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getPorcentaje_participacion() + "%", font)));
			String numeroC = "Sin Cta. Corriente", nombreB = "Sin Banco";
			for(CuentaCorriente c : acc.get(i).getCuentaCorriente()) {
				numeroC = c.getNumeroCuenta()+"";
				nombreB = c.getNombreBanco();
				break;
			}
			table.addCell(new PdfPCell(new Phrase(numeroC, font)));
			table.addCell(new PdfPCell(new Phrase(nombreB, font)));
			if (acc.get(i).getEstado() == 0) {
				table.addCell(new PdfPCell(new Phrase("Eliminado", font)));
			} else {
				table.addCell(new PdfPCell(new Phrase("Activo", font)));
			}
		}
		
		PdfPCell total = new PdfPCell();
		total.setBorder(Rectangle.NO_BORDER);
		total.setColspan(14);
		table.addCell(addcell("", font, new GrayColor(0.57f)));
		table.addCell(addcell("", font, new GrayColor(0.57f)));
		table.addCell(addcell("Total: ", font, new GrayColor(0.57f)));
		table.addCell(addcell(totalAccionesA + "", font, new GrayColor(0.57f)));
		table.addCell(total);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();

		try {
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			respuesta = "1";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respuesta;
	}

	public String reporteAccionistasMayoritariosTipoB() {
		String respuesta = "";

		// ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_VIEW);
		LOG.info("Call: " + "Reporte PDF Accionista -- desde controller");
//		List<AccionistaModel> acc = accionistaService.listAllAccionista();
		List<Accionista> acc = accionistaService.listAccionistasForTop10AccionTypeB();
		Empresa em = empresaService.findEmpresaActiva();
		
		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaPDF);
		ruta += "\\Acionistas\\Accionistas_AccionesTipoB_TOP10\\Reporte_Accionistas_" + FechaActual + "_" + HoraActual + ".pdf";
		
		File file = new File(ruta);
		file.getParentFile().mkdirs();
		Rectangle small = new Rectangle(1000, 300);
		Font font = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYBLACK);
		Font letra = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYWHITE);
		Document document = new Document(small, 5, 5, 50, 5);
		try {
			PdfWriter.getInstance(document, new FileOutputStream(ruta));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.open();

		float[] columnWidths = { 2, 5, 7, 5, 10, 5, 7, 7, 7, 4, 5, 5, 4 };
		PdfPTable table = new PdfPTable(columnWidths);
		table.setWidthPercentage(100);
		table.getDefaultCell().setUseAscender(true);
		table.getDefaultCell().setUseDescender(true);

		PdfPCell fecha = new PdfPCell(new Phrase(
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd " + " / " + " HH:mm:ss")), font));
		fecha.setBorder(Rectangle.NO_BORDER);
		fecha.setColspan(13);
		table.addCell(fecha);

		try {
			Image img = Image.getInstance(em.getLogo());
			img.scaleToFit(70, 70);
			PdfPCell encabezado = new PdfPCell(img);
			encabezado.setBorder(Rectangle.NO_BORDER);
			encabezado.setHorizontalAlignment(Element.ALIGN_CENTER);
			encabezado.setColspan(13);
			table.addCell(encabezado);
		} catch (BadElementException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		PdfPCell cell = new PdfPCell(new Phrase("Reporte de Accionistas Mayoritarios Acciones Tipo B", letra));
		cell.setBackgroundColor(GrayColor.GRAYBLACK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setColspan(13);
		table.addCell(cell);

		table.addCell(addcell("#", font, new GrayColor(0.75f)));
		table.addCell(addcell("Rut", font, new GrayColor(0.75f)));
		table.addCell(addcell("Nombre", font, new GrayColor(0.75f)));
		table.addCell(addcell("Acciones Tipo B", font, new GrayColor(0.75f)));
		table.addCell(addcell("Direccion", font, new GrayColor(0.75f)));
		table.addCell(addcell("Nacionalidad", font, new GrayColor(0.75f)));
		table.addCell(addcell("Apoderado", font, new GrayColor(0.75f)));
		table.addCell(addcell("Empresa", font, new GrayColor(0.75f)));
		table.addCell(addcell("Tipo Accionista", font, new GrayColor(0.75f)));
		table.addCell(addcell("% de Participacion", font, new GrayColor(0.75f)));
		table.addCell(addcell("Cta. Corriente", font, new GrayColor(0.75f)));
		table.addCell(addcell("Banco", font, new GrayColor(0.75f)));
		table.addCell(addcell("Estado", font, new GrayColor(0.75f)));
		table.setHeaderRows(2);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		int totalAccionesB = 0;
		for (int i = 0; i < acc.size(); i++) {
			table.addCell(new PdfPCell(new Phrase((i + 1) + "", font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getRut(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getNombre() + " "
					+ acc.get(i).getApellido_paterno() + " " + acc.get(i).getApellido_materno(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getCantidad_acciones_serie_b() + "", font)));
			totalAccionesB += acc.get(i).getCantidad_acciones_serie_b();
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getDireccion() + ", " + acc.get(i).getComuna()
					+ ", " + acc.get(i).getCiudad(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getNacionalidad().getNacionalidad(), font)));
			if (acc.get(i).getApoderado() == null) {
				table.addCell(new PdfPCell(new Phrase("/*Sin Apoderado*/", font)));
			} else {
				table.addCell(new PdfPCell(new Phrase(acc.get(i).getApoderado().getNombre() + " "
						+ acc.get(i).getApoderado().getApellido_paterno() + " "
						+ acc.get(i).getApoderado().getApellido_materno(), font)));
			}
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getEmpresa().getRazonSocial(), font)));
			table.addCell(new PdfPCell(
					new Phrase(acc.get(i).getTipoAccionista().getDescripcion_tipo_accionista(), font)));
			table.addCell(new PdfPCell(new Phrase(acc.get(i).getPorcentaje_participacion() + "%", font)));
			String numeroC = "/*Sin Cta. Corriente*/", nombreB = "/*Sin Banco*/";
			for(CuentaCorriente c : acc.get(i).getCuentaCorriente()) {
				numeroC = c.getNumeroCuenta()+"";
				nombreB = c.getNombreBanco();
				break;
			}
			table.addCell(new PdfPCell(new Phrase(numeroC, font)));
			table.addCell(new PdfPCell(new Phrase(nombreB, font)));
			if (acc.get(i).getEstado() == 0) {
				table.addCell(new PdfPCell(new Phrase("Eliminado", font)));
			} else {
				table.addCell(new PdfPCell(new Phrase("Activo", font)));
			}
		}
		
		PdfPCell total = new PdfPCell();
		total.setBorder(Rectangle.NO_BORDER);
		total.setColspan(13);
		table.addCell(addcell("", font, new GrayColor(0.57f)));
		table.addCell(addcell("", font, new GrayColor(0.57f)));
		table.addCell(addcell("Total: ", font, new GrayColor(0.57f)));
		table.addCell(addcell(totalAccionesB + "", font, new GrayColor(0.57f)));
		table.addCell(total);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();

		try {
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			respuesta = "1";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respuesta;
	}
	
	public String reporteDividendo() {
		String respuesta = "";

		// ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_VIEW);
		LOG.info("Call: " + "Reporte PDF Dividendo -- desde controller");
		List<Dividendo> dividendo = dividendoService.listAllDividendoEntity();
		Empresa em = empresaService.findEmpresaActiva();

		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaPDF);
		ruta += "\\Dividendo\\Reporte_Dividendo_" + FechaActual + "_" + HoraActual + ".pdf";
		
		File file = new File(ruta);
		file.getParentFile().mkdirs();
		Rectangle small = new Rectangle(800, 300);
		Font font = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYBLACK);
		Font letra = new Font(FontFamily.HELVETICA, 5, Font.NORMAL, GrayColor.GRAYWHITE);
		Document document = new Document(small, 5, 5, 50, 5);
		try {
			PdfWriter.getInstance(document, new FileOutputStream(ruta));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.open();

		float[] columnWidths = { 2, 5, 5, 7, 8, 5, 5, 5, 5, 7, 7};
		PdfPTable table = new PdfPTable(columnWidths);
		table.setWidthPercentage(100);
		table.getDefaultCell().setUseAscender(true);
		table.getDefaultCell().setUseDescender(true);

		PdfPCell fecha = new PdfPCell(new Phrase(
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd " + " / " + " HH:mm:ss")), font));
		fecha.setBorder(Rectangle.NO_BORDER);
		fecha.setColspan(11);
		table.addCell(fecha);

		try {
			Image img = Image.getInstance(em.getLogo());
			img.scaleToFit(70, 70);
			PdfPCell encabezado = new PdfPCell(img);
			encabezado.setBorder(Rectangle.NO_BORDER);
			encabezado.setHorizontalAlignment(Element.ALIGN_CENTER);
			encabezado.setColspan(11);
			table.addCell(encabezado);
		} catch (BadElementException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		PdfPCell cell = new PdfPCell(new Phrase("Reporte de Herederos", letra));
		cell.setBackgroundColor(GrayColor.GRAYBLACK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setColspan(11);
		table.addCell(cell);

		table.addCell(addcell("#", font, new GrayColor(0.75f)));
		table.addCell(addcell("ID Junta Accionistas", font, new GrayColor(0.75f)));
		table.addCell(addcell("ID Dividendo", font, new GrayColor(0.75f)));
		table.addCell(addcell("Rut Accionista", font, new GrayColor(0.75f)));
		table.addCell(addcell("Nombre Accionista", font, new GrayColor(0.75f)));
		table.addCell(addcell("Dividendo Total Acciones Tipo A", font, new GrayColor(0.75f)));
		table.addCell(addcell("Dividendo Total Acciones Tipo B", font, new GrayColor(0.75f)));
		table.addCell(addcell("Dividendo Total por Acciones", font, new GrayColor(0.75f)));
		table.addCell(addcell("Monto a Pagar", font, new GrayColor(0.75f)));
		table.addCell(addcell("Cta. Corriente", font, new GrayColor(0.75f)));
		table.addCell(addcell("Banco", font, new GrayColor(0.75f)));
		table.setHeaderRows(2);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		
		for (int i = 0; i < dividendo.size(); i++) {
			table.addCell(new PdfPCell(new Phrase((i + 1) + "", font)));
			table.addCell(new PdfPCell(new Phrase(dividendo.get(i).getJuntaAccionista().getIdJuntaAccionista() + "", font)));
			table.addCell(new PdfPCell(new Phrase(dividendo.get(i).getIdDividendo() + "", font)));
			table.addCell(new PdfPCell(new Phrase(dividendo.get(i).getAccionista().getRut(), font)));
			table.addCell(new PdfPCell(new Phrase(dividendo.get(i).getAccionista().getNombre() + " "
					+ dividendo.get(i).getAccionista().getApellido_paterno() + " " + dividendo.get(i).getAccionista().getApellido_materno(), font)));
			table.addCell(new PdfPCell(new Phrase("$" + dividendo.get(i).getDividendoAccionA(), font)));
			table.addCell(new PdfPCell(new Phrase("$" + dividendo.get(i).getDividendoAccionB(), font)));
			table.addCell(new PdfPCell(new Phrase("$" + dividendo.get(i).getDividendoTotal(), font)));
			table.addCell(new PdfPCell(new Phrase("$" + dividendo.get(i).getDividendoTotal(), font)));
			String numeroC = "/*Sin Cta. Corriente*/", nombreB = "/*Sin Banco*/";
			for(CuentaCorriente c : dividendo.get(i).getAccionista().getCuentaCorriente()) {
				numeroC = c.getNumeroCuenta()+"";
				nombreB = c.getNombreBanco();
				break;
			}
			table.addCell(new PdfPCell(new Phrase(numeroC, font)));
			table.addCell(new PdfPCell(new Phrase(nombreB, font)));
		}

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();

		try {
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			respuesta = "1";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respuesta;
	}
	
	public PdfPCell addcell(String texto, Font fuente, GrayColor color) {
		PdfPCell celda = new PdfPCell(new Phrase(texto, fuente));
		celda.setBackgroundColor(color);
		return celda;

	}
}
