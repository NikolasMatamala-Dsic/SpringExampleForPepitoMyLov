package com.accionistas.accionsis.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.accionistas.accionsis.constant.ViewConstant;

/**
 * Clase ErrorsController.
 */
@ControllerAdvice
public class ErrorsController {

	/**
	 * Muestra errores internos del server.
	 *
	 * @return the string
	 */
	@ExceptionHandler(Exception.class)
	public String showInternalServerError() {
		return ViewConstant.ISE_VIEW;
	}
	
}
