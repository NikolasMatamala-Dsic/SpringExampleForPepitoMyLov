package com.accionistas.accionsis.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.model.AsistenciaJuntaAccionistaModel;
import com.accionistas.accionsis.service.AsistenciaJuntaAccionistaService;

//TODO: Auto-generated Javadoc
/*
 * Clase AsistenciaJuntaAccionistaController
 */
@Controller
@RequestMapping("/asistenciaJuntaAccionista")
public class AsistenciaJuntaAccionistaController {
	
	/*
	 * LOG constante
	 */
	private static final Log LOG = LogFactory.getLog(AsistenciaJuntaAccionistaController.class);
	
	/*
	 * El AsistenciaJuntaAccionistaService
	 */
	@Autowired
	@Qualifier("AsistenciaJuntaAccionistaServiceImp")
	private AsistenciaJuntaAccionistaService asistenciaJuntaAccionistaService;
	
	/*
	 * Listar todos las AsistenciaJuntaAccionista
	 * @return el model and view
	 */
	@GetMapping("/listarAsistenciaJuntaAccionista")
	public ModelAndView listAllAsistenciaJuntaAccionista() {
		LOG.info("ListaAsistenciaJunta: " + "listAllAsistenciaJuntaAccionista() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.JUNTA_ACCIONISTAS_VIEW);
		mav.addObject("listarAsistenciaJunta", asistenciaJuntaAccionistaService.listAllAsistenciaJuntaAccionista());
		return mav;
	}
	
	/*
	 * Guarda AsistenciaJuntaAccionista
	 */
	@PostMapping("/addAsistenciaJuntaAccionista")
	public String addAsistenciaJuntaAccionista(@ModelAttribute(name="asistenciaJuntaAccionistaModel") AsistenciaJuntaAccionistaModel asistenciaJuntaAccionistaModel, Model model) {
		LOG.info("Call: " + "addAsistenciaJuntaAccionista()" + "--Param: " + asistenciaJuntaAccionistaModel.toString()); 
		if(null != asistenciaJuntaAccionistaService.addAsistenciaJuntaAccionista(asistenciaJuntaAccionistaModel)) {
			model.addAttribute("result", 1);
		}else {
			model.addAttribute("result", 0);
		}
		return "";
	}
	
	/*
	 * Genera % de asistencia en AsistenciaJuntaAccionista
	 */
	@PostMapping("/asistencia")
	public String AsistenciaJunta () {
		
		return null;
	}

}
