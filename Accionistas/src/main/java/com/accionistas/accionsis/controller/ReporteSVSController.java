package com.accionistas.accionsis.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.entity.Empresa;
import com.accionistas.accionsis.model.AccionistaModel;
import com.accionistas.accionsis.model.TraspasoaccionesModel;
import com.accionistas.accionsis.service.AccionistaService;
import com.accionistas.accionsis.service.EmpresaService;
import com.accionistas.accionsis.service.TraspasoaccionesService;
import com.accionistas.accionsis.utility.RutaReportes;




@Controller
@RequestMapping("reporteSVS")
public class ReporteSVSController {
	
	@Autowired
	@Qualifier("accionistaServiceImp")
	private AccionistaService accionistaService;
	
	@Autowired
	@Qualifier("empresaServiceImp")
	private EmpresaService empresaService;
	
	@Autowired
	@Qualifier("traspasoaccionesServiceImp")
	private TraspasoaccionesService traspasoaccionesService;
	
	@RequestMapping(value = "/FiltroSvs", method = RequestMethod.GET)
	public @ResponseBody String FiltroSVS(@RequestParam(name="filtroSVS") String filtroSVS) {		
		String respuesta = "";
		switch(filtroSVS) {
			case "reporteAB":
				respuesta = reporteSVS();
				break;
			case "reporteA":
				respuesta = reporteSVSSerieA();
				break;
			case "reporteB":
				respuesta = reporteSVSSerieB();
				break;
			default:
				respuesta = "2";
				break;
		}
		
		return respuesta;
	}
	
	public String reporteSVS() {
		
		String respuesta = "";
		int cantidadDeRegistros = 0;
		List<AccionistaModel> acc = accionistaService.listAllAccionista_active();
		Empresa em = empresaService.findEmpresaActiva();
		
		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMM"));
		
		String outputFile = RutaReportes.verificarRuta(RutaReportes.rutaTXT); 
		outputFile += "\\ReporteSVS\\RGAC" + FechaActual +".txt";
		File alreadyExists = new File(outputFile);
		if(alreadyExists.exists()) {
			if(!alreadyExists.delete()) {System.out.println("Un error ocurrio al eliminar el archivo: "+outputFile);}
		}
		alreadyExists.getParentFile().mkdirs();
		
		
		
		try {
			String rutSA = em.getRut().replace(".", "");
			rutSA = rutSA.replace("-", "");
			rutSA = String.format("%10s", rutSA).replace(' ','0');    
			FileWriter fww = new FileWriter(outputFile,true);	
			BufferedWriter fw = new BufferedWriter(fww);
			
			//Registo 1 Empresa Activa
			fw.write("1");
			fw.write(FechaActual);
			String razonSocialSA = em.getRazonSocial();
			razonSocialSA = String.format("%-100s", razonSocialSA.toUpperCase());
			fw.write(rutSA);
			fw.write(razonSocialSA);
			String filler = String.format("%-126s", "");
			fw.write(filler);
			fw.newLine();
			cantidadDeRegistros++;
			
			//Registro 2 Accionistas
            String rut = "";
            for(int i = 0; i < acc.size(); i++) {
            	String accionA = "";
            	String accionB = "";
            	fw.write("2");
            	if(acc.get(i).getCantidad_acciones_serie_aModel() == 0 && acc.get(i).getCantidad_acciones_serie_bModel() >= 1) {
            		accionB = "B";
            		accionB = String.format("%-2s", accionB.toUpperCase());
            		
            	}else if(acc.get(i).getCantidad_acciones_serie_aModel() >= 1 && acc.get(i).getCantidad_acciones_serie_bModel() == 0){
            		accionA = "A";
            		accionA = String.format("%-2s", accionA.toUpperCase()).replace(' ', ' ');
            		
            	}else if(acc.get(i).getCantidad_acciones_serie_aModel() >= 1 && acc.get(i).getCantidad_acciones_serie_bModel() >= 1){
            		accionB = "B";
            		accionB = String.format("%-2s", accionB.toUpperCase());
            		accionA = "A";
            		accionA = String.format("%-2s", accionA.toUpperCase()).replace(' ', ' ');
            	}
            	
            	if(accionA != "" && accionB == "") {
            		fw.write(accionA);
            	}else if(accionA == "" && accionB != "") {
            		fw.write(accionB);
            	}else if(accionA != "" && accionB != "") {
            		boolean a = true;
            		for(int it = 0; it<2 ; it++) {
            			if(a) {
            				fw.write(accionA);
            				a = false;
            			}else {
            				fw.write("2");
            				fw.write(accionB);
            				a = true;
            			}
            		
            			rut = acc.get(i).getRutModel().replace(".", "");
    	                rut = rut.replace("-", "");                
    	                rut = String.format("%10s", rut.toUpperCase()).replace(' ','0');                                
    	                fw.write(rut);
    	                if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 1) {
                    		fw.write("A");
                    	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 2) {
                    		fw.write("B");
                    	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 8) {
                    		fw.write("C");
                    	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 6) {
                    		fw.write("D");
                    	}
    	                String nombre = acc.get(i).getNombreModel();
    	                nombre = String.format("%-80s", nombre.toUpperCase());
    	                fw.write(nombre);
    	                String apellidoP = acc.get(i).getApellido_paternoModel();
    	                apellidoP = String.format("%-20s", apellidoP.toUpperCase());
    	                fw.write(apellidoP);
    	                String apellidoM = acc.get(i).getApellido_paternoModel();
    	                apellidoM = String.format("%-20s", apellidoM.toUpperCase());
    	                fw.write(apellidoM);
    	                String direccion = acc.get(i).getDireccionModel().replace("#", "nro ");
    	                direccion = direccion.replace("n°", "nro ");
    	                direccion = direccion.replace("N°", "nro ");
    	                direccion = String.format("%-40s", direccion.toUpperCase());
    	                fw.write(direccion);
    	                String ciudad = acc.get(i).getCiudadModel();
    	                ciudad = String.format("%-15s", ciudad.toUpperCase());
    	                fw.write(ciudad);
    	                String comuna = acc.get(i).getComunaModel();
    	                comuna = String.format("%-15s", comuna.toUpperCase());
    	                fw.write(comuna);
    	                String region = acc.get(i).getRegionModel().getIdRegionModel() + "";
    	                region = String.format("%2s", region.toUpperCase()).replace(' ', '0');
    	                fw.write(region);
    	                
    	                String accionAA = acc.get(i).getCantidad_acciones_serie_aModel() + "";
    	                String accionBB = acc.get(i).getCantidad_acciones_serie_bModel() + "";
    	                String vacio = " ";
    	                
	                	if(accionAA.matches("[0-9]*")) {
    	                	fw.write("N");
    	                }else {
    	                	fw.write("P");
    	                }
	                	if(a) {
	                		//Acciones SUSCRITAS
	                		accionBB = String.format("%15s", accionBB.toUpperCase()).replace(' ', '0');
	    	                fw.write(accionBB);
	    	                vacio = String.format("%4s", vacio).replace(' ', '0');
	    	                fw.write(vacio);
	    	                
	    	                //Acciones PAGO
	    	                accionBB = String.format("%15s", accionBB.toUpperCase()).replace(' ', '0');
	    	                fw.write(accionBB);
	    	                vacio = String.format("%4s", vacio).replace(' ', '0');
	    	                fw.write(vacio);
	                	}else {
	                		//Acciones SUSCRITAS
	                		accionAA = String.format("%15s", accionAA.toUpperCase()).replace(' ', '0');
	    	                fw.write(accionAA);
	    	                vacio = String.format("%4s", vacio).replace(' ', '0');
	    	                fw.write(vacio);
	    	                
	    	                //Acciones PAGO
	    	                accionAA = String.format("%15s", accionAA.toUpperCase()).replace(' ', '0');
	    	                fw.write(accionAA);
	    	                vacio = String.format("%4s", vacio).replace(' ', '0');
	    	                fw.write(vacio);
	                	}
    	                fw.newLine();
    	                cantidadDeRegistros++;
            		}
            	}
            	if(accionA == "" || accionB == "") {
            		rut = acc.get(i).getRutModel().replace(".", "");
	                rut = rut.replace("-", "");                
	                rut = String.format("%10s", rut.toUpperCase()).replace(' ','0');                                
	                fw.write(rut);
	                if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 1) {
                		fw.write("A");
                	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 2) {
                		fw.write("B");
                	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 8) {
                		fw.write("C");
                	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 6) {
                		fw.write("D");
                	}
	                String nombre = acc.get(i).getNombreModel();
	                nombre = String.format("%-80s", nombre.toUpperCase());
	                fw.write(nombre);
	                String apellidoP = acc.get(i).getApellido_paternoModel();
	                apellidoP = String.format("%-20s", apellidoP.toUpperCase());
	                fw.write(apellidoP);
	                String apellidoM = acc.get(i).getApellido_paternoModel();
	                apellidoM = String.format("%-20s", apellidoM.toUpperCase());
	                fw.write(apellidoM);
	                String direccion = acc.get(i).getDireccionModel().replace("#", "N°");
	                direccion = direccion.replace("n°", "nro ");
	                direccion = direccion.replace("N°", "nro ");
	                direccion = String.format("%-40s", direccion.toUpperCase());
	                fw.write(direccion);
	                String ciudad = acc.get(i).getCiudadModel();
	                ciudad = String.format("%-15s", ciudad.toUpperCase());
	                fw.write(ciudad);
	                String comuna = acc.get(i).getComunaModel();
	                comuna = String.format("%-15s", comuna.toUpperCase());
	                fw.write(comuna);
	                String region = acc.get(i).getRegionModel().getIdRegionModel() + "";
	                region = String.format("%2s", region.toUpperCase()).replace(' ', '0');
	                fw.write(region);
	                
	                String accionAA = acc.get(i).getCantidad_acciones_serie_aModel() + "";
	                String accionBB = acc.get(i).getCantidad_acciones_serie_bModel() + "";
	                String vacio = " ";
	                
                	if(accionAA.matches("[0-9]*")) {
	                	fw.write("N");
	                }else {
	                	fw.write("P");
	                }
                	if(accionA != "") {
                		//Acciones SUSCRITAS
                		accionAA = String.format("%15s", accionAA.toUpperCase()).replace(' ', '0');
    	                fw.write(accionAA);
    	                vacio = String.format("%4s", vacio.toUpperCase()).replace(' ', '0');
    	                fw.write(vacio);
    	                
    	              //Acciones PAGO
    	                accionAA = String.format("%15s", accionAA.toUpperCase()).replace(' ', '0');
    	                fw.write(accionAA);
    	                vacio = String.format("%4s", vacio.toUpperCase()).replace(' ', '0');
    	                fw.write(vacio);
                	}else {
                		//Acciones SUSCRITAS
                		accionBB = String.format("%15s", accionBB.toUpperCase()).replace(' ', '0');
    	                fw.write(accionBB);
    	                vacio = String.format("%4s", vacio.toUpperCase()).replace(' ', '0');
    	                fw.write(vacio);
    	              //Acciones PAGO
    	                accionBB = String.format("%15s", accionBB.toUpperCase()).replace(' ', '0');
    	                fw.write(accionBB);
    	                vacio = String.format("%4s", vacio).replace(' ', '0');
    	                fw.write(vacio);
                	}
            			                	                	               
	                fw.newLine();
	                cantidadDeRegistros++;
            	}
            }
            
            //Regristro 3 Compra de acciones
        	List<TraspasoaccionesModel> traspaso = traspasoaccionesService.listAllTraspasosacciones();
        	
        	String accionVA = "";
        	String accionVB = "";
        	for(int t = 0; t < traspaso.size(); t++) {
        		fw.write("3");
            	if(traspaso.get(t).getCantidad_acciones_vendidas_aModel() == 0 && traspaso.get(t).getCantidad_acciones_vendidas_bModel() >= 1) {
            		accionVB = "B";
            		accionVB = String.format("%-2s", accionVB.toUpperCase());
            		
            	}else if(traspaso.get(t).getCantidad_acciones_vendidas_aModel() >= 1 && traspaso.get(t).getCantidad_acciones_vendidas_bModel() == 0){
            		accionVA = "A";
            		accionVA = String.format("%-2s", accionVA.toUpperCase()).replace(' ', ' ');
            		
            	}else if(traspaso.get(t).getCantidad_acciones_vendidas_aModel() >= 1 && traspaso.get(t).getCantidad_acciones_vendidas_bModel() >= 1){
            		accionVB = "B";
            		accionVB = String.format("%-2s", accionVB.toUpperCase());
            		accionVA = "A";
            		accionVA = String.format("%-2s", accionVA.toUpperCase()).replace(' ', ' ');
            	}
            	
            	if(accionVA != "" && accionVB == "") {
            		fw.write(accionVA);
            	}else if(accionVA == "" && accionVB != "") {
            		fw.write(accionVB);
            	}else if(accionVA != "" && accionVB != "") {
            		boolean a = true;
            		for(int it = 0; it<2 ; it++) {
            			if(a) {
            				fw.write(accionVA);
            				a = false;
            			}else {
            				fw.write("3");
            				fw.write(accionVB);
            				a = true;
            			}
            			String rutC = "";
            			String rutV = "";
            		
            			rutC = traspaso.get(t).getRut_accionista_compradorModel().getRutModel().replace(".", "");
            			rutC = rutC.replace("-", "");                
    	                rutC = String.format("%10s", rutC.toUpperCase()).replace(' ','0');                                
    	                fw.write(rutC);
    	                rutV = traspaso.get(t).getRut_accionista_vendedorModel().getRutModel().replace(".", "");
    	                rutV = rutV.replace("-", "");                
    	                rutV = String.format("%10s", rutV.toUpperCase()).replace(' ','0');                                
    	                fw.write(rutV);
    	                
    	                if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 1) {
                    		fw.write("A");
                    	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 2) {
                    		fw.write("B");
                    	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 8) {
                    		fw.write("C");
                    	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 6) {
                    		fw.write("D");
                    	}
    	                String nombre = traspaso.get(t).getRut_accionista_compradorModel().getNombreModel();
    	                nombre = String.format("%-80s", nombre.toUpperCase());
    	                fw.write(nombre);
    	                String apellidoP = traspaso.get(t).getRut_accionista_compradorModel().getApellido_paternoModel();
    	                apellidoP = String.format("%-20s", apellidoP.toUpperCase());
    	                fw.write(apellidoP);
    	                String apellidoM = traspaso.get(t).getRut_accionista_compradorModel().getApellido_paternoModel();
    	                apellidoM = String.format("%-20s", apellidoM.toUpperCase());
    	                fw.write(apellidoM);
    	                
    	                String accionAA = traspaso.get(t).getCantidad_acciones_vendidas_aModel() + "";
    	                String accionBB = traspaso.get(t).getCantidad_acciones_vendidas_bModel() + "";
    	                String vacio = " ";
    	                
	                	if(accionAA.matches("[0-9]*")) {
    	                	fw.write("N");
    	                }else {
    	                	fw.write("P");
    	                }
	                	
	                	
	                	if(a) {
	                		//Acciones SUSCRITAS
	                		accionBB = String.format("%15s", accionBB.toUpperCase()).replace(' ', '0');
	    	                fw.write(accionBB);
	    	                vacio = String.format("%4s", vacio).replace(' ', '0');
	    	                fw.write(vacio);
	    	                
	    	              //Acciones PAGO
	    	                accionBB = String.format("%15s", accionBB.toUpperCase()).replace(' ', '0');
	    	                fw.write(accionBB);
	    	                vacio = String.format("%4s", vacio).replace(' ', '0');
	    	                fw.write(vacio);
	                		
	                	}else {
	    	              //Acciones SUSCRITAS
	                		accionAA = String.format("%15s", accionAA.toUpperCase()).replace(' ', '0');
	    	                fw.write(accionAA);
	    	                vacio = String.format("%4s", vacio).replace(' ', '0');
	    	                fw.write(vacio);
	    	                
	    	              //Acciones PAGO
	    	                accionAA = String.format("%15s", accionAA.toUpperCase()).replace(' ', '0');
	    	                fw.write(accionAA);
	    	                vacio = String.format("%4s", vacio).replace(' ', '0');
	    	                fw.write(vacio);
	                	}
	                	String filler2 = String.format("%-62s", "");
	        			fw.write(filler2);
    	                
    	                fw.newLine();
    	                cantidadDeRegistros++;
            		}
            	}
            	if(accionVA == "" || accionVB == "") {
            		String rutC = "";
        			String rutV = "";
            		rutC = traspaso.get(t).getRut_accionista_compradorModel().getRutModel().replace(".", "");
        			rutC = rutC.replace("-", "");                
	                rutC = String.format("%10s", rutC.toUpperCase()).replace(' ','0');                                
	                fw.write(rutC);
	                rutV = traspaso.get(t).getRut_accionista_vendedorModel().getRutModel().replace(".", "");
	                rutV = rutV.replace("-", "");                
	                rutV = String.format("%10s", rutV.toUpperCase()).replace(' ','0');                                
	                fw.write(rutV);
	                
	                if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 1) {
                		fw.write("A");
                	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 2) {
                		fw.write("B");
                	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 8) {
                		fw.write("C");
                	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 6) {
                		fw.write("D");
                	}
	                String nombre = traspaso.get(t).getRut_accionista_compradorModel().getNombreModel();
	                nombre = String.format("%-80s", nombre.toUpperCase());
	                fw.write(nombre);
	                String apellidoP = traspaso.get(t).getRut_accionista_compradorModel().getApellido_paternoModel();
	                apellidoP = String.format("%-20s", apellidoP.toUpperCase());
	                fw.write(apellidoP);
	                String apellidoM = traspaso.get(t).getRut_accionista_compradorModel().getApellido_paternoModel();
	                apellidoM = String.format("%-20s", apellidoM.toUpperCase());
	                fw.write(apellidoM);
	                
	                String accionAA = traspaso.get(t).getCantidad_acciones_vendidas_aModel() + "";
	                String accionBB = traspaso.get(t).getCantidad_acciones_vendidas_bModel() + "";
	                String vacio = " ";
	                
                	if(accionAA.matches("[0-9]*")) {
	                	fw.write("N");
	                }else {
	                	fw.write("P");
	                }
                	
                	if(accionVA != "") {
                		//Acciones SUSCRITAS
                		accionAA = String.format("%15s", accionAA.toUpperCase()).replace(' ', '0');
    	                fw.write(accionAA);
    	                vacio = String.format("%4s", vacio).replace(' ', '0');
    	                fw.write(vacio);
    	                
                		//Acciones PAGO
    	                accionAA = String.format("%15s", accionAA.toUpperCase()).replace(' ', '0');
    	                fw.write(accionAA);
    	                vacio = String.format("%4s", vacio).replace(' ', '0');
    	                fw.write(vacio);
                	}else {
                		//Acciones SUSCRITAS
                		accionBB = String.format("%15s", accionBB.toUpperCase()).replace(' ', '0');
    	                fw.write(accionBB);
    	                vacio = String.format("%4s", vacio).replace(' ', '0');
    	                fw.write(vacio);
    	                
    	              //Acciones PAGO
    	                accionBB = String.format("%15s", accionBB.toUpperCase()).replace(' ', '0');
    	                fw.write(accionBB);
    	                vacio = String.format("%4s", vacio).replace(' ', '0');
    	                fw.write(vacio);
                	}
                	String filler2 = String.format("%-62s", "");
        			fw.write(filler2);
	                
	                fw.newLine();
	                cantidadDeRegistros++;
	            }
        	}
        	cantidadDeRegistros++;
        	fw.write("4");
        	String lineas = String.format("%8s", cantidadDeRegistros).replace(' ', '0'); 
        	fw.write(lineas);
        	fw.close();	        	        	
            	
            	try {
            		 String content = FileUtils.readFileToString(alreadyExists, "UTF-8");
            		 content = content.replaceAll("ñ", "#");
            		 content = content.replaceAll("Ñ", "#");
            		 content = content.replaceAll("á", "a");
            		 content = content.replaceAll("Á", "A");
            		 content = content.replaceAll("é", "e");
            		 content = content.replaceAll("É", "E");
            		 content = content.replaceAll("í", "i");
            		 content = content.replaceAll("Í", "I");
            		 content = content.replaceAll("ó", "o");
            		 content = content.replaceAll("Ó", "O");
            		 content = content.replaceAll("ú", "u");
            		 content = content.replaceAll("Ú", "U");
            		 File tempFile = new File(outputFile);
            	     FileUtils.writeStringToFile(tempFile, content, "UTF-8");
            		                	                	
            	}catch (Exception e) {
    				// TODO: handle exception
    			}
        	
            
		}catch (IOException e) {
            e.printStackTrace();
        }
		
		try {
			Runtime.getRuntime().exec("cmd /c start " + outputFile);
			respuesta = "1";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return respuesta;
	}

	public String reporteSVSSerieA(){
		String respuesta = "";
		int cantidadDeRegistros = 0;
		List<AccionistaModel> acc = accionistaService.listAllAccionista_active();
		Empresa em = empresaService.findEmpresaActiva();
		
		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMM"));
		
		String outputFile = RutaReportes.verificarRuta(RutaReportes.rutaTXT); 
		outputFile += "\\ReporteSVS\\RGAC" + FechaActual +".txt";
		File alreadyExists = new File(outputFile);
		if(alreadyExists.exists()) {
			if(!alreadyExists.delete()) {System.out.println("Un error ocurrio al eliminar el archivo: "+outputFile);}
		}
		alreadyExists.getParentFile().mkdirs();
		
		
		
		try {
			String rutSA = em.getRut().replace(".", "");
			rutSA = rutSA.replace("-", "");
			rutSA = String.format("%10s", rutSA).replace(' ','0');    
			FileWriter fww = new FileWriter(outputFile,true);	
			BufferedWriter fw = new BufferedWriter(fww);
			
			//Registo 1 Empresa Activa
			fw.write("1");
			fw.write(FechaActual);
			String razonSocialSA = em.getRazonSocial();
			razonSocialSA = String.format("%-100s", razonSocialSA.toUpperCase());
			fw.write(rutSA);
			fw.write(razonSocialSA);
			String filler = String.format("%-126s", "");
			fw.write(filler);
			fw.newLine();
			cantidadDeRegistros++;
			
			//Registro 2 Accionistas
            String rut = "";
            for(int i = 0; i < acc.size(); i++) {
            	String accionA = "";
            	fw.write("2");
            	
        		accionA = "A";
        		accionA = String.format("%-2s", accionA.toUpperCase()).replace(' ', ' ');
            	fw.write(accionA);
            			
    			rut = acc.get(i).getRutModel().replace(".", "");
                rut = rut.replace("-", "");                
                rut = String.format("%10s", rut.toUpperCase()).replace(' ','0');                                
                fw.write(rut);
                if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 1) {
            		fw.write("A");
            	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 2) {
            		fw.write("B");
            	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 8) {
            		fw.write("C");
            	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 6) {
            		fw.write("D");
            	}
                String nombre = acc.get(i).getNombreModel();
                nombre = String.format("%-80s", nombre.toUpperCase());
                fw.write(nombre);
                String apellidoP = acc.get(i).getApellido_paternoModel();
                apellidoP = String.format("%-20s", apellidoP.toUpperCase());
                fw.write(apellidoP);
                String apellidoM = acc.get(i).getApellido_paternoModel();
                apellidoM = String.format("%-20s", apellidoM.toUpperCase());
                fw.write(apellidoM);
                String direccion = acc.get(i).getDireccionModel().replace("#", "nro ");
                direccion = direccion.replace("n°", "nro ");
                direccion = direccion.replace("N°", "nro ");
                direccion = String.format("%-40s", direccion.toUpperCase());
                fw.write(direccion);
                String ciudad = acc.get(i).getCiudadModel();
                ciudad = String.format("%-15s", ciudad.toUpperCase());
                fw.write(ciudad);
                String comuna = acc.get(i).getComunaModel();
                comuna = String.format("%-15s", comuna.toUpperCase());
                fw.write(comuna);
                String region = acc.get(i).getRegionModel().getIdRegionModel() + "";
                region = String.format("%2s", region.toUpperCase()).replace(' ', '0');
                fw.write(region);
                
                String accionAA = acc.get(i).getCantidad_acciones_serie_aModel() + "";
                String vacio = " ";
                
            	if(accionAA.matches("[0-9]*")) {
                	fw.write("N");
                }else {
                	fw.write("P");
                }
            	
        		accionAA = String.format("%15s", accionAA.toUpperCase()).replace(' ', '0');
                fw.write(accionAA);
                vacio = String.format("%4s", vacio).replace(' ', '0');
                fw.write(vacio);
	            
                fw.newLine();
                cantidadDeRegistros++;
            		
            	
            
            	if(accionA == "") {
            		rut = acc.get(i).getRutModel().replace(".", "");
                    rut = rut.replace("-", "");                
                    rut = String.format("%10s", rut.toUpperCase()).replace(' ','0');                                
                    fw.write(rut);
                    if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 1) {
                		fw.write("A");
                	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 2) {
                		fw.write("B");
                	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 8) {
                		fw.write("C");
                	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 6) {
                		fw.write("D");
                	}
                    nombre = String.format("%-80s", nombre);
                    fw.write(nombre);
                    apellidoP = String.format("%-20s", apellidoP);
                    fw.write(apellidoP);
                    apellidoM = String.format("%-20s", apellidoM);
                    fw.write(apellidoM);
                    String direccionA = acc.get(i).getDireccionModel().replace("#", "nro ");
                    direccionA = direccionA.replace("n°", "nro ");
                    direccionA = direccionA.replace("N°", "nro ");
                    direccionA = String.format("%-40s", direccionA.toUpperCase());
                    fw.write(direccionA);
                    ciudad = String.format("%-15s", ciudad.toUpperCase());
                    fw.write(ciudad);
                    comuna = String.format("%-15s", comuna.toUpperCase());
                    fw.write(comuna);
                    region = String.format("%2s", region.toUpperCase()).replace(' ', '0');
                    fw.write(region);
                    
                	if(accionAA.matches("[0-9]*")) {
                    	fw.write("N");
                    }else {
                    	fw.write("P");
                    }
                	
            		accionAA = String.format("%15s", accionAA.toUpperCase()).replace(' ', '0');
                    fw.write(accionAA);
                    vacio = String.format("%4s", vacio).replace(' ', '0');
                    fw.write(vacio);
    	            
                    fw.newLine();
                    cantidadDeRegistros++;
            	}
            }
            
            
            //Regristro 3 Compra de acciones
        	List<TraspasoaccionesModel> traspaso = traspasoaccionesService.listAllTraspasosacciones();
        	
        	String accionVA = "";
        	for(int t = 0; t < traspaso.size(); t++) {
        		fw.write("3");
            	
        		accionVA = "A";
        		accionVA = String.format("%-2s", accionVA.toUpperCase()).replace(' ', ' ');
        		fw.write(accionVA);
            	
    			String rutC = "";
    			String rutV = "";
    		
    			rutC = traspaso.get(t).getRut_accionista_compradorModel().getRutModel().replace(".", "");
    			rutC = rutC.replace("-", "");                
                rutC = String.format("%10s", rutC.toUpperCase()).replace(' ','0');                                
                fw.write(rutC);
                rutV = traspaso.get(t).getRut_accionista_vendedorModel().getRutModel().replace(".", "");
                rutV = rutV.replace("-", "");                
                rutV = String.format("%10s", rutV.toUpperCase()).replace(' ','0');                                
                fw.write(rutV);
                
                if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 1) {
            		fw.write("A");
            	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 2) {
            		fw.write("B");
            	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 8) {
            		fw.write("C");
            	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 6) {
            		fw.write("D");
            	}
                String nombre = traspaso.get(t).getRut_accionista_compradorModel().getNombreModel();
                nombre = String.format("%-80s", nombre.toUpperCase());
                fw.write(nombre);
                String apellidoP = traspaso.get(t).getRut_accionista_compradorModel().getApellido_paternoModel();
                apellidoP = String.format("%-20s", apellidoP.toUpperCase());
                fw.write(apellidoP);
                String apellidoM = traspaso.get(t).getRut_accionista_compradorModel().getApellido_paternoModel();
                apellidoM = String.format("%-20s", apellidoM.toUpperCase());
                fw.write(apellidoM);
                
                String accionAA = traspaso.get(t).getCantidad_acciones_vendidas_aModel() + "";
                String vacio = " ";
                
            	if(accionAA.matches("[0-9]*")) {
                	fw.write("N");
                }else {
                	fw.write("P");
                }
            	
        		accionAA = String.format("%15s", accionAA.toUpperCase()).replace(' ', '0');
                fw.write(accionAA);
                vacio = String.format("%4s", vacio).replace(' ', '0');
                fw.write(vacio);
	                
	              
            	String filler2 = String.format("%-62s", "");
    			fw.write(filler2);
                
                fw.newLine();
                cantidadDeRegistros++;
    		
            	
            	if(accionVA == "") {
            		rutC = traspaso.get(t).getRut_accionista_compradorModel().getRutModel().replace(".", "");
        			rutC = rutC.replace("-", "");                
	                rutC = String.format("%10s", rutC.toUpperCase()).replace(' ','0');                                
	                fw.write(rutC);
	                rutV = traspaso.get(t).getRut_accionista_vendedorModel().getRutModel().replace(".", "");
	                rutV = rutV.replace("-", "");                
	                rutV = String.format("%10s", rutV.toUpperCase()).replace(' ','0');                                
	                fw.write(rutV);
	                
	                if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 1) {
                		fw.write("A");
                	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 2) {
                		fw.write("B");
                	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 8) {
                		fw.write("C");
                	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 6) {
                		fw.write("D");
                	}
	                nombre = String.format("%-80s", nombre.toUpperCase());
	                fw.write(nombre);
	                apellidoP = String.format("%-20s", apellidoP.toUpperCase());
	                fw.write(apellidoP);
	                apellidoM = String.format("%-20s", apellidoM.toUpperCase());
	                fw.write(apellidoM);
	                
                	if(accionAA.matches("[0-9]*")) {
	                	fw.write("N");
	                }else {
	                	fw.write("P");
	                }
                	
            		accionAA = String.format("%15s", accionAA.toUpperCase()).replace(' ', '0');
	                fw.write(accionAA);
	                vacio = String.format("%4s", vacio).replace(' ', '0');
	                fw.write(vacio);
    	                
        			fw.write(filler2);
	                
	                fw.newLine();
	                cantidadDeRegistros++;
	            }
        	}
        	cantidadDeRegistros++;
        	fw.write("4");
        	String lineas = String.format("%8s", cantidadDeRegistros).replace(' ', '0'); 
        	fw.write(lineas);
        	fw.close();	        	        	
            	
            	try {
            		 String content = FileUtils.readFileToString(alreadyExists, "UTF-8");
            		 content = content.replaceAll("ñ", "#");
            		 content = content.replaceAll("Ñ", "#");
            		 content = content.replaceAll("á", "a");
            		 content = content.replaceAll("Á", "A");
            		 content = content.replaceAll("é", "e");
            		 content = content.replaceAll("É", "E");
            		 content = content.replaceAll("í", "i");
            		 content = content.replaceAll("Í", "I");
            		 content = content.replaceAll("ó", "o");
            		 content = content.replaceAll("Ó", "O");
            		 content = content.replaceAll("ú", "u");
            		 content = content.replaceAll("Ú", "U");
            		 File tempFile = new File(outputFile);
            	     FileUtils.writeStringToFile(tempFile, content, "UTF-8");
            		                	                	
            	}catch (Exception e) {
    				// TODO: handle exception
    			}
        	
            
		}catch (IOException e) {
            e.printStackTrace();
        }
		
		try {
			Runtime.getRuntime().exec("cmd /c start " + outputFile);
			respuesta = "1";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return respuesta;
		
	}

	public String reporteSVSSerieB() {

		String respuesta = "";
		int cantidadDeRegistros = 0;
		List<AccionistaModel> acc = accionistaService.listAllAccionista_active();
		Empresa em = empresaService.findEmpresaActiva();
		
		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMM"));
		
		String outputFile = RutaReportes.verificarRuta(RutaReportes.rutaTXT); 
		outputFile += "\\ReporteSVS\\RGAC" + FechaActual +".txt";
		File alreadyExists = new File(outputFile);
		if(alreadyExists.exists()) {
			if(!alreadyExists.delete()) {System.out.println("Un error ocurrio al eliminar el archivo: "+outputFile);}
		}
		alreadyExists.getParentFile().mkdirs();
		
		
		
		try {
			String rutSA = em.getRut().replace(".", "");
			rutSA = rutSA.replace("-", "");
			rutSA = String.format("%10s", rutSA).replace(' ','0');    
			FileWriter fww = new FileWriter(outputFile,true);	
			BufferedWriter fw = new BufferedWriter(fww);
			
			//Registo 1 Empresa Activa
			fw.write("1");
			fw.write(FechaActual);
			String razonSocialSA = em.getRazonSocial();
			razonSocialSA = String.format("%-100s", razonSocialSA.toUpperCase());
			fw.write(rutSA);
			fw.write(razonSocialSA);
			String filler = String.format("%-126s", "");
			fw.write(filler);
			fw.newLine();
			cantidadDeRegistros++;
			
			//Registro 2 Accionistas
            String rut = "";
            for(int i = 0; i < acc.size(); i++) {
            	String accionB = "";
            	fw.write("2");
            	
        		accionB = "B";
        		accionB = String.format("%-2s", accionB.toUpperCase()).replace(' ', ' ');
            	fw.write(accionB);
            			
    			rut = acc.get(i).getRutModel().replace(".", "");
                rut = rut.replace("-", "");                
                rut = String.format("%10s", rut.toUpperCase()).replace(' ','0');                                
                fw.write(rut);
                if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 1) {
            		fw.write("A");
            	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 2) {
            		fw.write("B");
            	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 8) {
            		fw.write("C");
            	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 6) {
            		fw.write("D");
            	}
                String nombre = acc.get(i).getNombreModel();
                nombre = String.format("%-80s", nombre.toUpperCase());
                fw.write(nombre);
                String apellidoP = acc.get(i).getApellido_paternoModel();
                apellidoP = String.format("%-20s", apellidoP.toUpperCase());
                fw.write(apellidoP);
                String apellidoM = acc.get(i).getApellido_paternoModel();
                apellidoM = String.format("%-20s", apellidoM.toUpperCase());
                fw.write(apellidoM);
                String direccion = acc.get(i).getDireccionModel().replace("#", "nro ");
                direccion = direccion.replace("n°", "nro ");
                direccion = direccion.replace("N°", "nro ");
                direccion = String.format("%-40s", direccion.toUpperCase());
                fw.write(direccion);
                String ciudad = acc.get(i).getCiudadModel();
                ciudad = String.format("%-15s", ciudad.toUpperCase());
                fw.write(ciudad);
                String comuna = acc.get(i).getComunaModel();
                comuna = String.format("%-15s", comuna.toUpperCase());
                fw.write(comuna);
                String region = acc.get(i).getRegionModel().getIdRegionModel() + "";
                region = String.format("%2s", region.toUpperCase()).replace(' ', '0');
                fw.write(region);
                
                String accionBB = acc.get(i).getCantidad_acciones_serie_bModel() + "";
                String vacio = " ";
                
            	if(accionBB.matches("[0-9]*")) {
                	fw.write("N");
                }else {
                	fw.write("P");
                }
            	
            	accionBB = String.format("%15s", accionBB.toUpperCase()).replace(' ', '0');
                fw.write(accionBB);
                vacio = String.format("%4s", vacio).replace(' ', '0');
                fw.write(vacio);
	            
                fw.newLine();
                cantidadDeRegistros++;
            		
            	
            
            	if(accionB == "") {
            		rut = acc.get(i).getRutModel().replace(".", "");
                    rut = rut.replace("-", "");                
                    rut = String.format("%10s", rut.toUpperCase()).replace(' ','0');                                
                    fw.write(rut);
                    if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 1) {
                		fw.write("A");
                	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 2) {
                		fw.write("B");
                	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 8) {
                		fw.write("C");
                	}else if(acc.get(i).getTipoAccionistaModel().getIdTipoAccionistaModel() == 6) {
                		fw.write("D");
                	}
                    nombre = String.format("%-80s", nombre.toUpperCase());
                    fw.write(nombre);
                    apellidoP = String.format("%-20s", apellidoP.toUpperCase());
                    fw.write(apellidoP);
                    apellidoM = String.format("%-20s", apellidoM.toUpperCase());
                    fw.write(apellidoM);
                    String direccionA = acc.get(i).getDireccionModel().replace("#", "nro ");
                    direccionA = direccionA.replace("n°", "nro ");
                    direccionA = direccionA.replace("N°", "nro ");
                    direccionA = String.format("%-40s", direccionA.toUpperCase());
                    fw.write(direccionA);
                    ciudad = String.format("%-15s", ciudad.toUpperCase());
                    fw.write(ciudad);
                    comuna = String.format("%-15s", comuna.toUpperCase());
                    fw.write(comuna);
                    region = String.format("%2s", region.toUpperCase()).replace(' ', '0');
                    fw.write(region);
                    
                	if(accionBB.matches("[0-9]*")) {
                    	fw.write("N");
                    }else {
                    	fw.write("P");
                    }
                	
                	accionBB = String.format("%15s", accionBB.toUpperCase()).replace(' ', '0');
                    fw.write(accionBB);
                    vacio = String.format("%4s", vacio).replace(' ', '0');
                    fw.write(vacio);
    	            
                    fw.newLine();
                    cantidadDeRegistros++;
            	}
            }
            
            
            //Regristro 3 Compra de acciones
        	List<TraspasoaccionesModel> traspaso = traspasoaccionesService.listAllTraspasosacciones();
        	
        	String accionVB = "";
        	for(int t = 0; t < traspaso.size(); t++) {
        		fw.write("3");
            	
        		accionVB = "B";
        		accionVB = String.format("%-2s", accionVB.toUpperCase()).replace(' ', ' ');
        		fw.write(accionVB);
            	
    			String rutC = "";
    			String rutV = "";
    		
    			rutC = traspaso.get(t).getRut_accionista_compradorModel().getRutModel().replace(".", "");
    			rutC = rutC.replace("-", "");                
                rutC = String.format("%10s", rutC.toUpperCase()).replace(' ','0');                                
                fw.write(rutC);
                rutV = traspaso.get(t).getRut_accionista_vendedorModel().getRutModel().replace(".", "");
                rutV = rutV.replace("-", "");                
                rutV = String.format("%10s", rutV.toUpperCase()).replace(' ','0');                                
                fw.write(rutV);
                
                if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 1) {
            		fw.write("A");
            	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 2) {
            		fw.write("B");
            	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 8) {
            		fw.write("C");
            	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 6) {
            		fw.write("D");
            	}
                String nombre = traspaso.get(t).getRut_accionista_compradorModel().getNombreModel();
                nombre = String.format("%-80s", nombre.toUpperCase());
                fw.write(nombre);
                String apellidoP = traspaso.get(t).getRut_accionista_compradorModel().getApellido_paternoModel();
                apellidoP = String.format("%-20s", apellidoP.toUpperCase());
                fw.write(apellidoP);
                String apellidoM = traspaso.get(t).getRut_accionista_compradorModel().getApellido_paternoModel();
                apellidoM = String.format("%-20s", apellidoM.toUpperCase());
                fw.write(apellidoM);
                
                String accionBB = traspaso.get(t).getCantidad_acciones_vendidas_bModel() + "";
                String vacio = " ";
                
            	if(accionBB.matches("[0-9]*")) {
                	fw.write("N");
                }else {
                	fw.write("P");
                }
            	
            	accionBB = String.format("%15s", accionBB.toUpperCase()).replace(' ', '0');
                fw.write(accionBB);
                vacio = String.format("%4s", vacio).replace(' ', '0');
                fw.write(vacio);
	                
	              
            	String filler2 = String.format("%-62s", "");
    			fw.write(filler2);
                
                fw.newLine();
                cantidadDeRegistros++;
    		
            	
            	if(accionVB == "") {
            		rutC = traspaso.get(t).getRut_accionista_compradorModel().getRutModel().replace(".", "");
        			rutC = rutC.replace("-", "");                
	                rutC = String.format("%10s", rutC.toUpperCase()).replace(' ','0');                                
	                fw.write(rutC);
	                rutV = traspaso.get(t).getRut_accionista_vendedorModel().getRutModel().replace(".", "");
	                rutV = rutV.replace("-", "");                
	                rutV = String.format("%10s", rutV.toUpperCase()).replace(' ','0');                                
	                fw.write(rutV);
	                
	                if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 1) {
                		fw.write("A");
                	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 2) {
                		fw.write("B");
                	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 8) {
                		fw.write("C");
                	}else if(traspaso.get(t).getRut_accionista_compradorModel().getTipoAccionistaModel().getIdTipoAccionistaModel() == 6) {
                		fw.write("D");
                	}
	                nombre = String.format("%-80s", nombre.toUpperCase());
	                fw.write(nombre);
	                apellidoP = String.format("%-20s", apellidoP.toUpperCase());
	                fw.write(apellidoP);
	                apellidoM = String.format("%-20s", apellidoM.toUpperCase());
	                fw.write(apellidoM);
	                
                	if(accionBB.matches("[0-9]*")) {
	                	fw.write("N");
	                }else {
	                	fw.write("P");
	                }
                	
                	accionBB = String.format("%15s", accionBB.toUpperCase()).replace(' ', '0');
	                fw.write(accionBB);
	                vacio = String.format("%4s", vacio).replace(' ', '0');
	                fw.write(vacio);
    	                
        			fw.write(filler2);
	                
	                fw.newLine();
	                cantidadDeRegistros++;
	            }
        	}
        	cantidadDeRegistros++;
        	fw.write("4");
        	String lineas = String.format("%8s", cantidadDeRegistros).replace(' ', '0'); 
        	fw.write(lineas);
        	fw.close();	        	        	
            	
            	try {
            		 String content = FileUtils.readFileToString(alreadyExists, "UTF-8");
            		 content = content.replaceAll("ñ", "#");
            		 content = content.replaceAll("Ñ", "#");
            		 content = content.replaceAll("á", "a");
            		 content = content.replaceAll("Á", "A");
            		 content = content.replaceAll("é", "e");
            		 content = content.replaceAll("É", "E");
            		 content = content.replaceAll("í", "i");
            		 content = content.replaceAll("Í", "I");
            		 content = content.replaceAll("ó", "o");
            		 content = content.replaceAll("Ó", "O");
            		 content = content.replaceAll("ú", "u");
            		 content = content.replaceAll("Ú", "U");
            		 File tempFile = new File(outputFile);
            	     FileUtils.writeStringToFile(tempFile, content, "UTF-8");
            		                	                	
            	}catch (Exception e) {
    				// TODO: handle exception
    			}
        	
            
		}catch (IOException e) {
            e.printStackTrace();
        }
		
		try {
			Runtime.getRuntime().exec("cmd /c start " + outputFile);
			respuesta = "1";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return respuesta;
	}
	
	
	@RequestMapping(value = "/svsView", method = RequestMethod.GET)
	public ModelAndView reportesvs() {
		ModelAndView mav = new ModelAndView(ViewConstant.SVS_VIEW);
		return mav;
	}

}
