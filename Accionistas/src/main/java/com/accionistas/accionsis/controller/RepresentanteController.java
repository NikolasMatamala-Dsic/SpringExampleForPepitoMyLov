package com.accionistas.accionsis.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.component.RepresentanteConverter;
import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.entity.Representante;
import com.accionistas.accionsis.model.EmpresaModel;
import com.accionistas.accionsis.model.RegionesModel;
import com.accionistas.accionsis.model.RepresentanteModel;
import com.accionistas.accionsis.repository.RepresentanteJpaRepository;
import com.accionistas.accionsis.service.EmpresaService;
import com.accionistas.accionsis.service.RegionesService;
import com.accionistas.accionsis.service.RepresentanteService;

@Controller
@RequestMapping("representantes")
public class RepresentanteController {


	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(RepresentanteController.class);
		
		/** The representante service. */
		@Autowired
		@Qualifier("representanteServiceImp")
		private RepresentanteService representanteService;
		
		@Autowired
		@Qualifier("representanteConverter")
		private RepresentanteConverter representanteConverter;
		
		@Autowired
		@Qualifier("empresaServiceImp")
		private EmpresaService empresaService;
		
		@Autowired
		@Qualifier("regionesServiceImp")
		private RegionesService regionesService;
		
		/**
		 * Lista todos los representantes.
		 *
		 * @return the model and view
		 */
		@GetMapping("/listrepresentantes")
		public ModelAndView listAllRepresentantes() {
			LOG.info("Call: "+ "listAllRepresentantes() -- desde controller");
			ModelAndView mav = new ModelAndView(ViewConstant.REPRESENTANTE_VIEW);
			List<EmpresaModel> empresaModelList = new ArrayList<EmpresaModel>();
			empresaModelList = empresaService.listAllEmpresa();
			mav.addObject("empresas", empresaModelList);
			mav.addObject("representantes", representanteService.listAllRepresentante());
			return mav;
		}	
		
		@RequestMapping(value="/addrepresentante", method=RequestMethod.GET)
		public @ResponseBody String addRepresentante(@RequestParam(name="id", required=false) String id, @RequestParam String rutModel, @RequestParam String nombreModel,
				@RequestParam String empresaModel, @RequestParam String apellido_paternoModel, @RequestParam String apellido_maternoModel, 
				@RequestParam String direccionModel, @RequestParam String emailModel, @RequestParam String fonoModel, @RequestParam String regionesModel) {
			LOG.info("Call: "+"addRepresentante()");
			
			String respuesta = "";
			
			RepresentanteModel representanteModel = null;
			
			if(!id.equalsIgnoreCase("0")) {
				int idRepresentante = Integer.parseInt(id);
				representanteModel = representanteConverter.entidad2Model(representanteService.findByidRepresentante(idRepresentante));
			}else {
				representanteModel = new RepresentanteModel();
			}
			
			representanteModel.setRutModel(rutModel);
			representanteModel.setApellido_paternoModel(apellido_paternoModel);
			representanteModel.setNombreModel(nombreModel);
			representanteModel.setApellido_maternoModel(apellido_maternoModel);
			representanteModel.setDireccionModel(direccionModel);
			representanteModel.setEmailModel(emailModel);
			representanteModel.setTelefonoModel(fonoModel);
			
			RegionesModel region = regionesService.findByidRegionModel(Integer.parseInt(regionesModel));
			representanteModel.setRegionesModel(region);
			
			EmpresaModel em = empresaService.findEmpresaByid(Integer.parseInt(empresaModel));
			
			if(em.getRepresentanteModel().getRutModel() != null && !em.getRepresentanteModel().getRutModel().equals("")) {		
				if(rutModel != "" && nombreModel != "" && apellido_paternoModel != "" && apellido_maternoModel != "" && direccionModel != "" &&
						emailModel != "" && fonoModel != "" && regionesModel != "" && empresaModel != "") {
					if(null != representanteService.addRepresentante(representanteModel)){
	//					model.addAttribute("result", 1);
						respuesta = "1";
					}
					else {
						respuesta = "3";
					}
				} else {
//					model.addAttribute("result", 0);
					respuesta = "2";
				}
				return respuesta;
			}else {
				
				respuesta = "2";
				
				return respuesta;
			}
			
			
		}
		
		@GetMapping("/addrepresentanteform")
		private ModelAndView redirectToAddRepresentanteForm(@RequestParam(name="rut", required=false) String rut,
				Model model) {
			
			ModelAndView mav = new ModelAndView(ViewConstant.REPRESENTANTE_FORM);
			RepresentanteModel representanteModel = new RepresentanteModel();
			if(!rut.equals("agregarrepresentante")) {
				representanteModel = representanteService.findRepresentanteByRutModel(rut);
			}
			mav.addObject("representantes", representanteService.listAllRepresentante());
			mav.addObject("empresas", empresaService.listAllEmpresa());
			mav.addObject("regiones", regionesService.listAllRegiones());
			mav.addObject(representanteModel);
			return mav;		
		}
		
		@GetMapping("/cancel")
		public ModelAndView cancel() {
			LOG.info("Call: "+ "listAllRepresentantes() -- desde controller");
			ModelAndView mav = new ModelAndView(ViewConstant.REPRESENTANTE_VIEW);
			mav.addObject("representantes", representanteService.listAllRepresentante());
			return mav;
		}
		
		@GetMapping("/removerepresentante")
		public ModelAndView removeRepresentante(@RequestParam(name="rut", required=true)String rut, Model model) {
			representanteService.removeRepresentante(rut);
			return listAllRepresentantes();
		}
		
		@RequestMapping(value="del", method=RequestMethod.GET)
		public @ResponseBody String removeRepresentante(@RequestParam String rut) {
			LOG.info("Call: "+ "removeTestigo()" + "-- Param: "+ rut.toString());
				
				String message = "Representante con el rut: " +rut+" eliminado";
			
				RepresentanteModel representante = new RepresentanteModel();		
				representante.setRutModel(rut);
				representante = representanteService.findRepresentanteByRutModel(rut);
				representante.setEstadoModel(0);			
				representante = representanteService.addRepresentante(representante);
				
			return message;
		}
		
		@RequestMapping(value="act", method=RequestMethod.GET)
		public @ResponseBody String updateTestigo(@RequestParam String rut) {
			LOG.info("Call: "+ "removeTestigo()" + "-- Param: "+ rut.toString());
				
			String message = "Representante con el rut: " +rut+" activar";
			
			RepresentanteModel representante = new RepresentanteModel();		
			representante.setRutModel(rut);
			representante = representanteService.findRepresentanteByRutModel(rut);
			representante.setEstadoModel(1);			
			representante = representanteService.addRepresentante(representante);
				
			return message;
		}
}
