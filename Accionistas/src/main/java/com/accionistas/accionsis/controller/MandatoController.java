package com.accionistas.accionsis.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.model.MandatoModel;
import com.accionistas.accionsis.service.MandatoService;

@Controller
@RequestMapping("mandatarios")
public class MandatoController {
	
	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(MandatoController.class);
		
		/** The apoderado service. */
		@Autowired
		@Qualifier("mandatarioServiceImp")
		private MandatoService mandatoService;
		
		/**
		 * Lista todos los mandatarios.
		 *
		 * @return the model and view
		 */
		@GetMapping("/listmandatarios")
		public ModelAndView listAllMandatos() {
			LOG.info("Call: "+ "listAllMandatarios() -- desde controller");
			ModelAndView mav = new ModelAndView(ViewConstant.MANDATARIO_VIEW);
			mav.addObject("mandatarios", mandatoService.listAllMandato());
			return mav;
		}	
		
		@PostMapping("/addmandatario")
		public String addMandato(@ModelAttribute(name="mandatarioModel") MandatoModel mandatoModel, 
				Model model) {
			LOG.info("Call: "+"addMandatario()"+"--Param: "+ mandatoModel.toString());
			if(null != mandatoService.addMandato(mandatoModel)){
				model.addAttribute("result", 1);
			} else {
				model.addAttribute("result", 0);
			}
			return "redirect:/mandatarios/listmandatarios";
		}
		
		@GetMapping("/addmandatarioform")
		private String redirectToAddMandatoForm(@RequestParam(name="id", required=false) int id, 
				Model model) {
			MandatoModel mandatoModel = new MandatoModel();
			if(id != 0) {
				mandatoModel = mandatoService.findMandatoByIdModel(id);	
			}
			
			model.addAttribute("mandatarioModel", mandatoModel);
			return ViewConstant.MANDATARIO_FORM;
		}
		
		@GetMapping("/cancel")
		public ModelAndView cancel() {
			LOG.info("Call: "+ "listAllMandatarios() -- desde controller");
			ModelAndView mav = new ModelAndView(ViewConstant.MANDATARIO_VIEW);
			mav.addObject("mandatarios", mandatoService.listAllMandato());
			return mav;
		}
		
		@GetMapping("/removemandatario")
		public ModelAndView removeMandato(@RequestParam(name="id", required=true)int id, Model model) {
			LOG.info("Call: "+ "removeMandato() -- desde controller");
			mandatoService.removeMandato(id);
			ModelAndView mav = new ModelAndView(ViewConstant.MANDATARIOS_VIEW);			
			return mav;
		}

}
