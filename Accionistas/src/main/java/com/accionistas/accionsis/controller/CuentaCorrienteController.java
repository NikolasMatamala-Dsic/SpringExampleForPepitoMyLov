package com.accionistas.accionsis.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.accionistas.accionsis.service.CuentaCorrienteService;

public class CuentaCorrienteController {
	
	private static final Log LOG = LogFactory.getLog(CuentaCorrienteController.class);

	@Autowired
	@Qualifier("cuentaCorrienteServiceImp")
	public CuentaCorrienteService cuentaCorrienteService;
}
