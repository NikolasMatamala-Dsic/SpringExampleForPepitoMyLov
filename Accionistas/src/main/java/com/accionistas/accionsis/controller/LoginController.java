package com.accionistas.accionsis.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.service.EmpresaService;

/**
 * Login LoginController.
 */
@Controller
public class LoginController {
	
	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(LoginController.class);
	
	/** The empresa service. */
	@Autowired
	@Qualifier("empresaServiceImp")
	private EmpresaService empresaService;
	
	
	/**
	 * muestra formulario login
	 *
	 * @param model the model
	 * @param error the error
	 * @param logout the logout
	 * @return the string
	 */
	@GetMapping("/login")
	public String showLoginForm(Model model,
			@RequestParam(name="error", required=false) String error,
			@RequestParam(name="logout", required=false) String logout){
		LOG.info("METHOD: showLoginForm() -- PARAMS: error=" + error +", logout= "+ logout);
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);
		
		LOG.info("Returning to login view");
		return (ViewConstant.LOGIN_FORM);
	}
	
	/**
	 * Login check.
	 *
	 * @return the string
	 */
	@GetMapping({"/loginsuccess" , "/"})
	public String loginCheck() {
		LOG.info("METHOD: loginCheck()");
		LOG.info("Returning to principal view");
		return "redirect:/empresas/listempresa";
	}
}
