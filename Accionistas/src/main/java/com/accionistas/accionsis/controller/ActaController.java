package com.accionistas.accionsis.controller;


import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.component.ActaConverter;
import com.accionistas.accionsis.component.AsistenciaJuntaAccionistaConverter;
import com.accionistas.accionsis.component.EmpresaConverter;
import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.entity.Acta;
import com.accionistas.accionsis.entity.JuntaAccionista;
import com.accionistas.accionsis.model.ActaModel;
import com.accionistas.accionsis.model.AsistenciaJuntaAccionistaModel;
import com.accionistas.accionsis.model.EmpresaModel;
import com.accionistas.accionsis.model.JuntaAccionistaModel;
import com.accionistas.accionsis.service.AccionistaService;
import com.accionistas.accionsis.service.ActaService;
import com.accionistas.accionsis.service.AsistenciaJuntaAccionistaService;
import com.accionistas.accionsis.service.EmpresaService;
import com.accionistas.accionsis.service.JuntaAccionistaService;
import com.accionistas.accionsis.service.StorageService;
import com.accionistas.accionsis.service.TipoActaService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.types.Constant;

import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;


@Controller
@RequestMapping("/acta")
public class ActaController {
	
	private static final Log LOG = LogFactory.getLog(ActaController.class);
	List<String> files = new ArrayList<String>();
	
	@Autowired
	@Qualifier("juntaAccionistaServiceImp")
	private JuntaAccionistaService juntaAccionistaService;
	
	@Autowired
	@Qualifier("actaConverter")
	private ActaConverter actaConverter;
	
	@Autowired
	@Qualifier("actaServiceImp")
	private ActaService actaService;
	
	@Autowired
	@Qualifier("accionistaServiceImp")
	private AccionistaService accionistaService;
	
	@Autowired
	@Qualifier("AsistenciaJuntaAccionistaServiceImp")
	private AsistenciaJuntaAccionistaService asistenciaJuntaAccionistaService;
	
	@Autowired
	@Qualifier("AsistenciaJuntaAccionistaConverter")
	private AsistenciaJuntaAccionistaConverter asistenciaJuntaAccionistaConverter;
	
	@Autowired
	@Qualifier("empresaServiceImp")
	private EmpresaService empresaService;
	
	@Autowired
	@Qualifier("tipoActaServiceImp")
	private TipoActaService tipoActaService;
	
	@Autowired
	@Qualifier("empresaConverter")
	private EmpresaConverter empresaConverter;
	
	@Autowired
	StorageService storageService;
	
	@GetMapping("/listAllActaForm")
	public ModelAndView listAllActaForm() {
		LOG.info("Call: "+ "listAllApoderados() -- desde controller");
		init_fileList();
		ModelAndView mav = new ModelAndView(ViewConstant.ACTA_FORM);
		List<Acta> actas = actaService.listAllActa();		
		List<String> list = new ArrayList<String>();							
		files.stream().map(fileName -> MvcUriComponentsBuilder
				.fromMethodName(ActaController.class, "getFile", fileName).build().toString())
		.forEach(fileName->{
			list.add(fileName);			
		});	
		
		mav.addObject("actas", actas);
		return mav;
	}
 
	@GetMapping("/listAllActaAdmin")
	public ModelAndView listAllActaAdmin() {
		LOG.info("Call: "+ "listAllApoderados() -- desde controller");
		init_fileList();
		ModelAndView mav = new ModelAndView(ViewConstant.ACTA_LIST);
		List<Acta> actas = actaService.listAllActa();		
		List<String> list = new ArrayList<String>();							
		files.stream().map(fileName -> MvcUriComponentsBuilder
				.fromMethodName(ActaController.class, "getFile", fileName).build().toString())
		.forEach(fileName->{
			list.add(fileName);			
		});	
		
		mav.addObject("actas", actas);
		return mav;
	}
	
	@GetMapping("/verActa")
	public ModelAndView verActa(@RequestParam(name="id", required=false) int id,
			Model model) {
		LOG.info("verActa: " + " desde controller verActa()" + " -- Param: " + id);
		ModelAndView mav = new ModelAndView(ViewConstant.ACTA_VIEW);
		ActaModel actaModel = new ActaModel();
		
		Acta acta = new Acta();
		acta.setIdActa(id);
		
		actaModel.setIdActa(id);
		if(id != 0) {
			acta = actaService.findActaByidActaModel(acta.getIdActa());
			mav.addObject("juntaAccionista", juntaAccionistaService.listAllJuntasAccionistas());
			mav.addObject("tiposActa", tipoActaService.listAllTipoActa());
			mav.addObject("actaModel", acta);
		}else {
			mav.addObject("juntaAccionista", juntaAccionistaService.listAllJuntasAccionistas());
			mav.addObject("tiposActa", tipoActaService.listAllTipoActa());
			mav.addObject("actaModel", new ActaModel());
		}
		
		return mav;
	}
	
	@RequestMapping(value="/addActa", method=RequestMethod.POST)
	public String guardarActa(@RequestParam("file") MultipartFile file, Model model, @RequestParam("idJunta") int idJunta, ActaModel actaModel) {
		
		LOG.info("Call: "+"addActa()");
		
		
		if(actaModel.getDetalleActa().trim().equalsIgnoreCase("")){
			model.addAttribute("error","Debes completar el campo detalle");
			return ViewConstant.ACTA_VIEW;
		}
		
		JuntaAccionista junta = juntaAccionistaService.findJuntaAccionistaById(idJunta);
		actaModel.setJuntaAccionista(junta);
		
		actaModel.setFechaApertura(junta.getFecha_realizacion());
		actaModel.setFechaTermino(junta.getFecha_realizacion());
		
		EmpresaModel em = empresaConverter.entidad2Model(empresaService.findEmpresaActiva());
		actaModel.setEmpresaAccionesA(Integer.toString(em.getCantidadAccionesSerieAModel()));
		actaModel.setEmpresaAccionesB(Integer.toString(em.getCantidadAccionesSerieBModel()));
		
		List<AsistenciaJuntaAccionistaModel> asistenciaJunta = asistenciaJuntaAccionistaService.listAllAsistenciaJuntaAccionista();
		int contadorA = 0;
		int contadorB = 0;
		for(int i = 0; i < asistenciaJunta.size(); i++) {		
			if(asistenciaJunta.get(i).getEstadoAsistenciaJuntaModel() == 1) {
				contadorA += asistenciaJunta.get(i).getAccionistaModel().getCantidad_acciones_serie_aModel();		
				contadorB +=  asistenciaJunta.get(i).getAccionistaModel().getCantidad_acciones_serie_bModel();
			}
		}
		actaModel.setAccionistasAccionesA(Integer.toString(contadorA));
		actaModel.setAccionistasAccionesB(Integer.toString(contadorB));
		
		
		String ruta = handleFileUploadWeb(file, model);
		getListFiles(model);
		actaModel.setUrl(ruta);
		
		actaModel.setDocumento(file.getOriginalFilename());
		

		actaService.addActa(actaModel);
		return "redirect:/acta/listAllActaAdmin";
	}
	
	@RequestMapping(value="/getActaValues", method=RequestMethod.GET)
	public @ResponseBody String generarActa(@RequestParam("idJunta") int idJunta) {
		
		String respuesta = "";
		
		//Busca id Junta Accionista
		JuntaAccionistaModel junta = new JuntaAccionistaModel();
		junta.setIdJuntaAccionistaModel(idJunta);
		junta = juntaAccionistaService.findJuntaAccionistaByIdModel(junta.getIdJuntaAccionistaModel());
		
		//Busca fechas de Junta Accionista
		String fechai = junta.getFecha_realizacionModel() + "";
		String fechat = junta.getFecha_realizacionModel() + "";
		
		//Catidad de acciones de Empresa Activa
		EmpresaModel em = new EmpresaModel();
		em = empresaConverter.entidad2Model(empresaService.findEmpresaActiva());
		int empresaSerieA = em.getCantidadAccionesSerieAModel();
		int empresaSerieB = em.getCantidadAccionesSerieBModel();
		
		//Recorre accionistas para traer total de acciones
		List<AsistenciaJuntaAccionistaModel> asistencia = asistenciaJuntaAccionistaService.listAllAsistenciaJuntaAccionista();
	
		int contadorA = 0;
		int contadorB = 0;
		for(int i = 0; i < asistencia.size(); i++) {		
			if(asistencia.get(i).getEstadoAsistenciaJuntaModel() == 1) {
				contadorA += asistencia.get(i).getAccionistaModel().getCantidad_acciones_serie_aModel();		
				contadorB +=  asistencia.get(i).getAccionistaModel().getCantidad_acciones_serie_bModel();
			}
		}
				
				
		//Se crea lista de objetos en JSON
		List<String> listJSON = new ArrayList<>();
		listJSON.add(contadorA+ "");
		listJSON.add(contadorB+ "");
		listJSON.add(fechai);
		listJSON.add(fechat);
		listJSON.add(empresaSerieA+"");
		listJSON.add(empresaSerieB+"");
		
		
		ObjectMapper objetMapperJSON = new ObjectMapper();
		try {
			respuesta = objetMapperJSON.writeValueAsString(listJSON);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(respuesta);
		return respuesta;
	}
 
	@PostMapping("/")
	public String handleFileUploadWeb(@RequestParam("file") MultipartFile file, Model model) {
		Path FileRute = null;
		String ruta = "";
		try {
			FileRute = storageService.store(file);		
		
			FileRute = FileRute.toAbsolutePath();
			ruta = FileRute.toString();
//			model.addAttribute("message", "You successfully uploaded " + file.getOriginalFilename() + "!");
			files.add(file.getOriginalFilename());
		} catch (Exception e) {
			model.addAttribute("message", "Fallo a Cargar " + file.getOriginalFilename() + "!");
		}
		return ruta;
	}
 
	@GetMapping("/gellallfiles")
	public String getListFiles(Model model) {
		init_fileList();
		
		model.addAttribute("files",
				files.stream()
						.map(fileName -> MvcUriComponentsBuilder
								.fromMethodName(ActaController.class, "getFile", fileName).build().toString())
						.collect(Collectors.toList()));
		model.addAttribute("totalFiles", "TotalFiles: " + files.size());
		return "addActa";
	}
 
	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> getFile(@PathVariable String filename) {
		Resource file = storageService.loadFile(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}
	
	private void init_fileList() {
		
		Path ruta = storageService.getRootLocation();
		String rutaAbsoluta = ruta.toAbsolutePath().toString();
		File[] files = new File(rutaAbsoluta).listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null. 
		this.files.clear();
		for (File file : files) {
		    if (file.isFile()) {
		        this.files.add(file.getName());
		    }
		}
	}
	
	

}
