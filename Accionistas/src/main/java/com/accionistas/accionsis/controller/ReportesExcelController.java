package com.accionistas.accionsis.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.entity.CuentaCorriente;
import com.accionistas.accionsis.entity.Dividendo;
import com.accionistas.accionsis.entity.Empresa;
import com.accionistas.accionsis.model.TraspasoaccionesModel;
import com.accionistas.accionsis.service.AccionistaService;
import com.accionistas.accionsis.service.CuentaCorrienteService;
import com.accionistas.accionsis.service.DividendoService;
import com.accionistas.accionsis.service.EmpresaService;
import com.accionistas.accionsis.service.TraspasoaccionesService;
import com.accionistas.accionsis.utility.RutaReportes;



@Controller
@RequestMapping("reporteExcel")
public class ReportesExcelController {
	
	@Autowired
	@Qualifier("accionistaServiceImp")
	private AccionistaService accionistaService;
	
	@Autowired
	@Qualifier("traspasoaccionesServiceImp")
	private TraspasoaccionesService traspasoaccionesService;
	
	@Autowired
	@Qualifier("cuentaCorrienteServiceImp")
	private CuentaCorrienteService cuentaCorrienteService;
	
	@Autowired
	@Qualifier("empresaServiceImp")
	private EmpresaService empresaService;
	
	@Autowired
	@Qualifier("dividendoServiceImp")
	private DividendoService dividendoService;
	
	@Autowired
	@Qualifier("cuentaCorrienteServiceImp")
	private CuentaCorrienteService corrienteService;
	
	@RequestMapping(value = "/reporteExcelTraspaso", method = RequestMethod.GET)
	public @ResponseBody String reporteListaTraspaso() {	
		return reporteEXCELTraspaso();
	}
	
	@RequestMapping(value = "/reporteExcelAccionista", method = RequestMethod.GET)
	public @ResponseBody String reporteListaAccionistas(@RequestParam(name="filtroExcel") String filtroExcel) {	
		String respuesta = "";
		switch(filtroExcel) {
			case "TodosAccionista":
				respuesta = reporteEXCELAccionista();
				break;
			case "TodosAccionistaActivos":
				respuesta = reporteEXCELAccionistasActivos();
				break;
			case "10AccionistasTipoA":
				respuesta = reporteEXCELAccionistasMayoritariosTipoA();
				break;
			case "10AccionistasTipoB":
				respuesta = reporteEXCELAccionistasMayoritariosTipoB();
				break;
			default:
				respuesta = "2";
				break;
		}
		
		return respuesta;
	}
	
	@RequestMapping(value = "/reporteExcelDividendo", method = RequestMethod.GET)
	public @ResponseBody String reporteListaDividendo() {	
		return reporteEXCELDividendo();
	}
	
	public String reporteEXCELTraspaso() {
   		String respuesta = "";
		
		List<TraspasoaccionesModel> listTraspaso = traspasoaccionesService.listAllTraspasosacciones();
		Empresa em = empresaService.findEmpresaActiva();
		
		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaEXCEL);
		ruta += "\\TraspasoAcciones\\Reporte_TraspasoAcciones_" + FechaActual + "_" + HoraActual + ".xls";
		
		File carpeta = new File(ruta);
		carpeta.getParentFile().mkdirs();
		HSSFRow fila;
		HSSFCell celda;
		String dato = "";
		
		try {
			
			FileOutputStream archivo = new FileOutputStream(ruta);
			HSSFWorkbook workbook = new HSSFWorkbook();

			HSSFSheet hojaTraspasos = workbook.createSheet("Traspasos de Acciones");
			hojaTraspasos.setColumnWidth(0, 3000);
			hojaTraspasos.setColumnWidth(1, 3000);
			hojaTraspasos.setColumnWidth(2, 5000);
			hojaTraspasos.setColumnWidth(3, 8000);
			hojaTraspasos.setColumnWidth(4, 5000);
			hojaTraspasos.setColumnWidth(5, 8000);
			hojaTraspasos.setColumnWidth(6, 4000);
			hojaTraspasos.setColumnWidth(7, 4000);
			hojaTraspasos.setColumnWidth(8, 8000);
			hojaTraspasos.setColumnWidth(9, 10000);
			hojaTraspasos.setColumnWidth(10, 4000);
			hojaTraspasos.setColumnWidth(11, 4000);
			hojaTraspasos.setColumnWidth(12, 4000);
			hojaTraspasos.setColumnWidth(13, 4000);
			hojaTraspasos.setColumnWidth(14, 4000);
			
			
			HSSFRow encabezado = hojaTraspasos.createRow(0);
			HSSFCellStyle hssfCellStyleCabecera = workbook.createCellStyle();
			HSSFCellStyle hssfCellStyleAlignRight = workbook.createCellStyle();
			HSSFCellStyle hssfCellStyleAlignCenter = workbook.createCellStyle();
			
			hssfCellStyleAlignCenter.setAlignment(CellStyle.ALIGN_CENTER);
			
			hssfCellStyleAlignRight.setAlignment(CellStyle.ALIGN_RIGHT);
			
			HSSFCellStyle estiloNombre = workbook.createCellStyle();
			estiloNombre.setWrapText(true);
			
			hssfCellStyleCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			hssfCellStyleCabecera.setFillForegroundColor(HSSFColor.BLACK.index);
			hssfCellStyleCabecera.setAlignment(CellStyle.ALIGN_CENTER);
			hssfCellStyleCabecera.setVerticalAlignment(CellStyle.ALIGN_CENTER);
			hssfCellStyleCabecera.setWrapText(true);
			
			
			
			HSSFFont hssfFont = workbook.createFont();
			hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			hssfFont.setColor(HSSFColor.WHITE.index);

			hssfCellStyleCabecera.setFont(hssfFont);

			HSSFCell celda1 = encabezado.createCell(0);
			HSSFCell celda2 = encabezado.createCell(1);
			HSSFCell celda3 = encabezado.createCell(2);
			HSSFCell celda4 = encabezado.createCell(3);
			HSSFCell celda5 = encabezado.createCell(4);
			HSSFCell celda6 = encabezado.createCell(5);
			HSSFCell celda7 = encabezado.createCell(6);
			HSSFCell celda8 = encabezado.createCell(7);
			HSSFCell celda9 = encabezado.createCell(8);
			HSSFCell celda10 = encabezado.createCell(9);
			HSSFCell celda11 = encabezado.createCell(10);
			HSSFCell celda12 = encabezado.createCell(11);
			HSSFCell celda13 = encabezado.createCell(12);
			HSSFCell celda14 = encabezado.createCell(13);
			HSSFCell celda15 = encabezado.createCell(14);

			celda1.setCellValue("#");
			celda1.setCellStyle(hssfCellStyleCabecera);
			celda2.setCellValue("ID Traspaso");
			celda2.setCellStyle(hssfCellStyleCabecera);
			celda3.setCellValue("Rut Accionista Comprador");
			celda3.setCellStyle(hssfCellStyleCabecera);
			celda4.setCellValue("Accionista Compador");
			celda4.setCellStyle(hssfCellStyleCabecera);
			celda5.setCellValue("Rut Accionista Vendedor");
			celda5.setCellStyle(hssfCellStyleCabecera);
			celda6.setCellValue("Accionista Vendedor");
			celda6.setCellStyle(hssfCellStyleCabecera);
			celda7.setCellValue("Fecha Venta");
			celda7.setCellStyle(hssfCellStyleCabecera);
			celda8.setCellValue("Rut Testigo");
			celda8.setCellStyle(hssfCellStyleCabecera);
			celda9.setCellValue("Testigo");
			celda9.setCellStyle(hssfCellStyleCabecera);
			celda10.setCellValue("Observacion");
			celda10.setCellStyle(hssfCellStyleCabecera);
			celda11.setCellValue("Cantidad Acciones Tipo A");
			celda11.setCellStyle(hssfCellStyleCabecera);
			celda12.setCellValue("Valor Acciones A");
			celda12.setCellStyle(hssfCellStyleCabecera);
			celda13.setCellValue("Cantidad Acciones Tipo B");
			celda13.setCellStyle(hssfCellStyleCabecera);
			celda14.setCellValue("Valor Acciones B");
			celda14.setCellStyle(hssfCellStyleCabecera);
			celda15.setCellValue("Monto Total Venta");
			celda15.setCellStyle(hssfCellStyleCabecera);

			int contador = 0;
			for (int filas = 1; filas < listTraspaso.size(); filas++) {

				fila = hojaTraspasos.createRow(filas);

				for (int columnas = 0; columnas < 15; columnas++) {

					switch (columnas) {

					case 0:						
						celda = fila.createCell(columnas);
						dato = (filas ) + "";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 1:						
						celda = fila.createCell(columnas);
						celda.setCellStyle(hssfCellStyleAlignRight);
						dato = listTraspaso.get(contador).getIdTraspasoAccionesModel() + "";
						celda.setCellValue(dato);
						break;
					case 2:						
						celda = fila.createCell(columnas);
						celda.setCellStyle(hssfCellStyleAlignCenter);
						dato = listTraspaso.get(contador).getRut_accionista_compradorModel().getRutModel();
						celda.setCellValue(dato);
						break;
					case 3:						
						celda = fila.createCell(columnas);
						dato = listTraspaso.get(contador).getRut_accionista_compradorModel().getNombreModel() + " " +
								listTraspaso.get(contador).getRut_accionista_compradorModel().getApellido_paternoModel() + " " +
								listTraspaso.get(contador).getRut_accionista_compradorModel().getApellido_maternoModel();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 4:						
						celda = fila.createCell(columnas);
						dato = listTraspaso.get(contador).getRut_accionista_vendedorModel().getRutModel();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 5:						
						celda = fila.createCell(columnas);
						dato = listTraspaso.get(contador).getRut_accionista_vendedorModel().getNombreModel()+ " " +
								listTraspaso.get(contador).getRut_accionista_vendedorModel().getApellido_paternoModel() + " " +
								listTraspaso.get(contador).getRut_accionista_vendedorModel().getApellido_maternoModel();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 6:						
						celda = fila.createCell(columnas);
						dato = listTraspaso.get(contador).getFecha_ventaModel() + "";
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 7:						
						celda = fila.createCell(columnas);
						dato = listTraspaso.get(contador).getTestigoModel().getRutModel();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 8:						
						celda = fila.createCell(columnas);
						dato = listTraspaso.get(contador).getTestigoModel().getNombreModel() + " " +
								listTraspaso.get(contador).getTestigoModel().getApellido_paternoModel() + " " +
								listTraspaso.get(contador).getTestigoModel().getApellido_maternoModel();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 9:						
						celda = fila.createCell(columnas);
						dato = listTraspaso.get(contador).getObservacionModel();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 10:						
						celda = fila.createCell(columnas);
						dato = listTraspaso.get(contador).getCantidad_acciones_vendidas_aModel() + "";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 11:						
						celda = fila.createCell(columnas);
						dato = em.getCantidadAccionesSerieA() + "";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 12:						
						celda = fila.createCell(columnas);
						dato = listTraspaso.get(contador).getCantidad_acciones_vendidas_bModel() +"";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 13:						
						celda = fila.createCell(columnas);
						dato = em.getCantidadAccionesSerieB() + "";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 14:						
						celda = fila.createCell(columnas);
						dato = listTraspaso.get(contador).getMontoModel() +"";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
						
					}
				}
				contador++;
			}
			workbook.write(archivo);
			archivo.close();
			respuesta = "1";
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return respuesta;
	}

	public String reporteEXCELAccionista() {
		
		String respuesta = "";
		
		List<Accionista> listAccionista = accionistaService.listAllAccionistaEntity();
		
		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaEXCEL);
		ruta += "\\Acionistas\\Accionistas_Todos\\Reporte_Accionistas_" + FechaActual + "_" + HoraActual + ".xls";
		
		File carpeta = new File(ruta);
		carpeta.getParentFile().mkdirs();
		HSSFRow fila;
		HSSFCell celda;
		String dato = "";
		
		try {
			
			FileOutputStream archivo = new FileOutputStream(ruta);
			HSSFWorkbook workbook = new HSSFWorkbook();

			HSSFSheet hojaTraspasos = workbook.createSheet("Todos Los Accionistas");
			hojaTraspasos.setColumnWidth(0, 3000);
			hojaTraspasos.setColumnWidth(1, 5000);
			hojaTraspasos.setColumnWidth(2, 8000);
			hojaTraspasos.setColumnWidth(3, 14000);
			hojaTraspasos.setColumnWidth(4, 8000);
			hojaTraspasos.setColumnWidth(5, 5000);
			hojaTraspasos.setColumnWidth(6, 10000);
			hojaTraspasos.setColumnWidth(7, 10000);
			hojaTraspasos.setColumnWidth(8, 5000);
			hojaTraspasos.setColumnWidth(9, 4000);
			hojaTraspasos.setColumnWidth(10, 4000);
			hojaTraspasos.setColumnWidth(11, 4000);
			hojaTraspasos.setColumnWidth(12, 4000);
			hojaTraspasos.setColumnWidth(13, 4000);
			hojaTraspasos.setColumnWidth(14, 4000);
			
			
			HSSFRow encabezado = hojaTraspasos.createRow(0);
			
			HSSFCellStyle hssfCellStyleCabecera = workbook.createCellStyle();
			HSSFCellStyle hssfCellStyleAlignCenter = workbook.createCellStyle();
			HSSFCellStyle hssfCellStyleAlignRight = workbook.createCellStyle();
			
			hssfCellStyleAlignCenter.setAlignment(CellStyle.ALIGN_CENTER);
			
			hssfCellStyleAlignRight.setAlignment(CellStyle.ALIGN_RIGHT);
			
			HSSFCellStyle estiloNombre = workbook.createCellStyle();
			estiloNombre.setWrapText(true);
			
			hssfCellStyleCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			hssfCellStyleCabecera.setFillForegroundColor(HSSFColor.BLACK.index);
			hssfCellStyleCabecera.setAlignment(CellStyle.ALIGN_CENTER);
			hssfCellStyleCabecera.setVerticalAlignment(CellStyle.ALIGN_CENTER);
			hssfCellStyleCabecera.setWrapText(true);

			HSSFFont hssfFont = workbook.createFont();
			hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			hssfFont.setColor(HSSFColor.WHITE.index);

			hssfCellStyleCabecera.setFont(hssfFont);

			HSSFCell celda1 = encabezado.createCell(0);
			HSSFCell celda2 = encabezado.createCell(1);
			HSSFCell celda3 = encabezado.createCell(2);
			HSSFCell celda4 = encabezado.createCell(3);
			HSSFCell celda5 = encabezado.createCell(4);
			HSSFCell celda6 = encabezado.createCell(5);
			HSSFCell celda7 = encabezado.createCell(6);
			HSSFCell celda8 = encabezado.createCell(7);
			HSSFCell celda9 = encabezado.createCell(8);
			HSSFCell celda10 = encabezado.createCell(9);
			HSSFCell celda11 = encabezado.createCell(10);
			HSSFCell celda12 = encabezado.createCell(11);
			HSSFCell celda13 = encabezado.createCell(12);
			HSSFCell celda14 = encabezado.createCell(13);
			HSSFCell celda15 = encabezado.createCell(14);

			celda1.setCellValue("#");
			celda1.setCellStyle(hssfCellStyleCabecera);
			celda2.setCellValue("Rut Accionista");
			celda2.setCellStyle(hssfCellStyleCabecera);
			celda3.setCellValue("Accionista");
			celda3.setCellStyle(hssfCellStyleCabecera);
			celda4.setCellValue("Direccion");
			celda4.setCellStyle(hssfCellStyleCabecera);
			celda5.setCellValue("Nacionalidad");
			celda5.setCellStyle(hssfCellStyleCabecera);
			celda6.setCellValue("Apoderado");
			celda6.setCellStyle(hssfCellStyleCabecera);
			celda7.setCellValue("Empresa");
			celda7.setCellStyle(hssfCellStyleCabecera);
			celda8.setCellValue("Tipo Accionista");
			celda8.setCellStyle(hssfCellStyleCabecera);
			celda9.setCellValue("% de Participacion");
			celda9.setCellStyle(hssfCellStyleCabecera);
			celda10.setCellValue("Cta. Corriente");
			celda10.setCellStyle(hssfCellStyleCabecera);
			celda11.setCellValue("Banco");
			celda11.setCellStyle(hssfCellStyleCabecera);
			celda12.setCellValue("Estado");
			celda12.setCellStyle(hssfCellStyleCabecera);
			celda13.setCellValue("Cantidad Acciones Tipo A");
			celda13.setCellStyle(hssfCellStyleCabecera);
			celda14.setCellValue("Cantidad Acciones Tipo B");
			celda14.setCellStyle(hssfCellStyleCabecera);
			celda15.setCellValue("Total Acciones");
			celda15.setCellStyle(hssfCellStyleCabecera);

			int contador = 0;
			for (int filas = 1; filas < listAccionista.size(); filas++) {

				fila = hojaTraspasos.createRow(filas);

				for (int columnas = 0; columnas < 16; columnas++) {

					switch (columnas) {

					case 0:						
						celda = fila.createCell(columnas);
						celda.setCellStyle(hssfCellStyleAlignRight);
						dato = (filas) + "";
						celda.setCellValue(dato);
						break;
					case 1:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getRut();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 2:						
						celda = fila.createCell(columnas);
						celda.setCellStyle(hssfCellStyleAlignCenter);
						dato = listAccionista.get(contador).getNombre() + " " + listAccionista.get(contador).getApellido_paterno() + " " + listAccionista.get(contador).getApellido_materno();
						celda.setCellValue(dato);
						break;
					case 3:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getDireccion() + " " + listAccionista.get(contador).getComuna() + " " +
								listAccionista.get(contador).getCiudad();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 4:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getNacionalidad().getNacionalidad();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 5:						
						celda = fila.createCell(columnas);
						if(listAccionista.get(contador).getApoderado() == null) {
							dato = "/*Sin Apoderado*/";
						}else {
							dato = listAccionista.get(contador).getApoderado().getRut();
						}
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 6:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getEmpresa().getRazonSocial();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 7:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getTipoAccionista().getDescripcion_tipo_accionista();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 8:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getPorcentaje_participacion() + "";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 9:						
						celda = fila.createCell(columnas);
						String numeroC = "/*Sin Cta. Coriente*/";
						for(CuentaCorriente c : listAccionista.get(contador).getCuentaCorriente()) {
							numeroC = c.getNumeroCuenta() + "";
							break;
						}
						dato = numeroC ;
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 10:						
						celda = fila.createCell(columnas);
						String nombreB = "/*Sin Banco*/";
						for(CuentaCorriente c : listAccionista.get(contador).getCuentaCorriente()) {
							nombreB = c.getNombreBanco();
							break;
						}
						dato = nombreB;
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 11:						
						celda = fila.createCell(columnas);
						if(listAccionista.get(contador).getEstado() == 0) {
							dato = "Eliminado";
						}else {
							dato = "Activo";
						}
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 12:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getCantidad_acciones_serie_a() +"";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 13:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getCantidad_acciones_serie_b() +"";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 14:						
						celda = fila.createCell(columnas);
						dato = (listAccionista.get(contador).getCantidad_acciones_serie_b() + listAccionista.get(contador).getCantidad_acciones_serie_a()) + "";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
						
					}
				}
				contador++;
			}
			workbook.write(archivo);
			archivo.close();
			respuesta = "1";
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return respuesta;
	}

	public String reporteEXCELAccionistasActivos() {
		String respuesta = "";
		
		List<Accionista> listAccionista = accionistaService.listAllAccionistaEntity_active();
		
		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaEXCEL);
		ruta += "\\Acionistas\\Accionistas_Activos\\Reporte_Accionistas_" + FechaActual + "_" + HoraActual + ".xls";
		
		File carpeta = new File(ruta);
		carpeta.getParentFile().mkdirs();
		HSSFRow fila;
		HSSFCell celda;
		String dato = "";
		
		try {
			
			FileOutputStream archivo = new FileOutputStream(ruta);
			HSSFWorkbook workbook = new HSSFWorkbook();

			HSSFSheet hojaTraspasos = workbook.createSheet("Todos Los Accionistas Activos");
			hojaTraspasos.setColumnWidth(0, 3000);
			hojaTraspasos.setColumnWidth(1, 5000);
			hojaTraspasos.setColumnWidth(2, 8000);
			hojaTraspasos.setColumnWidth(3, 14000);
			hojaTraspasos.setColumnWidth(4, 8000);
			hojaTraspasos.setColumnWidth(5, 5000);
			hojaTraspasos.setColumnWidth(6, 10000);
			hojaTraspasos.setColumnWidth(7, 10000);
			hojaTraspasos.setColumnWidth(8, 5000);
			hojaTraspasos.setColumnWidth(9, 4000);
			hojaTraspasos.setColumnWidth(10, 4000);
			hojaTraspasos.setColumnWidth(11, 4000);
			hojaTraspasos.setColumnWidth(12, 4000);
			hojaTraspasos.setColumnWidth(13, 4000);
			hojaTraspasos.setColumnWidth(14, 4000);
			
			
			HSSFRow encabezado = hojaTraspasos.createRow(0);
			
			HSSFCellStyle hssfCellStyleCabecera = workbook.createCellStyle();
			HSSFCellStyle hssfCellStyleAlignCenter = workbook.createCellStyle();
			HSSFCellStyle hssfCellStyleAlignRight = workbook.createCellStyle();
			
			hssfCellStyleAlignCenter.setAlignment(CellStyle.ALIGN_CENTER);
			
			hssfCellStyleAlignRight.setAlignment(CellStyle.ALIGN_RIGHT);
			
			HSSFCellStyle estiloNombre = workbook.createCellStyle();
			estiloNombre.setWrapText(true);
			
			hssfCellStyleCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			hssfCellStyleCabecera.setFillForegroundColor(HSSFColor.BLACK.index);
			hssfCellStyleCabecera.setAlignment(CellStyle.ALIGN_CENTER);
			hssfCellStyleCabecera.setVerticalAlignment(CellStyle.ALIGN_CENTER);
			hssfCellStyleCabecera.setWrapText(true);

			HSSFFont hssfFont = workbook.createFont();
			hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			hssfFont.setColor(HSSFColor.WHITE.index);

			hssfCellStyleCabecera.setFont(hssfFont);

			HSSFCell celda1 = encabezado.createCell(0);
			HSSFCell celda2 = encabezado.createCell(1);
			HSSFCell celda3 = encabezado.createCell(2);
			HSSFCell celda4 = encabezado.createCell(3);
			HSSFCell celda5 = encabezado.createCell(4);
			HSSFCell celda6 = encabezado.createCell(5);
			HSSFCell celda7 = encabezado.createCell(6);
			HSSFCell celda8 = encabezado.createCell(7);
			HSSFCell celda9 = encabezado.createCell(8);
			HSSFCell celda10 = encabezado.createCell(9);
			HSSFCell celda11 = encabezado.createCell(10);
			HSSFCell celda12 = encabezado.createCell(11);
			HSSFCell celda13 = encabezado.createCell(12);
			HSSFCell celda14 = encabezado.createCell(13);
			HSSFCell celda15 = encabezado.createCell(14);

			celda1.setCellValue("#");
			celda1.setCellStyle(hssfCellStyleCabecera);
			celda2.setCellValue("Rut Accionista");
			celda2.setCellStyle(hssfCellStyleCabecera);
			celda3.setCellValue("Accionista");
			celda3.setCellStyle(hssfCellStyleCabecera);
			celda4.setCellValue("Direccion");
			celda4.setCellStyle(hssfCellStyleCabecera);
			celda5.setCellValue("Nacionalidad");
			celda5.setCellStyle(hssfCellStyleCabecera);
			celda6.setCellValue("Apoderado");
			celda6.setCellStyle(hssfCellStyleCabecera);
			celda7.setCellValue("Empresa");
			celda7.setCellStyle(hssfCellStyleCabecera);
			celda8.setCellValue("Tipo Accionista");
			celda8.setCellStyle(hssfCellStyleCabecera);
			celda9.setCellValue("% de Participacion");
			celda9.setCellStyle(hssfCellStyleCabecera);
			celda10.setCellValue("Cta. Corriente");
			celda10.setCellStyle(hssfCellStyleCabecera);
			celda11.setCellValue("Banco");
			celda11.setCellStyle(hssfCellStyleCabecera);
			celda12.setCellValue("Estado");
			celda12.setCellStyle(hssfCellStyleCabecera);
			celda13.setCellValue("Cantidad Acciones Tipo A");
			celda13.setCellStyle(hssfCellStyleCabecera);
			celda14.setCellValue("Cantidad Acciones Tipo B");
			celda14.setCellStyle(hssfCellStyleCabecera);
			celda15.setCellValue("Total Acciones");
			celda15.setCellStyle(hssfCellStyleCabecera);

			int contador = 0;
			for (int filas = 1; filas < listAccionista.size(); filas++) {

				fila = hojaTraspasos.createRow(filas);

				for (int columnas = 0; columnas < 16; columnas++) {

					switch (columnas) {

					case 0:						
						celda = fila.createCell(columnas);
						celda.setCellStyle(hssfCellStyleAlignRight);
						dato = (filas) + "";
						celda.setCellValue(dato);
						break;
					case 1:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getRut();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 2:						
						celda = fila.createCell(columnas);
						celda.setCellStyle(hssfCellStyleAlignCenter);
						dato = listAccionista.get(contador).getNombre() + " " + listAccionista.get(contador).getApellido_paterno() + " " + listAccionista.get(contador).getApellido_materno();
						celda.setCellValue(dato);
						break;
					case 3:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getDireccion() + " " + listAccionista.get(contador).getComuna() + " " +
								listAccionista.get(contador).getCiudad();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 4:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getNacionalidad().getNacionalidad();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 5:						
						celda = fila.createCell(columnas);
						if(listAccionista.get(contador).getApoderado() == null) {
							dato = "/*Sin Apoderado*/";
						}else {
							dato = listAccionista.get(contador).getApoderado().getRut();
						}
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 6:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getEmpresa().getRazonSocial();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 7:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getTipoAccionista().getDescripcion_tipo_accionista();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 8:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getPorcentaje_participacion() + "";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 9:						
						celda = fila.createCell(columnas);
						String numeroC = "/*Sin Cta. Coriente*/";
						for(CuentaCorriente c : listAccionista.get(contador).getCuentaCorriente()) {
							numeroC = c.getNumeroCuenta() + "";
							break;
						}
						dato = numeroC ;
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 10:						
						celda = fila.createCell(columnas);
						String nombreB = "/*Sin Banco*/";
						for(CuentaCorriente c : listAccionista.get(contador).getCuentaCorriente()) {
							nombreB = c.getNombreBanco();
							break;
						}
						dato = nombreB;
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 11:						
						celda = fila.createCell(columnas);
						if(listAccionista.get(contador).getEstado() == 0) {
							dato = "Eliminado";
						}else {
							dato = "Activo";
						}
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 12:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getCantidad_acciones_serie_a() +"";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 13:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getCantidad_acciones_serie_b() +"";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 14:						
						celda = fila.createCell(columnas);
						dato = (listAccionista.get(contador).getCantidad_acciones_serie_b() + listAccionista.get(contador).getCantidad_acciones_serie_a()) + "";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
						
					}
				}
				contador++;
			}
			workbook.write(archivo);
			archivo.close();
			respuesta = "1";
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return respuesta;
	}
	
	public String reporteEXCELAccionistasMayoritariosTipoA() {
		String respuesta = "";
		
		List<Accionista> listAccionista = accionistaService.listAccionistasForTop10AccionTypeA();
		
		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaEXCEL);
		ruta += "\\Acionistas\\Accionistas_AccionesTipoA_TOP10\\Reporte_Accionistas_" + FechaActual + "_" + HoraActual + ".xls";
		
		File carpeta = new File(ruta);
		carpeta.getParentFile().mkdirs();
		HSSFRow fila;
		HSSFCell celda;
		String dato = "";
		
		try {
			
			FileOutputStream archivo = new FileOutputStream(ruta);
			HSSFWorkbook workbook = new HSSFWorkbook();

			HSSFSheet hojaTraspasos = workbook.createSheet("Todos Los Accionistas Mayoritarios Tipo A");
			hojaTraspasos.setColumnWidth(0, 3000);
			hojaTraspasos.setColumnWidth(1, 5000);
			hojaTraspasos.setColumnWidth(2, 8000);
			hojaTraspasos.setColumnWidth(3, 14000);
			hojaTraspasos.setColumnWidth(4, 8000);
			hojaTraspasos.setColumnWidth(5, 5000);
			hojaTraspasos.setColumnWidth(6, 10000);
			hojaTraspasos.setColumnWidth(7, 10000);
			hojaTraspasos.setColumnWidth(8, 5000);
			hojaTraspasos.setColumnWidth(9, 4000);
			hojaTraspasos.setColumnWidth(10, 4000);
			hojaTraspasos.setColumnWidth(11, 4000);
			hojaTraspasos.setColumnWidth(12, 4000);
			hojaTraspasos.setColumnWidth(13, 4000);
			
			
			HSSFRow encabezado = hojaTraspasos.createRow(0);
			
			HSSFCellStyle hssfCellStyleCabecera = workbook.createCellStyle();
			HSSFCellStyle hssfCellStyleAlignCenter = workbook.createCellStyle();
			HSSFCellStyle hssfCellStyleAlignRight = workbook.createCellStyle();
			
			hssfCellStyleAlignCenter.setAlignment(CellStyle.ALIGN_CENTER);
			
			hssfCellStyleAlignRight.setAlignment(CellStyle.ALIGN_RIGHT);
			
			HSSFCellStyle estiloNombre = workbook.createCellStyle();
			estiloNombre.setWrapText(true);
			
			hssfCellStyleCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			hssfCellStyleCabecera.setFillForegroundColor(HSSFColor.BLACK.index);
			hssfCellStyleCabecera.setAlignment(CellStyle.ALIGN_CENTER);
			hssfCellStyleCabecera.setVerticalAlignment(CellStyle.ALIGN_CENTER);
			hssfCellStyleCabecera.setWrapText(true);

			HSSFFont hssfFont = workbook.createFont();
			hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			hssfFont.setColor(HSSFColor.WHITE.index);

			hssfCellStyleCabecera.setFont(hssfFont);

			HSSFCell celda1 = encabezado.createCell(0);
			HSSFCell celda2 = encabezado.createCell(1);
			HSSFCell celda3 = encabezado.createCell(2);
			HSSFCell celda4 = encabezado.createCell(3);
			HSSFCell celda5 = encabezado.createCell(4);
			HSSFCell celda6 = encabezado.createCell(5);
			HSSFCell celda7 = encabezado.createCell(6);
			HSSFCell celda8 = encabezado.createCell(7);
			HSSFCell celda9 = encabezado.createCell(8);
			HSSFCell celda10 = encabezado.createCell(9);
			HSSFCell celda11 = encabezado.createCell(10);
			HSSFCell celda12 = encabezado.createCell(11);
			HSSFCell celda13 = encabezado.createCell(12);
			HSSFCell celda14 = encabezado.createCell(13);

			celda1.setCellValue("#");
			celda1.setCellStyle(hssfCellStyleCabecera);
			celda2.setCellValue("Rut Accionista");
			celda2.setCellStyle(hssfCellStyleCabecera);
			celda3.setCellValue("Accionista");
			celda3.setCellStyle(hssfCellStyleCabecera);
			celda4.setCellValue("Direccion");
			celda4.setCellStyle(hssfCellStyleCabecera);
			celda5.setCellValue("Nacionalidad");
			celda5.setCellStyle(hssfCellStyleCabecera);
			celda6.setCellValue("Apoderado");
			celda6.setCellStyle(hssfCellStyleCabecera);
			celda7.setCellValue("Empresa");
			celda7.setCellStyle(hssfCellStyleCabecera);
			celda8.setCellValue("Tipo Accionista");
			celda8.setCellStyle(hssfCellStyleCabecera);
			celda9.setCellValue("% de Participacion");
			celda9.setCellStyle(hssfCellStyleCabecera);
			celda10.setCellValue("Cta. Corriente");
			celda10.setCellStyle(hssfCellStyleCabecera);
			celda11.setCellValue("Banco");
			celda11.setCellStyle(hssfCellStyleCabecera);
			celda12.setCellValue("Estado");
			celda12.setCellStyle(hssfCellStyleCabecera);
			celda13.setCellValue("Cantidad Acciones Tipo A");
			celda13.setCellStyle(hssfCellStyleCabecera);
			celda14.setCellValue("Total Acciones");
			celda14.setCellStyle(hssfCellStyleCabecera);

			int contador = 0;
			for (int filas = 1; filas < listAccionista.size(); filas++) {

				fila = hojaTraspasos.createRow(filas);

				for (int columnas = 0; columnas < 15; columnas++) {

					switch (columnas) {

					case 0:						
						celda = fila.createCell(columnas);
						celda.setCellStyle(hssfCellStyleAlignRight);
						dato = (filas) + "";
						celda.setCellValue(dato);
						break;
					case 1:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getRut();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 2:						
						celda = fila.createCell(columnas);
						celda.setCellStyle(hssfCellStyleAlignCenter);
						dato = listAccionista.get(contador).getNombre() + " " + listAccionista.get(contador).getApellido_paterno() + " " + listAccionista.get(contador).getApellido_materno();
						celda.setCellValue(dato);
						break;
					case 3:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getDireccion() + " " + listAccionista.get(contador).getComuna() + " " +
								listAccionista.get(contador).getCiudad();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 4:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getNacionalidad().getNacionalidad();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 5:						
						celda = fila.createCell(columnas);
						if(listAccionista.get(contador).getApoderado() == null) {
							dato = "/*Sin Apoderado*/";
						}else {
							dato = listAccionista.get(contador).getApoderado().getRut();
						}
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 6:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getEmpresa().getRazonSocial();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 7:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getTipoAccionista().getDescripcion_tipo_accionista();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 8:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getPorcentaje_participacion() + "";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 9:						
						celda = fila.createCell(columnas);
						String numeroC = "/*Sin Cta. Coriente*/";
						for(CuentaCorriente c : listAccionista.get(contador).getCuentaCorriente()) {
							numeroC = c.getNumeroCuenta() + "";
							break;
						}
						dato = numeroC ;
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 10:						
						celda = fila.createCell(columnas);
						String nombreB = "/*Sin Banco*/";
						for(CuentaCorriente c : listAccionista.get(contador).getCuentaCorriente()) {
							nombreB = c.getNombreBanco();
							break;
						}
						dato = nombreB;
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 11:						
						celda = fila.createCell(columnas);
						if(listAccionista.get(contador).getEstado() == 0) {
							dato = "Eliminado";
						}else {
							dato = "Activo";
						}
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 12:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getCantidad_acciones_serie_a() +"";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 13:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getCantidad_acciones_serie_a() + "";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
						
					}
				}
				contador++;
			}
			workbook.write(archivo);
			archivo.close();
			respuesta = "1";
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return respuesta;
	}

	public String reporteEXCELAccionistasMayoritariosTipoB() {
		String respuesta = "";
		
		List<Accionista> listAccionista = accionistaService.listAccionistasForTop10AccionTypeB();
		
		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaEXCEL);
		ruta += "\\Acionistas\\Accionistas_AccionesTipoB_TOP10\\Reporte_Accionistas_" + FechaActual + "_" + HoraActual + ".xls";
		
		File carpeta = new File(ruta);
		carpeta.getParentFile().mkdirs();
		HSSFRow fila;
		HSSFCell celda;
		String dato = "";
		
		try {
			
			FileOutputStream archivo = new FileOutputStream(ruta);
			HSSFWorkbook workbook = new HSSFWorkbook();

			HSSFSheet hojaTraspasos = workbook.createSheet("Todos Los Accionistas Mayoritarios Tipo B");
			hojaTraspasos.setColumnWidth(0, 3000);
			hojaTraspasos.setColumnWidth(1, 5000);
			hojaTraspasos.setColumnWidth(2, 8000);
			hojaTraspasos.setColumnWidth(3, 14000);
			hojaTraspasos.setColumnWidth(4, 8000);
			hojaTraspasos.setColumnWidth(5, 5000);
			hojaTraspasos.setColumnWidth(6, 10000);
			hojaTraspasos.setColumnWidth(7, 10000);
			hojaTraspasos.setColumnWidth(8, 5000);
			hojaTraspasos.setColumnWidth(9, 4000);
			hojaTraspasos.setColumnWidth(10, 4000);
			hojaTraspasos.setColumnWidth(11, 4000);
			hojaTraspasos.setColumnWidth(12, 4000);
			hojaTraspasos.setColumnWidth(13, 4000);
			
			
			HSSFRow encabezado = hojaTraspasos.createRow(0);
			
			HSSFCellStyle hssfCellStyleCabecera = workbook.createCellStyle();
			HSSFCellStyle hssfCellStyleAlignCenter = workbook.createCellStyle();
			HSSFCellStyle hssfCellStyleAlignRight = workbook.createCellStyle();
			
			hssfCellStyleAlignCenter.setAlignment(CellStyle.ALIGN_CENTER);
			
			hssfCellStyleAlignRight.setAlignment(CellStyle.ALIGN_RIGHT);
			
			HSSFCellStyle estiloNombre = workbook.createCellStyle();
			estiloNombre.setWrapText(true);
			
			hssfCellStyleCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			hssfCellStyleCabecera.setFillForegroundColor(HSSFColor.BLACK.index);
			hssfCellStyleCabecera.setAlignment(CellStyle.ALIGN_CENTER);
			hssfCellStyleCabecera.setVerticalAlignment(CellStyle.ALIGN_CENTER);
			hssfCellStyleCabecera.setWrapText(true);

			HSSFFont hssfFont = workbook.createFont();
			hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			hssfFont.setColor(HSSFColor.WHITE.index);

			hssfCellStyleCabecera.setFont(hssfFont);

			HSSFCell celda1 = encabezado.createCell(0);
			HSSFCell celda2 = encabezado.createCell(1);
			HSSFCell celda3 = encabezado.createCell(2);
			HSSFCell celda4 = encabezado.createCell(3);
			HSSFCell celda5 = encabezado.createCell(4);
			HSSFCell celda6 = encabezado.createCell(5);
			HSSFCell celda7 = encabezado.createCell(6);
			HSSFCell celda8 = encabezado.createCell(7);
			HSSFCell celda9 = encabezado.createCell(8);
			HSSFCell celda10 = encabezado.createCell(9);
			HSSFCell celda11 = encabezado.createCell(10);
			HSSFCell celda12 = encabezado.createCell(11);
			HSSFCell celda13 = encabezado.createCell(12);
			HSSFCell celda14 = encabezado.createCell(13);

			celda1.setCellValue("#");
			celda1.setCellStyle(hssfCellStyleCabecera);
			celda2.setCellValue("Rut Accionista");
			celda2.setCellStyle(hssfCellStyleCabecera);
			celda3.setCellValue("Accionista");
			celda3.setCellStyle(hssfCellStyleCabecera);
			celda4.setCellValue("Direccion");
			celda4.setCellStyle(hssfCellStyleCabecera);
			celda5.setCellValue("Nacionalidad");
			celda5.setCellStyle(hssfCellStyleCabecera);
			celda6.setCellValue("Apoderado");
			celda6.setCellStyle(hssfCellStyleCabecera);
			celda7.setCellValue("Empresa");
			celda7.setCellStyle(hssfCellStyleCabecera);
			celda8.setCellValue("Tipo Accionista");
			celda8.setCellStyle(hssfCellStyleCabecera);
			celda9.setCellValue("% de Participacion");
			celda9.setCellStyle(hssfCellStyleCabecera);
			celda10.setCellValue("Cta. Corriente");
			celda10.setCellStyle(hssfCellStyleCabecera);
			celda11.setCellValue("Banco");
			celda11.setCellStyle(hssfCellStyleCabecera);
			celda12.setCellValue("Estado");
			celda12.setCellStyle(hssfCellStyleCabecera);
			celda13.setCellValue("Cantidad Acciones Tipo B");
			celda13.setCellStyle(hssfCellStyleCabecera);
			celda14.setCellValue("Total Acciones");
			celda14.setCellStyle(hssfCellStyleCabecera);

			int contador = 0;
			for (int filas = 1; filas < listAccionista.size(); filas++) {

				fila = hojaTraspasos.createRow(filas);

				for (int columnas = 0; columnas < 15; columnas++) {

					switch (columnas) {

					case 0:						
						celda = fila.createCell(columnas);
						celda.setCellStyle(hssfCellStyleAlignRight);
						dato = (filas) + "";
						celda.setCellValue(dato);
						break;
					case 1:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getRut();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 2:						
						celda = fila.createCell(columnas);
						celda.setCellStyle(hssfCellStyleAlignCenter);
						dato = listAccionista.get(contador).getNombre() + " " + listAccionista.get(contador).getApellido_paterno() + " " + listAccionista.get(contador).getApellido_materno();
						celda.setCellValue(dato);
						break;
					case 3:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getDireccion() + " " + listAccionista.get(contador).getComuna() + " " +
								listAccionista.get(contador).getCiudad();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 4:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getNacionalidad().getNacionalidad();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 5:						
						celda = fila.createCell(columnas);
						if(listAccionista.get(contador).getApoderado() == null) {
							dato = "/*Sin Apoderado*/";
						}else {
							dato = listAccionista.get(contador).getApoderado().getRut();
						}
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 6:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getEmpresa().getRazonSocial();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 7:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getTipoAccionista().getDescripcion_tipo_accionista();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 8:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getPorcentaje_participacion() + "";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 9:						
						celda = fila.createCell(columnas);
						String numeroC = "/*Sin Cta. Coriente*/";
						for(CuentaCorriente c : listAccionista.get(contador).getCuentaCorriente()) {
							numeroC = c.getNumeroCuenta() + "";
							break;
						}
						dato = numeroC ;
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 10:						
						celda = fila.createCell(columnas);
						String nombreB = "/*Sin Banco*/";
						for(CuentaCorriente c : listAccionista.get(contador).getCuentaCorriente()) {
							nombreB = c.getNombreBanco();
							break;
						}
						dato = nombreB;
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 11:						
						celda = fila.createCell(columnas);
						if(listAccionista.get(contador).getEstado() == 0) {
							dato = "Eliminado";
						}else {
							dato = "Activo";
						}
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 12:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getCantidad_acciones_serie_b() +"";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 13:						
						celda = fila.createCell(columnas);
						dato = listAccionista.get(contador).getCantidad_acciones_serie_b() + "";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
						
					}
				}
				contador++;
			}
			workbook.write(archivo);
			archivo.close();
			respuesta = "1";
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return respuesta;
	}

	public String reporteEXCELDividendo() {
		String respuesta = "";
		
		List<Dividendo> listDividendo = dividendoService.listAllDividendoEntity();
		
		String FechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String HoraActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
		
		String ruta = RutaReportes.verificarRuta(RutaReportes.rutaEXCEL);
		ruta += "\\Dividendo\\Reporte_Dividendo_" + FechaActual + "_" + HoraActual + ".xls";
		
		File carpeta = new File(ruta);
		carpeta.getParentFile().mkdirs();
		HSSFRow fila;
		HSSFCell celda;
		String dato = "";
		
		try {
			
			FileOutputStream archivo = new FileOutputStream(ruta);
			HSSFWorkbook workbook = new HSSFWorkbook();

			HSSFSheet hojaDividendo = workbook.createSheet("Dividendo");
			hojaDividendo.setColumnWidth(0, 3000);
			hojaDividendo.setColumnWidth(1, 3000);
			hojaDividendo.setColumnWidth(2, 3000);
			hojaDividendo.setColumnWidth(3, 5000);
			hojaDividendo.setColumnWidth(4, 10000);
			hojaDividendo.setColumnWidth(5, 5000);
			hojaDividendo.setColumnWidth(6, 5000);
			hojaDividendo.setColumnWidth(7, 5000);
			hojaDividendo.setColumnWidth(8, 5000);
			hojaDividendo.setColumnWidth(9, 10000);
			hojaDividendo.setColumnWidth(10, 10000);
			
			
			HSSFRow encabezado = hojaDividendo.createRow(0);
			HSSFCellStyle hssfCellStyleCabecera = workbook.createCellStyle();
			HSSFCellStyle hssfCellStyleAlignRight = workbook.createCellStyle();
			HSSFCellStyle hssfCellStyleAlignCenter = workbook.createCellStyle();
			
			hssfCellStyleAlignCenter.setAlignment(CellStyle.ALIGN_CENTER);
			
			hssfCellStyleAlignRight.setAlignment(CellStyle.ALIGN_RIGHT);
			
			HSSFCellStyle estiloNombre = workbook.createCellStyle();
			estiloNombre.setWrapText(true);
			
			hssfCellStyleCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			hssfCellStyleCabecera.setFillForegroundColor(HSSFColor.BLACK.index);
			hssfCellStyleCabecera.setAlignment(CellStyle.ALIGN_CENTER);
			hssfCellStyleCabecera.setVerticalAlignment(CellStyle.ALIGN_CENTER);
			hssfCellStyleCabecera.setWrapText(true);
			
			
			
			HSSFFont hssfFont = workbook.createFont();
			hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			hssfFont.setColor(HSSFColor.WHITE.index);

			hssfCellStyleCabecera.setFont(hssfFont);

			HSSFCell celda1 = encabezado.createCell(0);
			HSSFCell celda2 = encabezado.createCell(1);
			HSSFCell celda3 = encabezado.createCell(2);
			HSSFCell celda4 = encabezado.createCell(3);
			HSSFCell celda5 = encabezado.createCell(4);
			HSSFCell celda6 = encabezado.createCell(5);
			HSSFCell celda7 = encabezado.createCell(6);
			HSSFCell celda8 = encabezado.createCell(7);
			HSSFCell celda9 = encabezado.createCell(8);
			HSSFCell celda10 = encabezado.createCell(9);
			HSSFCell celda11 = encabezado.createCell(10);

			celda1.setCellValue("#");
			celda1.setCellStyle(hssfCellStyleCabecera);
			celda2.setCellValue("ID Junta Accionista");
			celda2.setCellStyle(hssfCellStyleCabecera);
			celda3.setCellValue("ID Dividendo");
			celda3.setCellStyle(hssfCellStyleCabecera);
			celda4.setCellValue("Rut Accionista");
			celda4.setCellStyle(hssfCellStyleCabecera);
			celda5.setCellValue("Nombre Accionista");
			celda5.setCellStyle(hssfCellStyleCabecera);
			celda6.setCellValue("Dividendo Total Acciones Tipo A");
			celda6.setCellStyle(hssfCellStyleCabecera);
			celda7.setCellValue("Dividendo Total Acciones Tipo B");
			celda7.setCellStyle(hssfCellStyleCabecera);
			celda8.setCellValue("Dividendo Total por Acciones");
			celda8.setCellStyle(hssfCellStyleCabecera);
			celda9.setCellValue("Monto a Pagar");
			celda9.setCellStyle(hssfCellStyleCabecera);
			celda10.setCellValue("Cta. Corriente");
			celda10.setCellStyle(hssfCellStyleCabecera);
			celda11.setCellValue("Banco");
			celda11.setCellStyle(hssfCellStyleCabecera);

			int contador = 0;
			for (int filas = 1; filas < listDividendo.size(); filas++) {

				fila = hojaDividendo.createRow(filas);

				for (int columnas = 0; columnas < 12; columnas++) {

					switch (columnas) {

					case 0:						
						celda = fila.createCell(columnas);
						dato = (filas ) + "";
						celda.setCellStyle(hssfCellStyleAlignRight);
						celda.setCellValue(dato);
						break;
					case 1:						
						celda = fila.createCell(columnas);
						celda.setCellStyle(hssfCellStyleAlignRight);
						dato = listDividendo.get(contador).getJuntaAccionista().getIdJuntaAccionista() + "";
						celda.setCellValue(dato);
						break;
					case 2:						
						celda = fila.createCell(columnas);
						celda.setCellStyle(hssfCellStyleAlignCenter);
						dato = listDividendo.get(contador).getIdDividendo() + "";
						celda.setCellValue(dato);
						break;
					case 3:						
						celda = fila.createCell(columnas);
						dato = listDividendo.get(contador).getAccionista().getRut();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 4:					
						celda = fila.createCell(columnas);
						dato = listDividendo.get(contador).getAccionista().getNombre() + " " +
								listDividendo.get(contador).getAccionista().getApellido_paterno() + " " +
								listDividendo.get(contador).getAccionista().getApellido_materno();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 5:						
						celda = fila.createCell(columnas);
						dato = "$" + listDividendo.get(contador).getDividendoAccionA() + " ";
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 6:						
						celda = fila.createCell(columnas);
						dato = "$" + listDividendo.get(contador).getDividendoAccionB();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 7:						
						celda = fila.createCell(columnas);
						dato = "$" + listDividendo.get(contador).getDividendoTotal();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 8:						
						celda = fila.createCell(columnas);
						dato = "$" + listDividendo.get(contador).getDividendoTotal();
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 9:						
						celda = fila.createCell(columnas);
						String numeroC = "/*Sin Cta. Coriente*/";
						for(CuentaCorriente c : listDividendo.get(contador).getAccionista().getCuentaCorriente()) {
							numeroC = c.getNumeroCuenta() + "";
							break;
						}
						dato = numeroC ;
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
					case 10:						
						celda = fila.createCell(columnas);
						String nombreB = "/*Sin Banco*/";
						for(CuentaCorriente c : listDividendo.get(contador).getAccionista().getCuentaCorriente()) {
							nombreB = c.getNombreBanco();
							break;
						}
						dato = nombreB;
						celda.setCellStyle(hssfCellStyleAlignCenter);
						celda.setCellValue(dato);
						break;
						
					}
				}
				contador++;
			}
			workbook.write(archivo);
			archivo.close();
			respuesta = "1";
			Runtime.getRuntime().exec("cmd /c start " + ruta);
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return respuesta;
	}
}
