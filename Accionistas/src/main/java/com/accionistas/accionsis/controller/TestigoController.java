package com.accionistas.accionsis.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.component.TestigoConverter;
import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.entity.Testigo;
import com.accionistas.accionsis.model.TestigoModel;
import com.accionistas.accionsis.service.RegionesService;
import com.accionistas.accionsis.service.TestigoService;

/**
 * Clase TestigoController.
 */
@Controller
@RequestMapping("testigos")
public class TestigoController {

/** LOG constante. */
private static final Log LOG = LogFactory.getLog(AccionistaController.class);
	
	/** The testigo service. */
	@Autowired
	@Qualifier("testigoServiceImp")
	private TestigoService testigoService;
	
	@Autowired
	@Qualifier("testigoConverter")
	private TestigoConverter testigoConverter;
	
	@Autowired
	@Qualifier("regionesServiceImp")
	private RegionesService regionesService;
	
	/**
	 * Lista todos los testigos.
	 *
	 * @return the model and view
	 */
	@GetMapping("/listtestigos")
	public ModelAndView listAllTestigos() {
		LOG.info("Call: "+ "listAllTestigos() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.TESTIGOS_VIEW);
		mav.addObject("testigos", testigoService.listAllTestigo_active());
		return mav;
	}	
	
	@GetMapping("/testigoAdmin")
	public ModelAndView listAllTestigoMantenedor() {
		LOG.info("Call: " + "listAllTestigo() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.TESTIGOS_LIST);
		List<TestigoModel> tes = testigoService.listAllTestigo();			
		mav.addObject("testigos", tes );
		return mav;
	}
	
	@RequestMapping(value="/addtestigo", method=RequestMethod.GET)
	public @ResponseBody String addTestigo(@RequestParam(name="id", required=false) String id, @RequestParam String nombreModel, @RequestParam String rutModel, 
			@RequestParam String apellido_paternoModel, @RequestParam String apellido_maternoModel,
			@RequestParam String emailModel, @RequestParam String fonoModel) {
		String respuesta = "";
		TestigoModel testigoModel = null;
		LOG.info("Call: "+"addTestigo()");
		
		if(!id.equalsIgnoreCase("0")) {
			int idTestipo = Integer.parseInt(id);
			testigoModel = testigoService.findTestigoByidTestigo(idTestipo);
		}else {
			testigoModel = new TestigoModel();
		}
		
		testigoModel.setRutModel(rutModel);
		testigoModel.setNombreModel(nombreModel);
		testigoModel.setApellido_paternoModel(apellido_paternoModel);
		testigoModel.setApellido_maternoModel(apellido_maternoModel);
		testigoModel.setEmailModel(emailModel);
		testigoModel.setFonoModel(fonoModel);
		
		if(rutModel != "" && nombreModel != "" && apellido_paternoModel != "" &&
				apellido_maternoModel != "" && emailModel != "" && fonoModel != "") {
			if(null != testigoService.addTestigo(testigoModel)){
	//			model.addAttribute("result", 1);
				respuesta = "1";
			}
		} else {
//			model.addAttribute("result", 0);
			respuesta = "2";
		}
		return respuesta;
	}
	

	@GetMapping("/addtestigoform")
	private ModelAndView redirectToAddTestigoForm(@RequestParam(name="rut", required=false) String rut,
			Model model) {
		ModelAndView mav = new ModelAndView(ViewConstant.TESTIGO_FORM);
		TestigoModel testigoModel = new TestigoModel();
		if(!rut.equals("agregarTestigo")) {
			testigoModel = testigoService.findTestigoByRutModel(rut);
		}
		mav.addObject("testigos", testigoService.listAllTestigo());	
		mav.addObject(testigoModel);
		return mav;		
	}

	@GetMapping("/cancel")
	public ModelAndView cancel() {
		LOG.info("Call: "+ "listAllTestigos() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.TESTIGOS_VIEW);
		mav.addObject("testigos", testigoService.listAllTestigo());
		return mav;
	}
	
	@GetMapping("/removetestigo")
	public ModelAndView removeUsuario(@RequestParam(name="rut", required=true) String rut) {
		testigoService.removeTestigo(rut);
		return listAllTestigos();
	}

	@RequestMapping(value="del", method=RequestMethod.GET)
	public @ResponseBody String removeTestigo(@RequestParam String rut) {
		LOG.info("Call: "+ "removeTestigo()" + "-- Param: "+ rut.toString());
			
			String message = "Testigo con el rut: " +rut+" eliminado";
		
			TestigoModel testigo = new TestigoModel();		
			testigo.setRutModel(rut);
			testigo = testigoService.findTestigoByRutModel(rut);
			testigo.setEstado(0);			
			testigo = testigoService.addTestigo(testigo);
			
		return message;
	}
	
	@RequestMapping(value="act", method=RequestMethod.GET)
	public @ResponseBody String updateTestigo(@RequestParam String rut) {
		LOG.info("Call: "+ "removeTestigo()" + "-- Param: "+ rut.toString());
			
			String message = "Testigo con el rut: " +rut+" activado";
		
			Testigo testigo = new Testigo();		
			testigo.setRut(rut);
			testigo = testigoConverter.modelo2Entidad(testigoService.findTestigoByRutModel(rut));
			testigo.setEstado(1);			
			testigo = testigoService.updateTestigo(testigo);
			
		return message;
	}
}
