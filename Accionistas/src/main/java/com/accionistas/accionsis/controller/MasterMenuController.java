package com.accionistas.accionsis.controller;

import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MasterMenuController {
	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(MasterMenuController.class);

	@GetMapping(value= {"/principal"})
	public String root(Locale locale, ModelMap model) {
		LOG.info("METHOD: MASTERMENU");
		model.addAttribute("content", "helloWorldView");     
		return "index";
	}
}
