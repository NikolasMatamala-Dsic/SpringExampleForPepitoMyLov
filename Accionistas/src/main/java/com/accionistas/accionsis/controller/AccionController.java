package com.accionistas.accionsis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.component.AccionConverter;
import com.accionistas.accionsis.component.EmpresaConverter;
import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.entity.Empresa;
import com.accionistas.accionsis.model.AccionModel;
import com.accionistas.accionsis.model.EmpresaModel;
import com.accionistas.accionsis.service.AccionService;
import com.accionistas.accionsis.service.EmpresaService;
import com.accionistas.accionsis.utility.JSONResponse;


@Controller
@RequestMapping("/accion")
public class AccionController {

	private static final Log LOG = LogFactory.getLog(AccionController.class);
	@Autowired
	@Qualifier("accionServiceImp")
	private AccionService accionService;
	
	@Autowired
	@Qualifier("empresaServiceImp")
	private EmpresaService empresaService;

	@Autowired
	@Qualifier("empresaConverter")
	private EmpresaConverter empresaConverter;
	
	@Autowired
	@Qualifier("accionConverter")
	private AccionConverter accionConverter;
	
	
	@GetMapping("/listaaccion")
	public ModelAndView listAllacciones() {
		LOG.info("Call: " + "listAllacciones() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.ACCIONES_VIEW);
		mav.addObject("acciones", accionService.listAllAccion());
		return mav;
	}

//	@PostMapping("/addaccion")
	@RequestMapping(value="/addaccion", method=RequestMethod.POST)
	public String addAccion(@ModelAttribute(name="accionModel") AccionModel accionModel, Model model) {			
		LOG.info("Call: "+"addAccion()"+"--Param: "+ accionModel.toString());
		String salida = "";
		
		EmpresaModel empresa = empresaConverter.entidad2Model(empresaService.findEmpresaActiva());
		
		accionModel.setEmpresaModel(empresa);
		
		if(null != accionService.addAccion(accionModel)){
			model.addAttribute("result", 1);
		} else {
			model.addAttribute("result", 0);
		}
		salida = "redirect:/accion/listaaccion";
		
		return salida;
	}
	
		
	@GetMapping("/addaccionesform")
	private String redirectToAddAccionForm(@RequestParam(name="id", required=false) int id, 
			Model model) {
		AccionModel accionModel = new AccionModel();
		if(id != 0) {
			accionModel = accionService.findAccionById_accionModel(id);	
		}
		model.addAttribute("accionModel", accionModel);
		return ViewConstant.ACCIONES_FORM;
	}
	
	@RequestMapping(value="del", method=RequestMethod.GET)
	public ModelAndView removeAccion(@RequestParam(name="id", required=true)int id, Model model) {
		accionService.removeAccion(id);
		return listAllacciones();
	}
	
	
	@GetMapping("/cancel")
	public ModelAndView cancel() {
		LOG.info("Call: "+ "listAllAcciones() -- Al cancelar");
		ModelAndView mav = new ModelAndView(ViewConstant.ACCIONES_VIEW);
		mav.addObject("acciones", accionService.listAllAccion());
		return mav;
	}
	
	@RequestMapping(value="/getAccionActiva", method=RequestMethod.GET)
	public @ResponseBody String getAccionActiva() {
		String msj = "";
		Empresa empresa =  empresaService.findEmpresaActiva();
		AccionModel accion = accionConverter.entidad2Modelo(empresa.getAccion());
		List<JSONResponse> resultado = new ArrayList<>();
		
		resultado.add(new JSONResponse(JSONResponse.valorAccionTipoAActual, accion.getValorAModel()+""));
		resultado.add(new JSONResponse(JSONResponse.valorAccionTipoBActual, accion.getValorBModel()+""));
		ObjectMapper mapper = new ObjectMapper();
		try {
			msj = mapper.writeValueAsString(resultado);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			msj = "error";
			e.printStackTrace();
		}
		
		return msj;
	}
	
	
	
}
