package com.accionistas.accionsis.controller;


import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.component.TraspasoaccionesConverter;
import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.entity.Traspasoacciones;
import com.accionistas.accionsis.service.AccionistaService;
import com.accionistas.accionsis.service.ApoderadoService;
import com.accionistas.accionsis.service.TraspasoaccionesService;

@Controller
@RequestMapping("/traspasoAcciones")
public class traspasoAccionesController {
	
	private static final Log LOG = LogFactory.getLog(traspasoAccionesController.class);
	
	@Autowired
	@Qualifier("traspasoaccionesServiceImp")
	private TraspasoaccionesService traspasoaccionesService;
	
	@Autowired
	@Qualifier("accionistaServiceImp")
	private AccionistaService accionistaService;
	
	@Autowired
	@Qualifier("apoderadoServiceImp")
	private ApoderadoService apoderadoService;
	
	@Autowired
	@Qualifier("traspasoaccionesConverter")
	private TraspasoaccionesConverter traspasoaccionesConverter;
	
	@GetMapping("/listTraspasos")
	public ModelAndView listAllTraspasoAcciones() {
		
		LOG.info("Call: " + "listAllTraspasoAcciones -- desde Controller");
		ModelAndView mav = new ModelAndView(ViewConstant.TRASPASOACCIONES_VIEW);
		
		mav.addObject("traspasoAcciones", traspasoaccionesService.listAllTraspasosacciones());
		return mav;
	}
	
	@GetMapping("/addTraspasoform")
	private ModelAndView redirectToAddTestigoForm(@RequestParam(name="id", required=false) int id,
			Model model) {
		ModelAndView mav = new ModelAndView(ViewConstant.TRASPASOACCIONES_FROM);
		mav.addObject(traspasoaccionesService.findTraspasoaccionesModelById(id));			
		return mav;		
	}
	
	@GetMapping("/cancel")
	public ModelAndView cancel() {
		LOG.info("Call: "+ "listAllTraspaso() -- Al cancelar");
		ModelAndView mav = new ModelAndView(ViewConstant.TRASPASOACCIONES_VIEW);
		mav.addObject(traspasoaccionesService.listAllTraspasosacciones());
		return mav;
	}
	
	@RequestMapping(value="/update", method=RequestMethod.GET)
	public @ResponseBody int updateTraspaso(@RequestParam int idTraspasoAccionesModel, @RequestParam String fecha_ventaModel, @RequestParam String observacionModel) {
		int msj = 0;
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		java.util.Date fechaAux = null;
		try {
			fechaAux = formatter.parse(fecha_ventaModel);			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("");
			System.out.println("");
			System.out.println("Error al convertir la fecha, ingresa una fecha correcta -> "+ fecha_ventaModel);
			System.out.println("");
			System.out.println("");
			msj = 4;
			e.printStackTrace();
			return msj;
		}
		Date fecha = new Date(fechaAux.getTime());
		Traspasoacciones tras = new Traspasoacciones();
		tras.setIdidTraspasoAcciones(idTraspasoAccionesModel);
		tras = traspasoaccionesService.findTraspasoaccionesByTraspasoacciones(tras);
		
		tras.setObservacion(observacionModel); 
		tras.setFecha_venta(fecha);
		
		tras = traspasoaccionesService.updateTraspaso(tras);
		if(tras !=null ) {
			msj = 3;
		}else {
			msj = 6;
		}
		return msj;
		
	}

}
