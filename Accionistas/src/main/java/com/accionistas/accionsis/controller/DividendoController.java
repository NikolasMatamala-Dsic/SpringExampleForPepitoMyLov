package com.accionistas.accionsis.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.component.DividendoConverter;
import com.accionistas.accionsis.component.EmpresaConverter;
import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.entity.Accion;
import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.entity.Empresa;
import com.accionistas.accionsis.model.DividendoModel;
import com.accionistas.accionsis.service.AccionService;
import com.accionistas.accionsis.service.DividendoService;
import com.accionistas.accionsis.service.EmpresaService;

//TODO: Auto-generated Javadoc
/*
 * Clase DividendoController
 */
@Controller
@RequestMapping("/dividendos")
public class DividendoController {

	/*LOG constante*/
	private static final Log LOG = LogFactory.getLog(DividendoController.class);
	
	/*The dividendo service.*/
	@Autowired
	@Qualifier("dividendoServiceImp")
	private DividendoService dividendoService;
	
	/*El accionista service*/
	@Autowired
	@Qualifier("accionServiceImp")
	private AccionService accionService;
	
	@Autowired
	@Qualifier("DividendoConverter")
	private DividendoConverter dividendoConverter;
	
	@Autowired
	@Qualifier("empresaServiceImp")
	private EmpresaService empresaService;

	@Autowired
	@Qualifier("empresaConverter")
	private EmpresaConverter empresaConverter;
	
	
	/*
	 * Listar todos los dividendos.
	 * 
	 * @return the model and view
	 */
	@GetMapping("/listadoDividendos")
	public ModelAndView listAllDividendo() {
		LOG.info("ListarDividendo: " + "listAllDividendo() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.DIVIDENDO_VIEW);
		mav.addObject("dividendo", dividendoService.listAllDividendo());
		return mav;
	}
	
	@PostMapping("/adddividendo")
	public String addDividendo(@ModelAttribute(name="dividendoModel") DividendoModel dividendoModel, Model model) {
		LOG.info("Call: " + "addDividendo()" + "--Param: " + dividendoModel.toString());
		if(null != dividendoService.addDividendo(dividendoModel)) {
			model.addAttribute("result", 1);
		} else {
			model.addAttribute("result", 0);
		}
		return "redirect:/dividendos/listadoDividendos";	
	}
	
	@GetMapping("/adddividendoform")
	private String redirectToAddDividendoForm(@RequestParam(name="accionistaRut", required=false) String accionistaRut, Model model) {
		DividendoModel dividendoModel = new DividendoModel();
		Accionista accionista = new Accionista();
		accionista.setRut(accionistaRut);
		if(accionistaRut.equals("adddividendo")) {
			dividendoModel =  dividendoConverter.entidad2Model(dividendoService.findDividendoByAccionista(accionista));
		}
		model.addAttribute("dividendoModel", dividendoModel);
		return ViewConstant.DIVIDENDO_FORM;
	}
	
	@GetMapping("/cancel")
	public ModelAndView cancel() {
		LOG.info("Call: " + "cancelDividendo() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.DIVIDENDO_VIEW);
		mav.addObject("dividendo", dividendoService.listAllDividendo());
		return mav;
	}
	
	@GetMapping("/removedividendo")
	public ModelAndView removeDividendo(@RequestParam(name="accionistaRut", required=true) String accionistaRut, Model model) {
		Accionista accionista = new Accionista();
		accionista.setRut(accionistaRut);
		dividendoService.removeDividendo(accionista);
		return listAllDividendo();
	}
	
	@GetMapping("/verdividendo")
	public ModelAndView verDividendos() {
		LOG.info("Call: " + "listAllVErDividendo() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.DIVIDENDO_VIEW);
		List<DividendoModel> lista = new ArrayList<DividendoModel>();
		lista = dividendoService.listAllDividendo();
		mav.addObject("listaDivendo", lista );
		LOG.info("Call" + mav.toString());
		return mav;
	}
	
	@PostMapping("/calcularDividendo")
	public ModelAndView calcularDividendo(@RequestParam(name="accion", required=false) String accion, @RequestParam(name="cantidadAccion", required=false) double cantidad, Model model) {
		LOG.info("Call: " + "calcularDividendo() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.CALCULARDIVIDENDO_VIEW);
		Empresa empresa = empresaService.findEmpresaActiva();
		Accion acc = empresa.getAccion();
		mav.addObject("acciones",acc);
		LOG.info("Valor acciones: " + acc.getValorA() + " - " + acc.getValorB());
		LOG.info("Valor acciones: " + accion + " - " + cantidad);

		double total = 0;
		
		if(accion.equals("A")) {
			
			total = acc.getValorA()*cantidad;
		}else {
			total = acc.getValorB()*cantidad;
		}
		mav.addObject("total",total);
		return mav;
	}
		
	@GetMapping("/veraccion")
	public ModelAndView verAccion() {
		LOG.info("Call: " + "verAccion() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.CALCULARDIVIDENDO_VIEW);
		Accion acc = accionService.findAccionById_accion(6);
		mav.addObject("acciones",acc);
		return mav;
	}
	
	@GetMapping("/verReporte")
	public ModelAndView verReporte() {
		ModelAndView mav = new ModelAndView(ViewConstant.DIVIDENDO_REPORTE);
		return mav;
	}
	
}
