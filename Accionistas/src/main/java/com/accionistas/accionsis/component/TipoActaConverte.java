package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.TipoActa;
import com.accionistas.accionsis.model.TipoActaModel;

@Component("tipoActaConverter")
public class TipoActaConverte {
	
	private static final Log LOG = LogFactory.getLog(TipoActaConverte.class);
	
	public TipoActa modelo2Entidad(TipoActaModel tipoActaModel) {
		
		if(tipoActaModel == null) {return null;}
		
		LOG.info("Convirtiendo tipoActa de modelo a entidad");
		TipoActa tipoActa = new TipoActa();
		tipoActa.setIdTipoActa(tipoActaModel.getIdTipoActa());
		tipoActa.setDescripcion(tipoActaModel.getDescripcion());
		
		return tipoActa;
	}
	
	public TipoActaModel entidad2Modelo(TipoActa tipoActa) {
		
		if(tipoActa == null) {return null;}
		
		LOG.info("Convirtiendo tipoActa de entidad a modelo");
		TipoActaModel tipoActaModel = new TipoActaModel();
		tipoActaModel.setIdTipoActa(tipoActa.getIdTipoActa());
		tipoActaModel.setDescripcion(tipoActa.getDescripcion());
		
		return tipoActaModel;
	}


}
