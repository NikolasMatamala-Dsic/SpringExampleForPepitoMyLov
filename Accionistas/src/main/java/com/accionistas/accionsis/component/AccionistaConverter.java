package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.model.AccionistaModel;

/**
 * Clase AccionistaConverter.
 */
@Component("accionistaConverter")
public class AccionistaConverter {
	
	EmpresaConverter eC = new EmpresaConverter();
	NacionalidadConverter nC = new NacionalidadConverter();
	TipoAccionistaConverter taC = new TipoAccionistaConverter();
	ApoderadoConverter apC = new ApoderadoConverter();
	RegionConverter rC = new RegionConverter();
	
	/** LOG Constante. */
	private static final Log LOG = LogFactory.getLog(AccionistaConverter.class);
	
	/**
	 * Modelo a entidad.
	 *
	 * @param accionistaModel AccionistaModel 
	 * @return accionista
	 */
	public Accionista modelo2Entidad(AccionistaModel accionistaModel) {
		LOG.info("Convirtiendo accionista de modelo a entidad");
		if(accionistaModel == null) {
			return null;
		}
		Accionista accionista= new Accionista();
		accionista.setIdAccionista(accionistaModel.getIdAccionistaModel());
		accionista.setRut(accionistaModel.getRutModel());
		accionista.setEmpresa(eC.modelo2Entidad(accionistaModel.getEmpresaModel()));
		accionista.setNombre(accionistaModel.getNombreModel());
		accionista.setApellido_paterno(accionistaModel.getApellido_paternoModel());
		accionista.setApellido_materno(accionistaModel.getApellido_maternoModel());
		accionista.setCantidad_acciones_serie_a(accionistaModel.getCantidad_acciones_serie_aModel());
		accionista.setCantidad_acciones_serie_b(accionistaModel.getCantidad_acciones_serie_bModel());
		accionista.setPorcentaje_participacion(accionistaModel.getPorcentaje_participacionModel());
		accionista.setFecha_nacimiento(accionistaModel.getFecha_nacimientoModel());
		accionista.setFecha_ingreso(accionistaModel.getFecha_ingresoModel());
		accionista.setEsValido(accionistaModel.getEsValidoModel());
		accionista.setEmail(accionistaModel.getEmailModel());
		accionista.setDireccion(accionistaModel.getDireccionModel());
		accionista.setFono(accionistaModel.getFonoModel());
		accionista.setNacionalidad(nC.modelo2Entidad(accionistaModel.getNacionalidadModel()));
		accionista.setTipoAccionista(taC.modelo2Entidad(accionistaModel.getTipoAccionistaModel()));
		accionista.setCiudad(accionistaModel.getCiudadModel());
		accionista.setComuna(accionistaModel.getComunaModel());
		accionista.setRegion(rC.modelo2Entidad(accionistaModel.getRegionModel()));
		accionista.setEstado(accionistaModel.getEstadoModel());
		accionista.setApoderado(apC.modelo2Entidad(accionistaModel.getApoderadoModel()));
		return accionista;
	}

	
	/**
	 * Entidad a model.
	 *
	 * @param accionista Accionista 
	 * @return accionistaModel
	 */
	//Entidad a modelo
	public AccionistaModel entidad2Model(Accionista accionista) {
		LOG.info("Convirtiendo accionista de entidad a modelo");		
		
		if(accionista == null) { return null;}
		
		AccionistaModel accionistaModel = new AccionistaModel();
		
		
		accionistaModel.setIdAccionistaModel(accionista.getIdAccionista());
		accionistaModel.setRutModel(accionista.getRut());
		accionistaModel.setEmpresaModel(eC.entidad2Model(accionista.getEmpresa()));
		accionistaModel.setNombreModel(accionista.getNombre());
		accionistaModel.setApellido_paternoModel(accionista.getApellido_paterno());
		accionistaModel.setApellido_maternoModel(accionista.getApellido_materno());
		accionistaModel.setCantidad_acciones_serie_aModel(accionista.getCantidad_acciones_serie_a());
		accionistaModel.setCantidad_acciones_serie_bModel(accionista.getCantidad_acciones_serie_b());
		accionistaModel.setPorcentaje_participacionModel(accionista.getPorcentaje_participacion());
		accionistaModel.setFecha_nacimientoModel(accionista.getFecha_nacimiento());
		accionistaModel.setFecha_ingresoModel(accionista.getFecha_ingreso());
		accionistaModel.setEsValidoModel(accionista.getEsValido());
		accionistaModel.setEmailModel(accionista.getEmail());
		accionistaModel.setDireccionModel(accionista.getDireccion());
		accionistaModel.setFonoModel(accionista.getFono());
		accionistaModel.setNacionalidadModel(nC.entidad2Model(accionista.getNacionalidad()));
		accionistaModel.setTipoAccionistaModel(taC.entidad2Model(accionista.getTipoAccionista()));
		accionistaModel.setCiudadModel(accionista.getCiudad());
		accionistaModel.setComunaModel(accionista.getComuna());
		accionistaModel.setRegionModel(rC.entidad2Modelo(accionista.getRegion()));
		accionistaModel.setEstadoModel(accionista.getEstado());
		accionistaModel.setApoderadoModel(apC.entidad2Model(accionista.getApoderado()));
		return accionistaModel;
	}
}


