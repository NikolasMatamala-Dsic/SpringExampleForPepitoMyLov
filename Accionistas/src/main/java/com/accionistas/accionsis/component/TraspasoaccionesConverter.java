package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.Traspasoacciones;
import com.accionistas.accionsis.model.TraspasoaccionesModel;

@Component("traspasoaccionesConverter")
public class TraspasoaccionesConverter {
	
	TestigoConverter tC = new TestigoConverter();
	AccionistaConverter aC = new AccionistaConverter();

	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(TraspasoaccionesConverter.class);
	
	/**
	 * Modelo a entidad.
	 * @param traspasoaccionesModel 
	 * @return traspasoacciones+
	 * 
	 */
	public Traspasoacciones modelo2Entidad(TraspasoaccionesModel traspasoaccionesModel) {
		LOG.info("Convirtiendo traspasoaccion de modelo a entidad");
		
		Traspasoacciones traspasoacciones = new Traspasoacciones();
		traspasoacciones.setIdidTraspasoAcciones(traspasoaccionesModel.getIdTraspasoAccionesModel());
		traspasoacciones.setAccionistaVendedor(aC.modelo2Entidad(traspasoaccionesModel.getRut_accionista_vendedorModel()));
		traspasoacciones.setAccionistaComprador(aC.modelo2Entidad(traspasoaccionesModel.getRut_accionista_compradorModel()));
		traspasoacciones.setFecha_venta(traspasoaccionesModel.getFecha_ventaModel());
		traspasoacciones.setCantidad_acciones_vendidas_a(traspasoaccionesModel.getCantidad_acciones_vendidas_aModel());
		traspasoacciones.setCantidad_acciones_vendidas_b(traspasoaccionesModel.getCantidad_acciones_vendidas_bModel());
		traspasoacciones.setMonto(traspasoaccionesModel.getMontoModel());
		traspasoacciones.setEs_sucesion(traspasoaccionesModel.getEs_sucesionModel());
		traspasoacciones.setTestigo(tC.modelo2Entidad(traspasoaccionesModel.getTestigoModel()));
		traspasoacciones.setObservacion(traspasoaccionesModel.getObservacionModel());
		traspasoacciones.setFecha_venta(traspasoaccionesModel.getFecha_ventaModel());
		return traspasoacciones;
	}
	
	/**
	 * Entidad a model.
	 * @param traspasoacciones  
	 * @return traspasoaccionesModel
	 */
	public TraspasoaccionesModel entidad2Model(Traspasoacciones traspasoacciones) {
		LOG.info("Convirtiendo traspasoaccion de entidad a modelo");
		
		TraspasoaccionesModel traspasoaccionesModel = new TraspasoaccionesModel();
		traspasoaccionesModel.setIdTraspasoAccionesModel(traspasoacciones.getIdidTraspasoAcciones());
		traspasoaccionesModel.setRut_accionista_vendedorModel(aC.entidad2Model(traspasoacciones.getAccionistaVendedor()));
		traspasoaccionesModel.setRut_accionista_compradorModel(aC.entidad2Model(traspasoacciones.getAccionistaComprador()));
		traspasoaccionesModel.setFecha_ventaModel(traspasoacciones.getFecha_venta());
		traspasoaccionesModel.setCantidad_acciones_vendidas_aModel(traspasoacciones.getCantidad_acciones_vendidas_a());
		traspasoaccionesModel.setCantidad_acciones_vendidas_bModel(traspasoacciones.getCantidad_acciones_vendidas_b());
		traspasoaccionesModel.setMontoModel(traspasoacciones.getMonto());
		traspasoaccionesModel.setEs_sucesionModel(traspasoacciones.getEs_sucesion());
		traspasoaccionesModel.setTestigoModel(tC.entidad2Model(traspasoacciones.getTestigo()));
		traspasoaccionesModel.setObservacionModel(traspasoacciones.getObservacion());
		traspasoaccionesModel.setFecha_ventaModel(traspasoacciones.getFecha_venta());
		return traspasoaccionesModel;
	}

	
}
