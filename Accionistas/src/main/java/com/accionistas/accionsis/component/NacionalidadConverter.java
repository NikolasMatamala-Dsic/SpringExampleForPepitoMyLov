package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.Nacionalidad;
import com.accionistas.accionsis.model.NacionalidadModel;

@Component("nacionalidadConverter")
public class NacionalidadConverter {
	
	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(NacionalidadConverter.class);
	
	/**
	 * Modelo a entidad.
	 * @param nacionalidadModel
	 * @return nacionalidad
	 */
	public Nacionalidad modelo2Entidad(NacionalidadModel nacionalidadModel) {
		LOG.info("Convirtiendo nacionalidad de modelo a entidad");
		Nacionalidad nacionalidad= new Nacionalidad();
		nacionalidad.setIdNacionalidad(nacionalidadModel.getIdNacionalidadModel());
		nacionalidad.setNacionalidad(nacionalidadModel.getNacionalidadModel());
		return nacionalidad;
	}
	
	/**
	 * Entidad a model.
	 * @param nacionalidad
	 * @return nacionalidadModel
	 */
	public NacionalidadModel entidad2Model(Nacionalidad nacionalidad) {
		LOG.info("Convirtiendo nacionalidad de entidad a modelo");
		NacionalidadModel nacionalidadModel = new NacionalidadModel();
		nacionalidadModel.setIdNacionalidadModel(nacionalidad.getIdNacionalidad());
		nacionalidadModel.setNacionalidadModel(nacionalidad.getNacionalidad());
		return nacionalidadModel;
	}

}
