package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.AsistenciaJuntaAccionista;
import com.accionistas.accionsis.model.AsistenciaJuntaAccionistaModel;

@Component("AsistenciaJuntaAccionistaConverter")
public class AsistenciaJuntaAccionistaConverter {
	
	JuntaAccionistaConverter jaC = new JuntaAccionistaConverter();
	AccionistaConverter aC = new AccionistaConverter();
	ApoderadoConverter apC = new ApoderadoConverter();
	
	/*LOG constante*/
	private static final Log LOG = LogFactory.getLog(AsistenciaJuntaAccionistaConverter.class);
	
	/*
	 * Modelo a entidad
	 * @param asistenciaJuntaAccionistaModel AsistenciaJuntaAccionistaModel
	 * @return asistenciaJuntaAccionista 
	 */
	public AsistenciaJuntaAccionista modelo2Entidad(AsistenciaJuntaAccionistaModel asistenciaJuntaAccionistaModel) {
		LOG.info("Convierte dividendo de modelo a entidad");
		
		AsistenciaJuntaAccionista asistenciaJuntaAccionista = new AsistenciaJuntaAccionista();
		asistenciaJuntaAccionista.setIdAsistencia(asistenciaJuntaAccionistaModel.getIdAsistenciaModel());
		asistenciaJuntaAccionista.setJuntaAccionista(jaC.modelo2Entidad(asistenciaJuntaAccionistaModel.getJuntaAccionistaModel()));
		asistenciaJuntaAccionista.setAccionista(aC.modelo2Entidad(asistenciaJuntaAccionistaModel.getAccionistaModel()));
		if(asistenciaJuntaAccionistaModel.getApoderadoModel() == null) {
			asistenciaJuntaAccionista.setApoderado(null);
		}else {
			asistenciaJuntaAccionista.setApoderado(apC.modelo2Entidad(asistenciaJuntaAccionistaModel.getApoderadoModel()));
		}
		
		asistenciaJuntaAccionista.setEstadoAsistenciaJunta(asistenciaJuntaAccionistaModel.getEstadoAsistenciaJuntaModel());
		return asistenciaJuntaAccionista;
	}
	
	/*
	 * Entidad a Modelo
	 * @param asistenciaJuntaAccionista AsistenciaJuntaAccionista
	 * @return asistenciaJuntaAccionistaModel
	 */
	public AsistenciaJuntaAccionistaModel entidad2Modelo(AsistenciaJuntaAccionista asistenciaJuntaAccionista) {
		LOG.info("Convierte dividendo de entidad a modelo");
		
		AsistenciaJuntaAccionistaModel asistenciaJuntaAccionistaModel = new AsistenciaJuntaAccionistaModel();
		asistenciaJuntaAccionistaModel.setIdAsistenciaModel(asistenciaJuntaAccionista.getIdAsistencia());
		asistenciaJuntaAccionistaModel.setJuntaAccionistaModel(jaC.entidad2Model(asistenciaJuntaAccionista.getJuntaAccionista()));
		asistenciaJuntaAccionistaModel.setAccionistaModel(aC.entidad2Model(asistenciaJuntaAccionista.getAccionista()));
		if(asistenciaJuntaAccionista.getApoderado() == null) {
			asistenciaJuntaAccionistaModel.setApoderadoModel(null);
		}else {
			asistenciaJuntaAccionistaModel.setApoderadoModel(apC.entidad2Model(asistenciaJuntaAccionista.getApoderado()));
		}
		
		asistenciaJuntaAccionistaModel.setEstadoAsistenciaJuntaModel(asistenciaJuntaAccionista.getEstadoAsistenciaJunta());
		return asistenciaJuntaAccionistaModel;
	}
}
