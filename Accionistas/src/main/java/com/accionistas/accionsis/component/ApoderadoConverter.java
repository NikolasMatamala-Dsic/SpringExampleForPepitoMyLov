package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.Apoderado;
import com.accionistas.accionsis.model.ApoderadoModel;

/**
 * Clase ApoderadoConverter.
 */
@Component("apoderadoConverter")
public class ApoderadoConverter {
	
	
	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(ApoderadoConverter.class);
	
	/**
	 * Modelo a entidad.
	 * @param apoderadoModel ApoderadoModel 
	 * @return apoderado
	 */
	public Apoderado modelo2Entidad(ApoderadoModel apoderadoModel) {
			LOG.info("Convirtiendo apoderado de modelo a entidad");
			if(apoderadoModel == null) {return null;}
			Apoderado apoderado= new Apoderado();
			apoderado.setIdApoderado(apoderadoModel.getIdApoderadoModel());
			apoderado.setRut(apoderadoModel.getRutModel());
			apoderado.setNombre(apoderadoModel.getNombreModel());
			apoderado.setApellido_paterno(apoderadoModel.getApellido_paternoModel());
			apoderado.setApellido_materno(apoderadoModel.getApellido_maternoModel());
			apoderado.setEmail(apoderadoModel.getEmailModel());
			apoderado.setFono(apoderadoModel.getFonoModel());
			apoderado.setEstado(apoderadoModel.getEstadoModel());
			apoderado.setTipoApoderado(apoderadoModel.getTipoApoderadoModel());
			return apoderado;
		}
	
	/**
	 * Entidad a model.
	 * @param apoderado Apoderado 
	 * @return apoderadoModel
	 */
	public ApoderadoModel entidad2Model(Apoderado apoderado) {
		LOG.info("Convirtiendo apoderado de entidad a modelo");
		if(apoderado == null) { return null;}
		ApoderadoModel apoderadoModel = new ApoderadoModel();
		apoderadoModel.setIdApoderadoModel(apoderado.getIdApoderado());
		apoderadoModel.setRutModel(apoderado.getRut());
		apoderadoModel.setNombreModel(apoderado.getNombre());
		apoderadoModel.setApellido_paternoModel(apoderado.getApellido_paterno());
		apoderadoModel.setApellido_maternoModel(apoderado.getApellido_materno());
		apoderadoModel.setEmailModel(apoderado.getEmail());
		apoderadoModel.setFonoModel(apoderado.getFono());
		apoderadoModel.setEstadoModel(apoderado.getEstado());
		apoderadoModel.setTipoApoderadoModel(apoderado.getTipoApoderado());
		return apoderadoModel;
	}

}
