package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.Heredero;
import com.accionistas.accionsis.model.HerederoModel;


@Component("herederoConverter")
public class HerederoConverter {
	
	EmpresaConverter eC = new EmpresaConverter();
	NacionalidadConverter nC = new NacionalidadConverter();
	TipoAccionistaConverter taC = new TipoAccionistaConverter();
	AccionistaConverter aC = new AccionistaConverter();

	private static final Log LOG = LogFactory.getLog(HerederoConverter.class);

	public Heredero modelo2Entidad(HerederoModel herederoModel) {
		LOG.info("Convirtiendo heredero de modelo a entidad");
		
		Heredero heredero = new Heredero();
		heredero.setIdHeredero(herederoModel.getIdHerederoModel());
		heredero.setRut(herederoModel.getRutModel());
		heredero.setEmpresa(eC.modelo2Entidad(herederoModel.getEmpresaModel()));
		heredero.setNombre(herederoModel.getNombreModel());
		heredero.setApellido_paterno(herederoModel.getApellido_paternoModel());
		heredero.setApellido_materno(herederoModel.getApellido_maternoModel());
		heredero.setCantidad_acciones_serie_a(herederoModel.getCantidad_acciones_serie_aModel());
		heredero.setCantidad_acciones_serie_b(herederoModel.getCantidad_acciones_serie_bModel());
		heredero.setPorcentaje_participacion(herederoModel.getPorcentaje_participacionModel());
		heredero.setFecha_nacimiento(herederoModel.getFecha_nacimientoModel());
		heredero.setEs_valido(herederoModel.getEs_validoModel());
		heredero.setEmail(herederoModel.getEmailModel());
		heredero.setDireccion(herederoModel.getDireccionModel());
		heredero.setFono(herederoModel.getFonoModel());
		heredero.setCiudad(herederoModel.getCiudadModel());
		heredero.setComuna(herederoModel.getComunaModel());
		heredero.setNacionalidad(nC.modelo2Entidad(herederoModel.getNacionalidadModel()));
		heredero.setTipoAccionista(taC.modelo2Entidad(herederoModel.getTipoAccionistaModel()));
		heredero.setAccionista(aC.modelo2Entidad(herederoModel.getAccionistaModel()));
		heredero.setEstado(herederoModel.getEstadoModel());
		
		return heredero;
	}
	
	public HerederoModel entidad2Modelo(Heredero heredero) {
		LOG.info("Convirtiendo heredero de entidad a modelo");
		
		HerederoModel herederoModel = new HerederoModel();
		herederoModel.setIdHerederoModel(heredero.getIdHeredero());
		herederoModel.setRutModel(heredero.getRut());
		herederoModel.setEmpresaModel(eC.entidad2Model(heredero.getEmpresa()));
		herederoModel.setNombreModel(heredero.getNombre());
		herederoModel.setApellido_paternoModel(heredero.getApellido_paterno());
		herederoModel.setApellido_maternoModel(heredero.getApellido_materno());
		herederoModel.setCantidad_acciones_serie_aModel(heredero.getCantidad_acciones_serie_a());
		herederoModel.setCantidad_acciones_serie_bModel(heredero.getCantidad_acciones_serie_b());
		herederoModel.setPorcentaje_participacionModel(heredero.getPorcentaje_participacion());
		herederoModel.setFecha_nacimientoModel(heredero.getFecha_nacimiento());
		herederoModel.setEs_validoModel(heredero.getEs_valido());
		herederoModel.setEmailModel(heredero.getEmail());
		herederoModel.setDireccionModel(heredero.getDireccion());
		herederoModel.setFonoModel(heredero.getFono());
		herederoModel.setCiudadModel(heredero.getCiudad());
		herederoModel.setComunaModel(heredero.getComuna());
		herederoModel.setNacionalidadModel(nC.entidad2Model(heredero.getNacionalidad()));
		herederoModel.setTipoAccionistaModel(taC.entidad2Model(heredero.getTipoAccionista()));
		herederoModel.setAccionistaModel(aC.entidad2Model(heredero.getAccionista()));
		herederoModel.setEstadoModel(heredero.getEstado());
		
		
		return herederoModel;
	}

}
