package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.JuntaAccionista;
import com.accionistas.accionsis.model.JuntaAccionistaModel;

@Component("juntaAccionistaConverter")
public class JuntaAccionistaConverter {

	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(JuntaAccionistaConverter.class);
	
	/**
	 * Modelo a entidad.
	 *
	 * @param juntaaccionistaModel JuntaAccionistaModel 
	 * @return juntaaccionista
	 */
	public JuntaAccionista modelo2Entidad(JuntaAccionistaModel juntaaccionistaModel) {
			LOG.info("Convirtiendo junta de accionistas de modelo a entidad");
			JuntaAccionista juntaaccionista= new JuntaAccionista();
			juntaaccionista.setIdJuntaAccionista(juntaaccionistaModel.getIdJuntaAccionistaModel());
			juntaaccionista.setUbicacion(juntaaccionistaModel.getUbicacionModel());
			juntaaccionista.setFecha_realizacion(juntaaccionistaModel.getFecha_realizacionModel());
			juntaaccionista.setHora_inicio(juntaaccionistaModel.getHora_inicioModel());
			juntaaccionista.setHora_termino(juntaaccionistaModel.getHora_terminoModel());
			juntaaccionista.setDireccion(juntaaccionistaModel.getDireccionModel());
			juntaaccionista.setFecha_difusion(juntaaccionistaModel.getFecha_difusionModel());
			juntaaccionista.setAsistenciaTotal(juntaaccionistaModel.getAsistenciaTotalModel());
			juntaaccionista.setEstado(juntaaccionistaModel.getEstadoModel());
			return juntaaccionista;
		}
	
	/**
	 * Entidad a model.
	 *
	 * @param juntaaccionista JuntaAccionista 
	 * @return mandatarioModel
	 */
	public JuntaAccionistaModel entidad2Model(JuntaAccionista juntaaccionista) {
		LOG.info("Convirtiendo junta de accionistas de entidad a modelo");
		JuntaAccionistaModel juntaaccionistaModel = new JuntaAccionistaModel();
		juntaaccionistaModel.setIdJuntaAccionistaModel(juntaaccionista.getIdJuntaAccionista());
		juntaaccionistaModel.setUbicacionModel(juntaaccionista.getUbicacion());
		juntaaccionistaModel.setFecha_realizacionModel(juntaaccionista.getFecha_realizacion());
		juntaaccionistaModel.setHora_inicioModel(juntaaccionista.getHora_inicio());
		juntaaccionistaModel.setHora_terminoModel(juntaaccionista.getHora_termino());
		juntaaccionistaModel.setDireccionModel(juntaaccionista.getDireccion());
		juntaaccionistaModel.setFecha_difusionModel(juntaaccionista.getFecha_difusion());
		juntaaccionistaModel.setAsistenciaTotalModel(juntaaccionista.getAsistenciaTotal());
		juntaaccionistaModel.setEstadoModel(juntaaccionista.getEstado());
		return juntaaccionistaModel;
	}	
}
