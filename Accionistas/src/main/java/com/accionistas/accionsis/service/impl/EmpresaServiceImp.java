package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.EmpresaConverter;
import com.accionistas.accionsis.entity.Empresa;
import com.accionistas.accionsis.entity.Representante;
import com.accionistas.accionsis.model.EmpresaModel;
import com.accionistas.accionsis.repository.EmpresaJpaRepository;
import com.accionistas.accionsis.service.EmpresaService;

/**
 * Clase EmpresaServiceImp.
 */
@Service("empresaServiceImp")
public class EmpresaServiceImp implements EmpresaService {

	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(EmpresaServiceImp.class);
	
	/** The empresa jpa repository. */
	@Autowired
	@Qualifier("empresaJpaRepository")
	private EmpresaJpaRepository empresaJpaRepository;
	
	/** The empresa converter. */
	@Autowired
	@Qualifier("empresaConverter")
	private EmpresaConverter empresaConverter;
	
	/**
	 * Metodo para agregar empresas
	 * @param empresaModel
	 */
	@Override
	public EmpresaModel addEmpresa(EmpresaModel empresaModel) {
		LOG.info("Call: " + "addEmpresa()" +" Desde EmpresaService");
		Empresa empresa = empresaJpaRepository.save(empresaConverter.modelo2Entidad(empresaModel));
		return empresaConverter.entidad2Model(empresa);	
	}

	/**
	 * Metodo para listar empresas
	 *
	 */
	@Override
	public List<EmpresaModel> listAllEmpresa() {
		LOG.info("Call: " + "listAllEmpresa() -- desde service");
		List<Empresa> empresas = empresaJpaRepository.findAll();
		List<EmpresaModel> empresasModel = new ArrayList<EmpresaModel>();
		for (Empresa empresa : empresas) {
			empresasModel.add(empresaConverter.entidad2Model(empresa));
		}		 
		return empresasModel;
	}


	@Override
	public EmpresaModel findEmpresaByid(Empresa empresa) {
		return empresaConverter.entidad2Model(empresaJpaRepository.findEmpresaByidEmpresa(empresa.getIdEmpresa()));
	}

	@Override
	public void removeEmpresa(Empresa empresa) {
		empresa = empresaJpaRepository.findEmpresaByidEmpresa(empresa.getIdEmpresa());
		if (null != empresa) {
			empresaJpaRepository.delete(empresa);
		}
		
	}

	@Override
	public EmpresaModel findEmpresaByid(int id) {
		return empresaConverter.entidad2Model(empresaJpaRepository.findEmpresaByidEmpresa(id));
	}

	@Override
	public List<EmpresaModel> findEmpresaByRepresentante(Representante representante) {
		LOG.info("Call: " + "-- Lista Empresa por Representantes");
		List<Empresa> em = new ArrayList<Empresa>();
		em = empresaJpaRepository.findEmpresaByRepresentante(representante);
		List<EmpresaModel> empresaModelList = new ArrayList<>();
		for(Empresa empresa : em) {
			empresaModelList.add(empresaConverter.entidad2Model(empresa));
		}
		
		
		return empresaModelList;
		
	}

	@Override
	public Empresa findEmpresaActiva() {
		
		return empresaJpaRepository.findTop();
	}

	
}
