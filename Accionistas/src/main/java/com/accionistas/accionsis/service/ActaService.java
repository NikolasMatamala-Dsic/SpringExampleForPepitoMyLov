package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Acta;
import com.accionistas.accionsis.model.ActaModel;

public interface ActaService {
	
	public abstract ActaModel addActa(ActaModel actaModel);
	
	public abstract String updateEstadoActa(ActaModel actaModel);
	
	public abstract ActaModel updateActaModel(ActaModel actaModel);
	
	public abstract Acta findActaByidActaModel (int id);
	
	public abstract List<Acta> listAllActa ();

}
