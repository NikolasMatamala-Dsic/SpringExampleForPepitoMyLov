package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Regiones;
import com.accionistas.accionsis.model.RegionesModel;

public interface RegionesService {
	
	public abstract List<Regiones> listAllRegiones();
	
	public abstract RegionesModel findByidRegionModel(int id);

}
