package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.entity.CuentaCorriente;

public interface CuentaCorrienteService {

	public abstract CuentaCorriente listAllCuentaCorriente(Accionista Accionista);
	public abstract List<CuentaCorriente> listAllCuentaCorrienteEntity();
}
