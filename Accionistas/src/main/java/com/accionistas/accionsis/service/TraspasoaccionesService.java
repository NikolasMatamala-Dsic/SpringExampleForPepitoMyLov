package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Traspasoacciones;
import com.accionistas.accionsis.model.TraspasoaccionesModel;

public interface TraspasoaccionesService {
	
	public abstract List<TraspasoaccionesModel> listAllTraspasosacciones();
	
	public abstract TraspasoaccionesModel addTraspasoacciones(TraspasoaccionesModel traspasoaccionesModel);
	
	public abstract TraspasoaccionesModel findTraspasoaccionesModelById(int id);
	
	public abstract Traspasoacciones updateTraspaso(Traspasoacciones traspasoacciones);
	
	public abstract Traspasoacciones findTraspasoaccionesByTraspasoacciones(Traspasoacciones traspasoacciones);

}
