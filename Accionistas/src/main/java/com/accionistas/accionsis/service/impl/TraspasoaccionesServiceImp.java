package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.TraspasoaccionesConverter;
import com.accionistas.accionsis.entity.Traspasoacciones;
import com.accionistas.accionsis.model.TraspasoaccionesModel;
import com.accionistas.accionsis.repository.TraspasoaccionesJpaRepository;
import com.accionistas.accionsis.service.TraspasoaccionesService;

@Service("traspasoaccionesServiceImp")
public class TraspasoaccionesServiceImp implements TraspasoaccionesService{

	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(TraspasoaccionesServiceImp.class);
	
	/** The traspasoacciones jpa repository. */
	@Autowired
	@Qualifier("traspasoaccionesJpaRepository")
	private TraspasoaccionesJpaRepository traspasoaccionesJpaRepository;
	
	/** The traspasoacciones converter. */
	@Autowired
	@Qualifier("traspasoaccionesConverter")
	private TraspasoaccionesConverter traspasoaccionesConverter;
	
	@Override
	public List<TraspasoaccionesModel> listAllTraspasosacciones() {
		LOG.info("Call: " + "listAllTraspasoacciones() -- desde TraspasoaccionesService");
		List<Traspasoacciones> traspasosacciones = traspasoaccionesJpaRepository.findAll();
		List<TraspasoaccionesModel> traspasoaccionesModel = new ArrayList<TraspasoaccionesModel>();
		for (Traspasoacciones traspasoacciones: traspasosacciones) {
			traspasoaccionesModel.add(traspasoaccionesConverter.entidad2Model(traspasoacciones));
		}		 
		return traspasoaccionesModel;
	}

	@Override
	public TraspasoaccionesModel addTraspasoacciones(TraspasoaccionesModel traspasoaccionesModel) {
		LOG.info("Call: " + "addTraspasoacciones()" +" Desde TraspasoaccionesService");
		Traspasoacciones traspasoacciones = traspasoaccionesJpaRepository.save(traspasoaccionesConverter.modelo2Entidad(traspasoaccionesModel));
		return traspasoaccionesConverter.entidad2Model(traspasoacciones);	
	}

	@Override
	public TraspasoaccionesModel findTraspasoaccionesModelById(int id) {
		// TODO Auto-generated method stub
		return traspasoaccionesConverter.entidad2Model(traspasoaccionesJpaRepository.findTraspasoaccionesByIdidTraspasoAcciones(id));
	}

	@Override
	public Traspasoacciones updateTraspaso(Traspasoacciones traspasoacciones) {
		LOG.info("Call: " + "updateTraspaso() -- desde Traspasoacciones ServiceImp");
		return traspasoaccionesJpaRepository.save(traspasoacciones);
	}

	@Override
	public Traspasoacciones findTraspasoaccionesByTraspasoacciones(Traspasoacciones traspasoacciones) {
		// TODO Auto-generated method stub
		return traspasoaccionesJpaRepository.findTraspasoaccionesByIdidTraspasoAcciones(traspasoacciones.getIdidTraspasoAcciones());
	}
	
	

}
