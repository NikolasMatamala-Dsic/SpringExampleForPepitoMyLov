package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.TipoAccionista;
import com.accionistas.accionsis.model.TipoAccionistaModel;

public interface TipoAccionistaService {
	
public abstract List<TipoAccionistaModel> listAllTiposAccionistas();
	
	/**
	 *  encontrar TiposAccionistas por id
	 * @param id 
	 * @return tipoAccionista
	 */
	public abstract TipoAccionista findTipoAccionistaById(int id);
	
	
	/**
	 * Encontrar TipoAcionistaModel a travez de  id model.
	 * @param id 
	 * @return TipoAccionistaModel model
	 */
	public abstract TipoAccionistaModel findTipoAccionistaModelByIdModel(int id);
	
	public abstract List<TipoAccionista> listAllTipoAccionista();
}
