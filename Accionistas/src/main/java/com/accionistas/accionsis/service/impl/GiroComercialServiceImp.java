package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.GiroComercialConverter;
import com.accionistas.accionsis.entity.GiroComercial;
import com.accionistas.accionsis.model.GiroComercialModel;
import com.accionistas.accionsis.repository.GiroComercialJpaRepository;
import com.accionistas.accionsis.service.GiroComercialService;

@Service("giroComercialServiceImp")
public class GiroComercialServiceImp implements GiroComercialService{

	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(GiroComercialServiceImp.class);
	
	/** The giroComercial jpa repository. */
	@Autowired
	@Qualifier("giroComercialJpaRepository")
	private GiroComercialJpaRepository giroComercialJpaRepository;
	
	/** The giroComercial converter. */
	@Autowired
	@Qualifier("giroComercialConverter")
	private GiroComercialConverter giroComercialConverter;
	
	
	@Override
	public List<GiroComercialModel> listAllGirosComerciales() {
		LOG.info("Call: " + "listAllGirosComerciales() -- desde service");
		List<GiroComercial> girosComerciales = giroComercialJpaRepository.findAll();
		List<GiroComercialModel> girosComercialesModel = new ArrayList<GiroComercialModel>();
		for (GiroComercial giroComercial: girosComerciales) {
			girosComercialesModel.add(giroComercialConverter.entidad2Model(giroComercial));
		}		 
		return girosComercialesModel;
		
	}


	@Override
	public GiroComercial findGiroComercialById(int id) {
		return giroComercialJpaRepository.findByidGiroComercial(id);
	}

	@Override
	public GiroComercialModel findGiroComercialModelByModel(GiroComercialModel model) {
		
		return giroComercialConverter.entidad2Model(findGiroComercialById(model.getIdGiroComercialModel()));
		
	}

}
