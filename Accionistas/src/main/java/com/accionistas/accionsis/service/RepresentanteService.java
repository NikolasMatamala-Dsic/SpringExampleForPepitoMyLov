package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Representante;
import com.accionistas.accionsis.model.RepresentanteModel;

public interface RepresentanteService {
	
	/*
	 * metodo agregar representante
	 */
	public abstract RepresentanteModel addRepresentante(RepresentanteModel representanteModel);
	/*
	 *metodo listar para llenar 
	 */
	public abstract List<RepresentanteModel> listAllRepresentante();
	/*
	 * encontrar representante atravez de rut
	 */
	public abstract Representante findRepresentanteByRut(String rut);
	/*
	 * encontrar representante atravez de id
	 */
	public abstract Representante findRepresentanteByID(int id);
	
	public abstract Representante findByidRepresentante(int id);
	/*
	 * encontrar representante por id Model
	 */
	public abstract RepresentanteModel findRepresentanteByRutModel(String rut);
	/*
	 * Elimina representante por rut
	 */
	public abstract void removeRepresentante(String rut);
	/*
	 * Listar representante activos
	 */
	public abstract List<RepresentanteModel> listAllRepresentantes_active();
	
	public abstract Representante updateRepresentante(Representante representante);

}
