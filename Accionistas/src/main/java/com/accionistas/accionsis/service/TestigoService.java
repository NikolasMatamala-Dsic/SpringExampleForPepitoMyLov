package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Testigo;
import com.accionistas.accionsis.model.TestigoModel;

/**
 * Interface TestigoService.
 */
public interface TestigoService {
	
	/**
	 * Agregar testigo.
	 * @param testigoModel
	 * @return testigo model
	 */
	public abstract TestigoModel addTestigo(TestigoModel testigoModel);
	
	/**
	 * Listar testigos.
	 * @return Lista de testigos
	 */
	public abstract List<TestigoModel> listAllTestigo();
	
	public abstract List<TestigoModel> listAllTestigo_active();

	
	/**
	 * Buscar testigo por rut.
	 * @param rut 
	 * @return testigo
	 */
	public abstract Testigo findTestigoByRut(String rut);
	
	public abstract TestigoModel findTestigoByidTestigo(int id);
	
	/**
	 * Eliminar testigo.
	 * @param rut
	 */
	public abstract void removeTestigo(String rut);
	
	/**
	 * Buscar testigo a travez de rut model.
	 * @param rut 
	 * @return testigo model
	 */
	public abstract TestigoModel findTestigoByRutModel(String rut);
	
	public abstract Testigo updateTestigo(Testigo testigo);

}
