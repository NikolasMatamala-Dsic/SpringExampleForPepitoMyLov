package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.AccionistaConverter;
import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.model.AccionistaModel;
import com.accionistas.accionsis.repository.AccionistaJpaRepository;
import com.accionistas.accionsis.service.AccionistaService;

/**
 * Clase AccionistaServiceImp.
 */
@Service("accionistaServiceImp")
public class AccionistaServiceImp implements AccionistaService{
	
	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(AccionistaServiceImp.class);
	
	/** The accionista jpa repository. */
	@Autowired
	@Qualifier("accionistaJpaRepository")
	private AccionistaJpaRepository accionistaJpaRepository;
	
	/** The accionista converter. */
	@Autowired
	@Qualifier("accionistaConverter")
	private AccionistaConverter accionistaConverter;

	/** 
	 * Metodo para agregar accionistas
	 * @param accionistaModel
	 */
	@Override
	public Accionista addAccionista(Accionista accionista) {
		LOG.info("Call: " + "addAccionista()" +" Desde AccionistaService");
		Accionista accionistax = accionistaJpaRepository.save(accionista);
		return accionistax;	
	}

	/** 
	 * Metodo para listar todos los accionistas
	 * 
	 */
	@Override
	public List<AccionistaModel> listAllAccionista() {
		LOG.info("Call: " + "listAllAccionista() -- desde AccionistaService");
		List<Accionista> accionistas = accionistaJpaRepository.findAll();
		List<AccionistaModel> accionistaModel = new ArrayList<AccionistaModel>();
		for (Accionista accionista : accionistas) {
			accionistaModel.add(accionistaConverter.entidad2Model(accionista));
		}		 
		return accionistaModel;
	}
	
	@Override
	public List<AccionistaModel> listAllAccionista_active() {
		LOG.info("Call: " + "listAllAccionista() -- desde AccionistaService");
		List<Accionista> accionistas = accionistaJpaRepository.findTodosAB();
		List<AccionistaModel> accionistaModel = new ArrayList<AccionistaModel>();
		for (Accionista accionista : accionistas) {
			if(accionista.getEstado() == 1) {
				accionistaModel.add(accionistaConverter.entidad2Model(accionista));
			}
		}		 
		return accionistaModel;
	}

	/** 
	 * Metodo para encontrar un accionista a travez de rut
	 * @param rut
	 */
	@Override
	public Accionista findAccionistaByAccionista(Accionista accionista) {
		return accionistaJpaRepository.findByRut(accionista.getRut());
	}

	/** 
	 * Metodo para eliminar accionista
	 * @param rut
	 */
	@Override
	public void removeAccionista(Accionista accionista) {
		LOG.info("Call: " + "removeAccionista() -- desde AccionistaService");
		Accionista ac = findAccionistaByAccionista(accionista);
		if (null != ac) {
			accionistaJpaRepository.delete(ac);
		}	
	}

	/**
	 * Metodo para encontrar accionista a travez de rut de accionistaModel
	 * @param rut
	 */
	@Override
	public Accionista findAccionistaByAccionistaModel(Accionista accionista) {
	
		return accionistaJpaRepository.findByRut(accionista.getRut());
	}

	@Override
	public Accionista updateAccionista(Accionista accionista) {
		LOG.info("Call: " + "updateAccionista() -- desde Accionista ServiceImp");
		
		return accionistaJpaRepository.save(accionista);
		
	}

	@Override
	public Accionista findAccionistaByRut(Accionista accionista) {
		// TODO Auto-generated method stub
		return accionistaJpaRepository.findByRut(accionista.getRut());
	}

	@Override
	public AccionistaModel findAccionistaModelById(int id) {
		
		return accionistaConverter.entidad2Model(accionistaJpaRepository.findByidAccionista(id));
	}

	@Override
	public Accionista findAccionistaByRut(String rut) {
		// TODO Auto-generated method stub
		return accionistaJpaRepository.findByRut(rut);
	}

	@Override
	public AccionistaModel findAccionistaModelByRut(String rut) {
		// TODO Auto-generated method stub
		return accionistaConverter.entidad2Model(accionistaJpaRepository.findByRut(rut));
	}

	@Override
	public List<AccionistaModel> listAccionistasForGraficoTop10AccionTypeA() {
		List<Accionista> listEntity = accionistaJpaRepository.findTodos();
		List<AccionistaModel> listModel = new ArrayList<>();
		for(Accionista acc : listEntity) {
			if(acc.getEstado() == 1) {
				listModel.add(accionistaConverter.entidad2Model(acc));
			}
		}
		return listModel;
	}
	
	@Override
	public List<AccionistaModel> listAccionistasForGraficoTop10AccionTypeB() {
		List<Accionista> listEntity = accionistaJpaRepository.findTodosB();
		List<AccionistaModel> listModel = new ArrayList<>();
		for(Accionista acc : listEntity) {
			if(acc.getEstado() == 1) {
				listModel.add(accionistaConverter.entidad2Model(acc));
			}
		}
		return listModel;
	}

	@Override
	public List<Accionista> listAllAccionistaEntity() {
		List<Accionista> listEntity = accionistaJpaRepository.findAll();
		return listEntity;
	}

	@Override
	public List<Accionista> listAccionistasForTop10AccionTypeA() {
		List<Accionista> listentityA = accionistaJpaRepository.findTodos();
		List<Accionista> listFinal = new ArrayList<>(); 
		for(Accionista acc : listentityA) {
			if(acc.getEstado() == 1) {
				listFinal.add(acc);
			}
		}
		return listFinal;
	}

	@Override
	public List<Accionista> listAccionistasForTop10AccionTypeB() {
		List<Accionista> listEntityB = accionistaJpaRepository.findTodosB();
		List<Accionista> listFinal = new ArrayList<>(); 
		for(Accionista acc : listEntityB) {
			if(acc.getEstado() == 1) {
				listFinal.add(acc);
			}
		}
		return listFinal;
	}

	@Override
	public List<Accionista> listAllAccionistaEntity_active() {
		List<Accionista> listEntity = accionistaJpaRepository.findAll();
		List<Accionista> listFinal = new ArrayList<>(); 
		for(Accionista acc : listEntity) {
			if(acc.getEstado() == 1) {
				listFinal.add(acc);
			}
		}
		return listFinal;
	}
	
}
