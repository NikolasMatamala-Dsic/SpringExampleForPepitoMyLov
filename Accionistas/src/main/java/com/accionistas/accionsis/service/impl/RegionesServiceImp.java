package com.accionistas.accionsis.service.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.RegionConverter;
import com.accionistas.accionsis.entity.Regiones;
import com.accionistas.accionsis.model.RegionesModel;
import com.accionistas.accionsis.repository.RegionesJpaRepository;
import com.accionistas.accionsis.service.RegionesService;

@Service("regionesServiceImp")
public class RegionesServiceImp implements RegionesService{
	
	@Autowired
	@Qualifier("regionesJpaRepository")
	private RegionesJpaRepository regionesJpaRepository;
	
	@Autowired
	@Qualifier("regionConverter")
	private RegionConverter regionConverter;

	private static final Log LOG = LogFactory.getLog(ApoderadoServiceImp.class);
	
	
	@Override
	public List<Regiones> listAllRegiones() {
		LOG.info("Call: " + "listAllRegionesEntity()");
		List<Regiones> region = regionesJpaRepository.findAll();
		return region;
	}


	@Override
	public RegionesModel findByidRegionModel(int id) {
		// TODO Auto-generated method stub
		return regionConverter.entidad2Modelo(regionesJpaRepository.findByidRegion(id));
	}
	
	

}
