package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.ActaConverter;
import com.accionistas.accionsis.entity.Acta;
import com.accionistas.accionsis.model.ActaModel;
import com.accionistas.accionsis.repository.ActaJpaRepository;
import com.accionistas.accionsis.service.ActaService;

@Service("actaServiceImp")
public class ActaServiceImp implements ActaService {
	
	@Autowired
	@Qualifier("actaJpaRepository")
	private ActaJpaRepository actaJpaRepository;
	
	@Autowired
	@Qualifier("actaConverter")
	private ActaConverter actaConverter;
	
	private static final Log LOG = LogFactory.getLog(AccionistaServiceImp.class);

	@Override
	public ActaModel addActa(ActaModel actaModel) {
		LOG.info("Call: " + "addActa()" +" Desde ActaService");
		Acta acta = actaJpaRepository.save(actaConverter.modelo2Entidad(actaModel));
		return actaConverter.entidad2Modelo(acta);
	}

	@Override
	public String updateEstadoActa(ActaModel actaModel) {
		String respuesta = "";
		
		if(null !=  actaJpaRepository.save(actaConverter.modelo2Entidad(actaModel))) {
			respuesta = "Acta n° "+actaModel.getIdActa() + " actualizada correctamente";
		}else {
			respuesta = "Un error ocurrio al actualizar la acta n° "+actaModel.getIdActa();
		}
		return respuesta;
	}

	@Override
	public ActaModel updateActaModel(ActaModel actaModel) {
		return actaConverter.entidad2Modelo(actaJpaRepository.save(actaConverter.modelo2Entidad(actaModel)));
	}

	@Override
	public Acta findActaByidActaModel(int id) {
		return actaJpaRepository.findByidActa(id);
	}

	@Override
	public List<Acta> listAllActa() {
		return actaJpaRepository.findAll();
	}

}
