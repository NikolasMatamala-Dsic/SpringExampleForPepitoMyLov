package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.AsistenciaJuntaAccionista;
import com.accionistas.accionsis.entity.JuntaAccionista;
import com.accionistas.accionsis.model.AsistenciaJuntaAccionistaModel;
import com.accionistas.accionsis.model.JuntaAccionistaModel;


/*
 *Interface AsistenciaJuntaAccionistaService 
 */
public interface AsistenciaJuntaAccionistaService {
	
	/*Guarda AsistenciaJuntaAccionista*/
	public abstract AsistenciaJuntaAccionistaModel addAsistenciaJuntaAccionista(AsistenciaJuntaAccionistaModel asistenciaJuntaAccionistaModel);
	
	/*
	 * Lista AsistenciaJuntaAccionista
	 * @return Lista de AsistenciaJuntaAccionista
	 */
	public abstract List<AsistenciaJuntaAccionistaModel> listAllAsistenciaJuntaAccionista();
	
	/*
	 * Busca AsistenciaJuntaAccionista por idJunta
	 * @param idJunta
	 * @return AsistenciaJuntaAccionista
	 */
	public abstract List<AsistenciaJuntaAccionistaModel> findAsistenciaJuntaAccionistaByJuntaAccionista(JuntaAccionistaModel juntaAccionista);
	public abstract List<AsistenciaJuntaAccionista> findAsistenciaJuntaAccionistaEntityByJuntaAccionista(JuntaAccionistaModel juntaAccionista);
	
	/*
	 * Elimina AsistenciaJuntaAccionista
	 * @param idJunta
	 * @return AsistenciaJuntaAccionistaModel
	 */
	public void removeAsistenciaJuntaAccionista(JuntaAccionista juntaAccionista);
	
	public abstract AsistenciaJuntaAccionistaModel updateAsistencia(AsistenciaJuntaAccionista asistenciaJuntaAccionista);
	
	public abstract AsistenciaJuntaAccionistaModel findAsistenciaJuntaAccionistaByIdAsistenciaJuntaAccionista(int id);
	
	
	
}
