package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.TipoActa;

public interface TipoActaService {
	
	public abstract TipoActa findTipoActaByidActa(int idTipoActa);
	
	public abstract List<TipoActa> listAllTipoActa();

}
