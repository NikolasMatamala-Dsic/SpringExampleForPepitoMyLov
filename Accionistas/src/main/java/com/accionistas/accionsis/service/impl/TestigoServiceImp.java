package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.TestigoConverter;
import com.accionistas.accionsis.entity.Testigo;
import com.accionistas.accionsis.model.TestigoModel;
import com.accionistas.accionsis.repository.TestigoJpaRepository;
import com.accionistas.accionsis.service.TestigoService;

/**
 * Clase TestigoServiceImp.
 */
@Service("testigoServiceImp")
public class TestigoServiceImp implements TestigoService{

	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(TestigoServiceImp.class);
	
	/** The testigo jpa repository. */
	@Autowired
	@Qualifier("testigoJpaRepository")
	private TestigoJpaRepository testigoJpaRepository;
	
	/** The testigo converter. */
	@Autowired
	@Qualifier("testigoConverter")
	private TestigoConverter testigoConverter;

	/**
	 * Metodo para agregar testigos
	 * @param testigoModel
	 */
	@Override
	public TestigoModel addTestigo(TestigoModel testigoModel) {
		LOG.info("Call: " + "addTestigo()" +" Desde TestigoService");
		Testigo testigo = testigoJpaRepository.save(testigoConverter.modelo2Entidad(testigoModel));
		return testigoConverter.entidad2Model(testigo);	
	}

	/**
	 * Lista todos los testigos
	 */
	@Override
	public List<TestigoModel> listAllTestigo() {
		LOG.info("Call: " + "listAllTestigo() -- desde TestigoService");
		List<Testigo> testigos = testigoJpaRepository.findAll();
		List<TestigoModel> testigoModel = new ArrayList<TestigoModel>();
		for (Testigo testigo: testigos) {
			testigoModel.add(testigoConverter.entidad2Model(testigo));
		}		 
		return testigoModel;
	}
	
	public List<TestigoModel> listAllTestigo_active() {
		LOG.info("Call: " + "listAllTestigoActive() -- desde TestigoService");
		List<Testigo> testigo = testigoJpaRepository.findAll();
		List<TestigoModel> testigoModel = new ArrayList<TestigoModel>();
		for (Testigo tes : testigo) {
			if(tes.getEstado() == 1) {
				testigoModel.add(testigoConverter.entidad2Model(tes));
			}
		}		 
		return testigoModel;
	}

	/**
	 * Metodo para encontrar testigo por rut
	 * @param rut
	 */
	@Override
	public Testigo findTestigoByRut(String rut) {
		return testigoJpaRepository.findByRut(rut);
	}

	/**
	 * Metodo para eliminar testigo
	 * @param rut
	 */
	@Override
	public void removeTestigo(String rut) {
		Testigo testigo = findTestigoByRut(rut);
		if (null != testigo) {
			testigoJpaRepository.delete(testigo);
		}			
	}

	/**
	 * Metodo para encontrar testigo a travez de rut de TestigoModel
	 */
	@Override
	public TestigoModel findTestigoByRutModel(String rut) {
		return testigoConverter.entidad2Model(findTestigoByRut(rut));
	}

	@Override
	public Testigo updateTestigo(Testigo testigo) {
		return testigoJpaRepository.save(testigo);
	}

	@Override
	public TestigoModel findTestigoByidTestigo(int id) {
		// TODO Auto-generated method stub
		return testigoConverter.entidad2Model(testigoJpaRepository.findByidTestigo(id));
	}

}
