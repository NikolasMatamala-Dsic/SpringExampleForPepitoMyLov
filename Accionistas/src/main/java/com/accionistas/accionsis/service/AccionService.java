package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Accion;
import com.accionistas.accionsis.model.AccionModel;

public interface AccionService {

	public abstract AccionModel addAccion(AccionModel accionModel);

	/**
	 * Listar accion.
	 * 
	 * @return Lista de accion
	 */
	public abstract List<AccionModel> listAllAccion();

	/**
	 * Buscar accion a travez de id_accion.
	 * 
	 * @param id_accion
	 * @return accion
	 */
	public abstract Accion findAccionById_accion(int id_accion);

	/**
	 * Eliminar accion.
	 * 
	 * @param id_accion
	 */
	public abstract void removeAccion(int id_accion);

	/**
	 * Buscar accion a travez de id_accion model.
	 * 
	 * @param id_accion
	 * @return accion model
	 */
	public abstract AccionModel findAccionById_accionModel(int id_accion);
	
}
