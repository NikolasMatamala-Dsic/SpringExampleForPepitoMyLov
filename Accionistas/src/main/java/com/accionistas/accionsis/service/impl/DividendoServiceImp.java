package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.DividendoConverter;
import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.entity.Dividendo;
import com.accionistas.accionsis.model.DividendoModel;
import com.accionistas.accionsis.repository.DividendoJpaRepository;
import com.accionistas.accionsis.service.DividendoService;

@Service("dividendoServiceImp")
public class DividendoServiceImp implements DividendoService{

	private static final Log LOG = LogFactory.getLog(DividendoServiceImp.class);
	
	@Autowired
	@Qualifier("DividendoJpaRepository")
	private DividendoJpaRepository dividendoJpaRepository;
	
	@Autowired
	@Qualifier("DividendoConverter")
	private DividendoConverter dividendoConverter;
	
	@Override
	public DividendoModel addDividendo(DividendoModel dividendoModel) {
		LOG.info("AgregarDividendo: " + "addDividendo() -- desde DividendoService");
		Dividendo dividendo = dividendoJpaRepository.save(dividendoConverter.modelo2Entidad(dividendoModel));
		return dividendoConverter.entidad2Model(dividendo);
	}

	@Override
	public List<DividendoModel> listAllDividendo() {
		LOG.info("MostrarDividendo: " + "listAllDivivendo() -- desde DividendoService");
		List<Dividendo> dividendos = dividendoJpaRepository.findAll();
		List<DividendoModel> dividendosModel = new ArrayList<DividendoModel>();
		for(Dividendo dividendo : dividendos) {
			dividendosModel.add(dividendoConverter.entidad2Model(dividendo));
		}
		return dividendosModel;
	}

	@Override
	public void removeDividendo(Accionista accionista) {
		LOG.info("RemoverDividendo" + "removeDividendo() -- desde DividendoService");
		Dividendo dividendo = findDividendoByAccionista(accionista);
		if(null != dividendo) {
			dividendoJpaRepository.delete(dividendo);
		}		
	}
	@Override
	public Dividendo findDividendoByAccionista(Accionista accionista) {
		Dividendo dividendo= findDividendoByAccionista(accionista);
		return dividendo;
	}

	@Override
	public List<Dividendo> listAllDividendoEntity() {
		List<Dividendo> dividendo = dividendoJpaRepository.findAll();
		return dividendo;
	}
	
	
	
}
