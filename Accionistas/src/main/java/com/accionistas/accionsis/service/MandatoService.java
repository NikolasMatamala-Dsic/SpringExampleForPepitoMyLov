package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Mandato;
import com.accionistas.accionsis.model.MandatoModel;

public interface MandatoService {
	/*
	 * metodo agregar mandatario
	 */
	public abstract MandatoModel addMandato(MandatoModel mandatoModel);
	/*
	 *metodo listar para llenar 
	 */
	public abstract List<MandatoModel> listAllMandato();
	/*
	 * encontrar mandatario atravez de id
	 */
	public abstract Mandato findMandatoById(int id);
	/*
	 * encontrar mandatario por id Model
	 */
	public abstract MandatoModel findMandatoByIdModel(int id);
	/*
	 * Elimina mandatario por id
	 */
	public abstract boolean removeMandato(int id);
	/*
	 * Listar mandatario activos
	 */
	public abstract List<MandatoModel> listAllMandato_active();
}
