package com.accionistas.accionsis.service;
import java.util.List;

import com.accionistas.accionsis.entity.JuntaAccionista;
import com.accionistas.accionsis.model.JuntaAccionistaModel;
/**
 * Interface JuntaAccionistaService.
 */
public interface JuntaAccionistaService {

		public abstract JuntaAccionistaModel updateJunta(JuntaAccionistaModel juntaAccionistaModel);
	
		public abstract String updateEstadoJunta(JuntaAccionistaModel juntaAccionistaModel);
	
	
		/**
		 * agregar junta de accionista.
		 * @param juntaaccionistaModel 
		 * @return junta accionista model
		 */
		
	
		public abstract JuntaAccionistaModel addJuntaAcconista(JuntaAccionistaModel juntaaccionistaModel);
		
		/**
		 * Listar juntas de accionistas.
		 * @return Lista de juntas de accionistas
		 */
		public abstract List<JuntaAccionistaModel> listAllJuntasAccionistas();

		
		/**
		 * Buscar junta de accionista a travez de su id
		 * @param id 
		 * @return Junta de accionista
		 */
		public abstract JuntaAccionista findJuntaAccionistaById(int id);
		
		/**
		 * Buscar junta de accionistas a travez de id model.
		 * @param id 
		 * @return junta de accionista model
		 */
		public abstract JuntaAccionistaModel findJuntaAccionistaByIdModel(int id);
}
