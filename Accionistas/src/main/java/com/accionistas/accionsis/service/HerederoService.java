package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Heredero;
import com.accionistas.accionsis.model.HerederoModel;

public interface HerederoService {
	/*
	 * Agregar Heredero
	 */
	public abstract HerederoModel addHeredero(HerederoModel herederoModel);
	/*
	 * Creando metodo listar para llenar
	 */
	public abstract List<HerederoModel> listAllHeredero();
	/*
	 * encontrar heredero atravez del rut
	 */
	public abstract Heredero findHerederoByRut(String rut);
	
	public abstract Heredero findHerederoByidHeredero(int id);
	
	/*
	 * encontrar en herederoModel por rut
	 */
	public abstract HerederoModel findHerederoModelByRutModel(String rut);
	/*
	 * Elimina heredero por rut
	 */
	public abstract void removeHeredero(String rut);
	/*
	 * Lista herederos activos
	 */
	public abstract List<HerederoModel> listAllHerederos_active();
	
	public abstract Heredero updateHeredero(Heredero heredero);
}
