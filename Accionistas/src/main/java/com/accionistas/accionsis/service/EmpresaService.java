package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Empresa;
import com.accionistas.accionsis.entity.Representante;
import com.accionistas.accionsis.model.EmpresaModel;

/**
 *Interface EmpresaService.
 */
public interface EmpresaService {

	Empresa findEmpresaActiva();
	
	/**
	 * Agregar empresa.
	 * @param empresaModel the empresa model
	 * @return empresa model
	 */	
	
	public abstract EmpresaModel addEmpresa(EmpresaModel empresaModel);
	
	/**
	 * Listar todas las empresas.
	 * @return Lista de empresas
	 */
	public abstract List<EmpresaModel> listAllEmpresa();

	
	/**
	 * Encontrar empresa por id.
	 * @param id 
	 * @return empresa
	 */
	public abstract EmpresaModel findEmpresaByid(Empresa empresa);
	public abstract EmpresaModel findEmpresaByid(int id);
	
	
	/**
	 * Eliminar empresa.
	 * @param id 
	 */
	public abstract void removeEmpresa(Empresa empresa);
	

	public abstract List<EmpresaModel> findEmpresaByRepresentante(Representante representante);

}
