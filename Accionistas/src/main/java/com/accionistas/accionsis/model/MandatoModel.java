package com.accionistas.accionsis.model;

public class MandatoModel {
	
	private int idMandatoModel;	
	private String descripcionMandato;
	
	public MandatoModel() {
		super();
	}

	public MandatoModel(int idMandatoModel, String descripcionMandato) {
		super();
		this.idMandatoModel = idMandatoModel;
		this.descripcionMandato = descripcionMandato;
	}

	@Override
	public String toString() {
		return "MandatoModel [idMandatoModel=" + idMandatoModel + ", descripcionMandato=" + descripcionMandato + "]";
	}

	public int getIdMandatoModel() {
		return idMandatoModel;
	}

	public void setIdMandatoModel(int idMandatoModel) {
		this.idMandatoModel = idMandatoModel;
	}

	public String getDescripcionMandato() {
		return descripcionMandato;
	}

	public void setDescripcionMandato(String descripcionMandato) {
		this.descripcionMandato = descripcionMandato;
	}
	
	
	
	
}
