package com.accionistas.accionsis.model;

public class AccionesConProhibicionModel {
	
	private int idAccionesProhibicionModel;

	private AccionistaModel accionistaModel;
	
	private int cantidadAccionesConProhibicionTipoAModel;
	
	private int cantidadAccionesConProhibicionTipoBModel;

	public AccionesConProhibicionModel() {
		super();
	}

	public AccionesConProhibicionModel(int idAccionesProhibicionModel, AccionistaModel accionistaModel,
			int cantidadAccionesConProhibicionTipoAModel, int cantidadAccionesConProhibicionTipoBModel) {
		super();
		this.idAccionesProhibicionModel = idAccionesProhibicionModel;
		this.accionistaModel = accionistaModel;
		this.cantidadAccionesConProhibicionTipoAModel = cantidadAccionesConProhibicionTipoAModel;
		this.cantidadAccionesConProhibicionTipoBModel = cantidadAccionesConProhibicionTipoBModel;
	}

	@Override
	public String toString() {
		return "AccionesConProhibicionModel [idAccionesProhibicionModel=" + idAccionesProhibicionModel
				+ ", accionistaModel=" + accionistaModel + ", cantidadAccionesConProhibicionTipoAModel="
				+ cantidadAccionesConProhibicionTipoAModel + ", cantidadAccionesConProhibicionTipoBModel="
				+ cantidadAccionesConProhibicionTipoBModel + "]";
	}

	public int getIdAccionesProhibicionModel() {
		return idAccionesProhibicionModel;
	}

	public void setIdAccionesProhibicionModel(int idAccionesProhibicionModel) {
		this.idAccionesProhibicionModel = idAccionesProhibicionModel;
	}

	public AccionistaModel getAccionistaModel() {
		return accionistaModel;
	}

	public void setAccionistaModel(AccionistaModel accionistaModel) {
		this.accionistaModel = accionistaModel;
	}

	public int getCantidadAccionesConProhibicionTipoAModel() {
		return cantidadAccionesConProhibicionTipoAModel;
	}

	public void setCantidadAccionesConProhibicionTipoAModel(int cantidadAccionesConProhibicionTipoAModel) {
		this.cantidadAccionesConProhibicionTipoAModel = cantidadAccionesConProhibicionTipoAModel;
	}

	public int getCantidadAccionesConProhibicionTipoBModel() {
		return cantidadAccionesConProhibicionTipoBModel;
	}

	public void setCantidadAccionesConProhibicionTipoBModel(int cantidadAccionesConProhibicionTipoBModel) {
		this.cantidadAccionesConProhibicionTipoBModel = cantidadAccionesConProhibicionTipoBModel;
	}

	
	
}
