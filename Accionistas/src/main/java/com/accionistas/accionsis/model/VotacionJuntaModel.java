package com.accionistas.accionsis.model;

public class VotacionJuntaModel {

	private int idVotacionModel;
	
	private JuntaAccionistaModel juntaAccionistaModel;
	
	private int cantidadVotacionesModel;
	
	private int votosPositivosModel;
	
	private int votosNegativosModel;
	
	private String observacionModel;

	public VotacionJuntaModel() {
		super();
	}

	public VotacionJuntaModel(int idVotacionModel, JuntaAccionistaModel juntaAccionistaModel,
			int cantidadVotacionesModel, int votosPositivosModel, int votosNegativosModel, String observacionModel) {
		super();
		this.idVotacionModel = idVotacionModel;
		this.juntaAccionistaModel = juntaAccionistaModel;
		this.cantidadVotacionesModel = cantidadVotacionesModel;
		this.votosPositivosModel = votosPositivosModel;
		this.votosNegativosModel = votosNegativosModel;
		this.observacionModel = observacionModel;
	}

	@Override
	public String toString() {
		return "VotacionJuntaModel [idVotacionModel=" + idVotacionModel + ", juntaAccionistaModel="
				+ juntaAccionistaModel + ", cantidadVotacionesModel=" + cantidadVotacionesModel
				+ ", votosPositivosModel=" + votosPositivosModel + ", votosNegativosModel=" + votosNegativosModel
				+ ", observacionModel=" + observacionModel + "]";
	}

	public int getIdVotacionModel() {
		return idVotacionModel;
	}

	public void setIdVotacionModel(int idVotacionModel) {
		this.idVotacionModel = idVotacionModel;
	}

	public JuntaAccionistaModel getJuntaAccionistaModel() {
		return juntaAccionistaModel;
	}

	public void setJuntaAccionistaModel(JuntaAccionistaModel juntaAccionistaModel) {
		this.juntaAccionistaModel = juntaAccionistaModel;
	}

	public int getCantidadVotacionesModel() {
		return cantidadVotacionesModel;
	}

	public void setCantidadVotacionesModel(int cantidadVotacionesModel) {
		this.cantidadVotacionesModel = cantidadVotacionesModel;
	}

	public int getVotosPositivosModel() {
		return votosPositivosModel;
	}

	public void setVotosPositivosModel(int votosPositivosModel) {
		this.votosPositivosModel = votosPositivosModel;
	}

	public int getVotosNegativosModel() {
		return votosNegativosModel;
	}

	public void setVotosNegativosModel(int votosNegativosModel) {
		this.votosNegativosModel = votosNegativosModel;
	}

	public String getObservacionModel() {
		return observacionModel;
	}

	public void setObservacionModel(String observacionModel) {
		this.observacionModel = observacionModel;
	}

	
	
}
