package com.accionistas.accionsis.model;

public class NacionalidadModel {

	private int idNacionalidadModel;
	private String nacionalidadModel;
	
	public NacionalidadModel() {
		super();
	}

	public NacionalidadModel(int idNacionalidadModel, String nacionalidadModel) {
		super();
		this.idNacionalidadModel = idNacionalidadModel;
		this.nacionalidadModel = nacionalidadModel;
	}

	@Override
	public String toString() {
		return "NacionalidadModel [idNacionalidadModel=" + idNacionalidadModel + ", nacionalidadModel="
				+ nacionalidadModel + "]";
	}

	public int getIdNacionalidadModel() {
		return idNacionalidadModel;
	}

	public void setIdNacionalidadModel(int idNacionalidadModel) {
		this.idNacionalidadModel = idNacionalidadModel;
	}

	public String getNacionalidadModel() {
		return nacionalidadModel;
	}

	public void setNacionalidadModel(String nacionalidadModel) {
		this.nacionalidadModel = nacionalidadModel;
	}

	

}
