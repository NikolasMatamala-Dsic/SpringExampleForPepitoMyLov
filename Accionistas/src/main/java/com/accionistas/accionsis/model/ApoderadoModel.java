package com.accionistas.accionsis.model;

/**
 * Class ApoderadoModel.
 */
public class ApoderadoModel {

	/** id. */
	private int idApoderadoModel;
	
	/** rut. */
	private String rutModel;
	
	/** nombre. */
	private String nombreModel;
	
	/** apellido paterno. */
	private String apellido_paternoModel;
	
	/** apellido materno. */
	private String apellido_maternoModel;
	
	/** mail. */
	private String EmailModel;
	
	/** fono. */
	private String fonoModel;
	
	private int estadoModel;
	
	private int tipoApoderadoModel;

	public ApoderadoModel() {
		super();
	}

	public ApoderadoModel(int idApoderadoModel, String rutModel, String nombreModel, String apellido_paternoModel,
			String apellido_maternoModel, String emailModel, String fonoModel, int estadoModel,
			int tipoApoderadoModel) {
		super();
		this.idApoderadoModel = idApoderadoModel;
		this.rutModel = rutModel;
		this.nombreModel = nombreModel;
		this.apellido_paternoModel = apellido_paternoModel;
		this.apellido_maternoModel = apellido_maternoModel;
		EmailModel = emailModel;
		this.fonoModel = fonoModel;
		this.estadoModel = estadoModel;
		this.tipoApoderadoModel = tipoApoderadoModel;
	}

	@Override
	public String toString() {
		return "ApoderadoModel [idApoderadoModel=" + idApoderadoModel + ", rutModel=" + rutModel + ", nombreModel="
				+ nombreModel + ", apellido_paternoModel=" + apellido_paternoModel + ", apellido_maternoModel="
				+ apellido_maternoModel + ", EmailModel=" + EmailModel + ", fonoModel=" + fonoModel + ", estadoModel="
				+ estadoModel + ", tipoApoderadoModel=" + tipoApoderadoModel + "]";
	}

	public int getIdApoderadoModel() {
		return idApoderadoModel;
	}

	public void setIdApoderadoModel(int idApoderadoModel) {
		this.idApoderadoModel = idApoderadoModel;
	}

	public String getRutModel() {
		return rutModel;
	}

	public void setRutModel(String rutModel) {
		this.rutModel = rutModel;
	}

	public String getNombreModel() {
		return nombreModel;
	}

	public void setNombreModel(String nombreModel) {
		this.nombreModel = nombreModel;
	}

	public String getApellido_paternoModel() {
		return apellido_paternoModel;
	}

	public void setApellido_paternoModel(String apellido_paternoModel) {
		this.apellido_paternoModel = apellido_paternoModel;
	}

	public String getApellido_maternoModel() {
		return apellido_maternoModel;
	}

	public void setApellido_maternoModel(String apellido_maternoModel) {
		this.apellido_maternoModel = apellido_maternoModel;
	}

	public String getEmailModel() {
		return EmailModel;
	}

	public void setEmailModel(String emailModel) {
		EmailModel = emailModel;
	}

	public String getFonoModel() {
		return fonoModel;
	}

	public void setFonoModel(String fonoModel) {
		this.fonoModel = fonoModel;
	}

	public int getEstadoModel() {
		return estadoModel;
	}

	public void setEstadoModel(int estadoModel) {
		this.estadoModel = estadoModel;
	}

	public int getTipoApoderadoModel() {
		return tipoApoderadoModel;
	}

	public void setTipoApoderadoModel(int tipoApoderadoModel) {
		this.tipoApoderadoModel = tipoApoderadoModel;
	}
	

	
	
}
