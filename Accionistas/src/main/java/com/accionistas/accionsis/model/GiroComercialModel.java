package com.accionistas.accionsis.model;

public class GiroComercialModel {
	
	private int idGiroComercialModel;
	private String descripcionModel;
	
	public GiroComercialModel() {
		super();
	}

	public GiroComercialModel(int idGiroComercialModel, String descripcionModel) {
		super();
		this.idGiroComercialModel = idGiroComercialModel;
		this.descripcionModel = descripcionModel;
	}

	@Override
	public String toString() {
		return "GiroComercialModel [idGiroComercialModel=" + idGiroComercialModel + ", descripcionModel="
				+ descripcionModel + "]";
	}

	public int getIdGiroComercialModel() {
		return idGiroComercialModel;
	}

	public void setIdGiroComercialModel(int idGiroComercialModel) {
		this.idGiroComercialModel = idGiroComercialModel;
	}

	public String getDescripcionModel() {
		return descripcionModel;
	}

	public void setDescripcionModel(String descripcionModel) {
		this.descripcionModel = descripcionModel;
	}
	
	

}
