package com.accionistas.accionsis.model;

/**
 * Clase UserCredential.
 */
public class UserCredential {

	/** username. */
	private String username;
	
	/** password. */
	private String password;

	/**
	 * Instanciando un nuevo user credential.
	 */
	public UserCredential() {

	}

	/**
	 * Instanciando un nuevo user credential.
	 * @param username 
	 * @param password 
	 */
	public UserCredential(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	/**
	 * Get username.
	 * @return username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Set username.
	 * @param username 
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Get password.
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Set password.
	 * @param password 
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Sobre-escribiendo metodo toString
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserCredential [username=" + username + ", password=" + password + "]";
	}
}
