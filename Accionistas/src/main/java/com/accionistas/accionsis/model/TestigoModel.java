package com.accionistas.accionsis.model;

import com.accionistas.accionsis.entity.Regiones;

/**
 * Clase TestigoModel.
 */
public class TestigoModel {

	/*id*/
	private int idTestigoModel;
	
	/** rut. */
	private String rutModel;
	
	/** nombre. */
	private String nombreModel;
	
	/** apellido paterno. */
	private String apellido_paternoModel;
	
	/** apellido materno. */
	private String apellido_maternoModel;
	
	/** mail. */
	private String EmailModel;
	
	/** fono. */
	private String fonoModel;
	
	
	/*Estado*/
	private int estado;


	public TestigoModel() {
		super();
	}


	public TestigoModel(int idTestigoModel, String rutModel, String nombreModel, String apellido_paternoModel,
			String apellido_maternoModel, String emailModel, String fonoModel, int estado) {
		super();
		this.idTestigoModel = idTestigoModel;
		this.rutModel = rutModel;
		this.nombreModel = nombreModel;
		this.apellido_paternoModel = apellido_paternoModel;
		this.apellido_maternoModel = apellido_maternoModel;
		EmailModel = emailModel;
		this.fonoModel = fonoModel;
		this.estado = estado;
	}


	@Override
	public String toString() {
		return "TestigoModel [idTestigoModel=" + idTestigoModel + ", rutModel=" + rutModel + ", nombreModel="
				+ nombreModel + ", apellido_paternoModel=" + apellido_paternoModel + ", apellido_maternoModel="
				+ apellido_maternoModel + ", EmailModel=" + EmailModel + ", fonoModel=" + fonoModel + ", estado="
				+ estado + "]";
	}


	public int getIdTestigoModel() {
		return idTestigoModel;
	}


	public void setIdTestigoModel(int idTestigoModel) {
		this.idTestigoModel = idTestigoModel;
	}


	public String getRutModel() {
		return rutModel;
	}


	public void setRutModel(String rutModel) {
		this.rutModel = rutModel;
	}


	public String getNombreModel() {
		return nombreModel;
	}


	public void setNombreModel(String nombreModel) {
		this.nombreModel = nombreModel;
	}


	public String getApellido_paternoModel() {
		return apellido_paternoModel;
	}


	public void setApellido_paternoModel(String apellido_paternoModel) {
		this.apellido_paternoModel = apellido_paternoModel;
	}


	public String getApellido_maternoModel() {
		return apellido_maternoModel;
	}


	public void setApellido_maternoModel(String apellido_maternoModel) {
		this.apellido_maternoModel = apellido_maternoModel;
	}


	public String getEmailModel() {
		return EmailModel;
	}


	public void setEmailModel(String emailModel) {
		EmailModel = emailModel;
	}


	public String getFonoModel() {
		return fonoModel;
	}


	public void setFonoModel(String fonoModel) {
		this.fonoModel = fonoModel;
	}


	public int getEstado() {
		return estado;
	}


	public void setEstado(int estado) {
		this.estado = estado;
	}

	
	
}
