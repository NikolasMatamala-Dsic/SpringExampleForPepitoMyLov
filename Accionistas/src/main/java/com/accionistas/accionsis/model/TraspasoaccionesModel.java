package com.accionistas.accionsis.model;

import java.sql.Date;

public class TraspasoaccionesModel {

	private int idTraspasoAccionesModel;
	private AccionistaModel rut_accionista_vendedorModel;
	private AccionistaModel rut_accionista_compradorModel;
	private Date fecha_ventaModel;
	private int cantidad_acciones_vendidas_aModel;
	private int cantidad_acciones_vendidas_bModel;
	private double montoModel;
	private String es_sucesionModel;
	private TestigoModel testigoModel;
	private String observacionModel;
	
	public TraspasoaccionesModel() {
		super();
	}

	public TraspasoaccionesModel(int idTraspasoAccionesModel, AccionistaModel rut_accionista_vendedorModel,
			AccionistaModel rut_accionista_compradorModel, Date fecha_ventaModel, int cantidad_acciones_vendidas_aModel,
			int cantidad_acciones_vendidas_bModel, double montoModel, String es_sucesionModel,
			TestigoModel testigoModel, String observacionModel) {
		super();
		this.idTraspasoAccionesModel = idTraspasoAccionesModel;
		this.rut_accionista_vendedorModel = rut_accionista_vendedorModel;
		this.rut_accionista_compradorModel = rut_accionista_compradorModel;
		this.fecha_ventaModel = fecha_ventaModel;
		this.cantidad_acciones_vendidas_aModel = cantidad_acciones_vendidas_aModel;
		this.cantidad_acciones_vendidas_bModel = cantidad_acciones_vendidas_bModel;
		this.montoModel = montoModel;
		this.es_sucesionModel = es_sucesionModel;
		this.testigoModel = testigoModel;
		this.observacionModel = observacionModel;
	}

	@Override
	public String toString() {
		return "TraspasoaccionesModel [idTraspasoAccionesModel=" + idTraspasoAccionesModel
				+ ", rut_accionista_vendedorModel=" + rut_accionista_vendedorModel + ", rut_accionista_compradorModel="
				+ rut_accionista_compradorModel + ", fecha_ventaModel=" + fecha_ventaModel
				+ ", cantidad_acciones_vendidas_aModel=" + cantidad_acciones_vendidas_aModel
				+ ", cantidad_acciones_vendidas_bModel=" + cantidad_acciones_vendidas_bModel + ", montoModel="
				+ montoModel + ", es_sucesionModel=" + es_sucesionModel + ", testigoModel=" + testigoModel
				+ ", observacionModel=" + observacionModel + "]";
	}

	public int getIdTraspasoAccionesModel() {
		return idTraspasoAccionesModel;
	}

	public void setIdTraspasoAccionesModel(int idTraspasoAccionesModel) {
		this.idTraspasoAccionesModel = idTraspasoAccionesModel;
	}

	public AccionistaModel getRut_accionista_vendedorModel() {
		return rut_accionista_vendedorModel;
	}

	public void setRut_accionista_vendedorModel(AccionistaModel rut_accionista_vendedorModel) {
		this.rut_accionista_vendedorModel = rut_accionista_vendedorModel;
	}

	public AccionistaModel getRut_accionista_compradorModel() {
		return rut_accionista_compradorModel;
	}

	public void setRut_accionista_compradorModel(AccionistaModel rut_accionista_compradorModel) {
		this.rut_accionista_compradorModel = rut_accionista_compradorModel;
	}

	public Date getFecha_ventaModel() {
		return fecha_ventaModel;
	}

	public void setFecha_ventaModel(Date fecha_ventaModel) {
		this.fecha_ventaModel = fecha_ventaModel;
	}

	public int getCantidad_acciones_vendidas_aModel() {
		return cantidad_acciones_vendidas_aModel;
	}

	public void setCantidad_acciones_vendidas_aModel(int cantidad_acciones_vendidas_aModel) {
		this.cantidad_acciones_vendidas_aModel = cantidad_acciones_vendidas_aModel;
	}

	public int getCantidad_acciones_vendidas_bModel() {
		return cantidad_acciones_vendidas_bModel;
	}

	public void setCantidad_acciones_vendidas_bModel(int cantidad_acciones_vendidas_bModel) {
		this.cantidad_acciones_vendidas_bModel = cantidad_acciones_vendidas_bModel;
	}

	public double getMontoModel() {
		return montoModel;
	}

	public void setMontoModel(double montoModel) {
		this.montoModel = montoModel;
	}

	public String getEs_sucesionModel() {
		return es_sucesionModel;
	}

	public void setEs_sucesionModel(String es_sucesionModel) {
		this.es_sucesionModel = es_sucesionModel;
	}

	public TestigoModel getTestigoModel() {
		return testigoModel;
	}

	public void setTestigoModel(TestigoModel testigoModel) {
		this.testigoModel = testigoModel;
	}

	public String getObservacionModel() {
		return observacionModel;
	}

	public void setObservacionModel(String observacionModel) {
		this.observacionModel = observacionModel;
	}
	
	
}
