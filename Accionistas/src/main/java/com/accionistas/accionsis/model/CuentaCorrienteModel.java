package com.accionistas.accionsis.model;

import com.accionistas.accionsis.entity.Accionista;

public class CuentaCorrienteModel {
	
	private int idCuentaCorrienteModel;
	
	private int numeroCuentaModel;
	
	private String nombreBancoModel;
	
	private Accionista accionistaModel;

	public CuentaCorrienteModel() {
		super();
	}

	public CuentaCorrienteModel(int idCuentaCorrienteModel, int numeroCuentaModel, String nombreBancoModel,
			Accionista accionistaModel) {
		super();
		this.idCuentaCorrienteModel = idCuentaCorrienteModel;
		this.numeroCuentaModel = numeroCuentaModel;
		this.nombreBancoModel = nombreBancoModel;
		this.accionistaModel = accionistaModel;
	}

	@Override
	public String toString() {
		return "CuentaCorrienteModel [idCuentaCorrienteModel=" + idCuentaCorrienteModel + ", numeroCuentaModel="
				+ numeroCuentaModel + ", nombreBancoModel=" + nombreBancoModel + ", accionistaModel=" + accionistaModel
				+ "]";
	}

	public int getIdCuentaCorrienteModel() {
		return idCuentaCorrienteModel;
	}

	public void setIdCuentaCorrienteModel(int idCuentaCorrienteModel) {
		this.idCuentaCorrienteModel = idCuentaCorrienteModel;
	}

	public int getNumeroCuentaModel() {
		return numeroCuentaModel;
	}

	public void setNumeroCuentaModel(int numeroCuentaModel) {
		this.numeroCuentaModel = numeroCuentaModel;
	}

	public String getNombreBancoModel() {
		return nombreBancoModel;
	}

	public void setNombreBancoModel(String nombreBancoModel) {
		this.nombreBancoModel = nombreBancoModel;
	}

	public Accionista getAccionistaModel() {
		return accionistaModel;
	}

	public void setAccionistaModel(Accionista accionistaModel) {
		this.accionistaModel = accionistaModel;
	}

	
}
