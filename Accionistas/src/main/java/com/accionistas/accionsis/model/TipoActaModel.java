package com.accionistas.accionsis.model;

public class TipoActaModel {

	private int idTipoActa;
	
	private String descripcion;

	public TipoActaModel() {
		super();
	}

	public TipoActaModel(int idTipoActa, String descripcion) {
		super();
		this.idTipoActa = idTipoActa;
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "TipoActaModel [idTipoActa=" + idTipoActa + ", descripcion=" + descripcion + "]";
	}

	public int getIdTipoActa() {
		return idTipoActa;
	}

	public void setIdTipoActa(int idTipoActa) {
		this.idTipoActa = idTipoActa;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
