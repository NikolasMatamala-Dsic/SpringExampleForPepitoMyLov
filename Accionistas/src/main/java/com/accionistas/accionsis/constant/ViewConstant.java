package com.accionistas.accionsis.constant;

/**
 * Clase ViewConstant.
 * Se almacenan las vistas en constantes.
 */
public class ViewConstant {
	
	/**
	 * Se declaran las constantes de vistas
	**/
	public static final String ISE_VIEW = "error/500";
	public static final String LOGIN_FORM = "index";
	public static final String LOGIN_FORM_ERROR = "index";
	public static final String PRINCIPAL_VIEW = "principal";
	public static final String USUARIOS_VIEW = "usuarios";
	public static final String USUARIOS_FORM ="adduserform";
	public static final String ACCIONISTAS_VIEW = "accionistas";
	public static final String ACCIONISTAS_LIST = "listadoaccionistas";
	public static final String ACCIONISTAS_FORM = "addaccionistasform";
	public static final String APODERADOS_VIEW = "apoderados";
	public static final String APODERADOS_LIST = "admin_apoderados";
	public static final String TESTIGOS_VIEW = "testigos";
	public static final String TESTIGOS_LIST = "testigoAdmin";
	public static final String MANDATARIOS_VIEW = "mandatarios";
	public static final String ACCIONES_TRASPASO = "traspasoacciones";
	public static final String HISTORIAL_TRASPASO = "historialtraspasoacciones";
	public static final String GRAFICOS_VIEW = "graficos";
	public static final String JUNTA_ACCIONISTAS_VIEW = "listjuntaaccionista";
	public static final String JUNTA_ACCIONISTAS_FORM = "juntaaccionistasform";
	public static final String JUNTA_ACCIONISTAS_ADMIN = "juntaAccionistaAdmin";
	public static final String EMPRESA_ACTUALIZAR_DATOS = "empresa_editform";
	public static final String ACCIONES_VIEW = "acciones";
	public static final String ACCIONES_FORM = "addaccionform";
	public static final String EMPRESA_FORM = "empresaform";
	public static final String APODERADO_FORM = "addapoderadoform";
	public static final String TESTIGO_FORM = "addtestigoform";
	public static final String HEREDERO_FORM = "addherederoform";
	public static final String HEREDERO_VIEW = "herederos";
	public static final String MANDATARIO_VIEW = "mandatarios";
	public static final String MANDATARIO_FORM = "addmandatariosform";
	public static final String REPRESENTANTE_VIEW = "representantes";
	public static final String REPRESENTANTE_FORM = "addrepresentanteform";
	public static final String DIVIDENDO_VIEW = "verdividendo";
	public static final String DIVIDENDO_FORM = "adddividendoform";
	public static final String DIVIDENDO_REPORTE = "reporteDividendo";
	public static final String CALCULARDIVIDENDO_VIEW = "calcularDividendo";
	public static final String CALCULARDIVIDENDO_FORM = "addcalculardividendoform";
	public static final String TRASPASOACCIONES_VIEW = "traspasoAccionesAdmin";
	public static final String TRASPASOACCIONES_FROM = "addTranspasoAccionesAdmin";
	public static final String SVS_VIEW = "reporteSVS";
	public static final String ACTA_VIEW = "addActa";
	public static final String ACTA_LIST = "actaAdmin";
	public static final String ACTA_FORM = "actaForm";
	

}
