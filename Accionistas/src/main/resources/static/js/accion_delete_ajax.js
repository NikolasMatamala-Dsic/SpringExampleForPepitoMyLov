$(document).ready(function (){
	$(".eliminar").on('click',function(){
		 var dato = $(this).attr('id');
		 var data = {id:dato};
		 $.confirm({
			    title: 'Accion',
			    content: '¿Desea eliminar el accion con id: '+dato+' ?',
			    type: 'dark',
			    typeAnimated: true,
			    autoClose: 'cancelar|8000',
			    buttons: {
			    	deleteUser: {
			    		text: 'Eliminar',
			    		btnClass: 'btn-red',					    		
			            action: function () {
			            	delete_data(data);
			            }
			        },					        
			        cancelar: function () {
			        	
			        }
			    }
		});
		 		 		 		
	});
	function delete_data(data){
		 $.ajax({
			 url: "/accion/del",
			 type: "GET",
			 data: data,
			 beforeSend:  function(){
				 
				 
			 },
			 success: function(resp){				 				 				 
				location.reload();
				
			},
			error: function(jqXHR, estado, error){
				console.log(error);				
			},
			complete: function(jqXHR, estado){
				console.log(estado);				
			},
			timeout: 10000			 
		 });
	 }
});