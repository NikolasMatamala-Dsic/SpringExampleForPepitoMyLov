function alerta (){	
	var accionistaVendedor = $('#rut_accionista_vendedorModel').val();
	var accionistaComprador = $('#rut_accionista_compradorModel').val();
	var fecha = $('#fecha_ventaModel').val();
	var cantidadAccionesVendidasA = $('#cantidad_acciones_vendidas_aModel').val();
	var cantidadAccionesVendidasB = $('#cantidad_acciones_vendidas_bModel').val();
	var sucesionDeAcciones = $('#es_sucesionModel').val();
	var testigo = $('#testigoModel').val();
	var observacion = $('#observacionModel').val();
	
	var data = {rut_accionista_vendedorModel: accionistaVendedor, rut_accionista_compradorModel: accionistaComprador, fecha_ventaModel: fecha,
			cantidad_acciones_vendidas_aModel:cantidadAccionesVendidasA, 
			cantidad_acciones_vendidas_bModel: cantidadAccionesVendidasB, 
			es_sucesionModel: sucesionDeAcciones, testigoModel: testigo, observacionModel: observacion};
			
	//console.log(data);
	
	$.get("/accionistas/traspasoacciones2", data, function(e){
		//console.log(e);
		switch (e){
		case 1: 
			iziToast.error({
			    title: 'Error',
			    message: 'No se pudo realizar venta de acciones por que el accionista comprador y vendedor son la misma persona',
			});
			break;
		case 2:
			iziToast.warning({
			    title: 'Precaucion',
			    message: 'No se pudo realizar venta de acciones debido a que accionista vendedor no posee la cantidad de acciones serie A o serie B ingresadas por sistema'
			});
			break;
		case 3: 
			iziToast.success({
			    title: 'Exito',
			    message: 'Traspaso de acciones realizado correctamente'
			});
			break;
		case 4: 
			iziToast.warning({
			    title: 'Precausion',
			    message: 'El traspaso no se pudo completar devido a la fecha ingresada, comprueba su estado y vuelve a intentar'
			});
			break;
		case 5: 
			iziToast.warning({
			    title: 'Precausion',
			    message: 'Debe asignar valor a la cantidad de accionies que desea vender si es para tipo A o tipoB'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se realizo traspaso de acciones'
			});
			break;
		}			
	});
}

function alertaUpdate(){	
	
	var fecha = $('#fecha_ventaModel').val();
	var sucesionDeAcciones = $('#es_sucesionModel').val();	
	var observacion = $('#observacionModel').val();
	var idTraspasoAccionesModel = $('#idTraspasoAccionesModel').val();
	
	var data = {idTraspasoAccionesModel: idTraspasoAccionesModel, fecha_ventaModel: fecha, es_sucesionModel: sucesionDeAcciones, observacionModel: observacion};
			
	console.log(data);
	
	$.get("/traspasoAcciones/update", data, function(e){
		console.log(e);
		switch (e){
		case 1: 
			iziToast.error({
			    title: 'Error',
			    message: 'No se pudo realizar venta de acciones por que el accionista comprador y vendedor son la misma persona',
			});
			break;
		case 2:
			iziToast.warning({
			    title: 'Precaucion',
			    message: 'No se pudo realizar venta de acciones debido a que accionista vendedor no posee la cantidad de acciones serie A o serie B ingresadas por sistema'
			});
			break;
		case 3: 
			iziToast.success({
			    title: 'Exito',
			    message: 'Traspaso de acciones realizado correctamente'
			});
			break;
		case 4: 
			iziToast.warning({
			    title: 'Precausion',
			    message: 'El traspaso no se pudo completar devido a la fecha ingresada, comprueba su estado y vuelve a intentar'
			});
			break;
		case 5: 
			iziToast.warning({
			    title: 'Precausion',
			    message: 'Debe asignar valor a la cantidad de accionies que desea vender si es para tipo A o tipoB'
			});
			break;
		case 6: 
			iziToast.warning({
			    title: 'Precausion',
			    message: 'No se pudo completar la actualización'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se realizo traspaso de acciones'
			});
			break;
		}			
	});
}

function alertaAgregarAccionista(){	
		
		var id = $('#idAccionistaModel').val();
		var empresa = $(document.getElementById("empresaModel.idEmpresaModel")).val();
		var nombre = $('#nombreModel').val();
		var rut = $('#rutModel').val();	
		var apellidoP = $('#apellido_paternoModel').val();
		var apellidoM = $('#apellido_maternoModel').val();
		var accionA = $('#cantidad_acciones_serie_aModel').val();
		var accionB = $('#cantidad_acciones_serie_bModel').val();
		var fechaN = $('#fecha_nacimientoModel').val();
		var fechaI = $('#fecha_ingresoModel').val();
		var apoderado = $(document.getElementById("apoderadoModel.idApoderadoModel")).val();
		var habilitado = $('#esValidoModel').val();
		var email = $('#emailModel').val();
		var direccion = $('#direccionModel').val();
		var nacionalidad = $(document.getElementById("nacionalidadModel.idNacionalidadModel")).val();
		var tipoAccionista = $(document.getElementById("tipoAccionistaModel.idTipoAccionistaModel")).val();
		var fono = $('#fonoModel').val();
		var ciudad = $('#ciudadModel').val();
		var comuna = $('#comunaModel').val();
		var region = $(document.getElementById("regionModel.idRegionModel")).val();
		
		
		var data = {id: id, empresaModel: empresa, nombreModel: nombre, rutModel: rut, apellido_paternoModel: apellidoP, apellido_maternoModel: apellidoM,
				cantidad_acciones_serie_aModel: accionA, cantidad_acciones_serie_bModel: accionB, fecha_nacimientoModel: fechaN,
				fecha_ingresoModel: fechaI, apoderadoModel: apoderado, esValidoModel: habilitado, EmailModel: email, direccionModel: direccion, nacionalidadModel: nacionalidad,
				tipoAccionistaModel: tipoAccionista, fonoModel: fono, ciudadModel: ciudad, comunaModel: comuna , regionModel: region};
				
		console.log(data);
		
		$.get("/accionistas/addaccionista", data, function(e){
			console.log(e);
			switch (e){
			case "1": 
				iziToast.warning({
				    title: 'Precaucion',
				    message: 'Excede la cantidad de acciones tipo A',
				});
				break;
			case "2":
				iziToast.warning({
				    title: 'Precaucion',
				    message: 'Excede la cantidad de acciones tipo B'
				});
				break;
			case "3": 
				iziToast.success({
				    title: 'Exito',
				    message: 'Se agrego un accionista correctamente'
				});
				break;
			case "4": 
				iziToast.warning({
				    title: 'Precaucion',
				    message: 'Error al ingresar fecha'
				});
				break;
			default:
				iziToast.error({
				    title: 'Error',
				    message: 'No se pudo agregar un accionista, debe rellenar todos los campos!'
				});
				break;
			}			
		});
	
}

function alertaAgregarApoderado(){	

	var id = $('#idApoderadoModel').val();
	var rut = $('#rutModel').val();
	var nombre = $('#nombreModel').val();
	var apellidoP = $('#apellido_paternoModel').val();
	var apellidoM = $('#apellido_maternoModel').val();
	var email = $('#emailModel').val();
	var fono = $('#fonoModel').val();
	
	var data = {id: id, nombreModel: nombre, rutModel: rut, apellido_paternoModel: apellidoP, apellido_maternoModel: apellidoM,
			emailModel: email, fonoModel: fono};
			
	console.log(data);
	
	$.get("/apoderados/addapoderado", data, function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Se agrego un apoderado correctamente'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se pudo agregar un apoderado, debe rellenar todos los campos!'
			});
			break;
		}			
	});

}

function alertaAgregarTestigo(){	
	
	var id = $('#idTestigoModel').val();
	var rut = $('#rutModel').val();
	var nombre = $('#nombreModel').val();
	var apellidoP = $('#apellido_paternoModel').val();
	var apellidoM = $('#apellido_maternoModel').val();
	var email = $('#emailModel').val();
	var fono = $('#fonoModel').val();
	
	var data = {id: id, nombreModel: nombre, rutModel: rut, apellido_paternoModel: apellidoP, apellido_maternoModel: apellidoM,
			emailModel: email, fonoModel: fono};
			
	console.log(data);
	
	$.get("/testigos/addtestigo", data, function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Se agrego un testigo correctamente'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se pudo agregar un testigo, debe rellenar todos los campos!'
			});
			break;
		}			
	});

}

function alertaAgregarHeredero(){	
	
	var id = $('#idHerederoModel').val();
	var rut = $('#rutModel').val();
	var empresa = $(document.getElementById("empresaModel.idEmpresaModel")).val();
	var nombre = $('#nombreModel').val();
	var apellidoP = $('#apellido_paternoModel').val();
	var apellidoM = $('#apellido_maternoModel').val();
	var accionA = $('#cantidad_acciones_serie_aModel').val();
	var accionB = $('#cantidad_acciones_serie_bModel').val();
	var fechaN = $('#fecha_nacimientoModel').val();
	var esValido = $('#es_validoModel').val();
	var direccion = $('#direccionModel').val();
	var email = $('#emailModel').val();
	var fono = $('#fonoModel').val();
	var ciudad = $('#ciudadModel').val();
	var comuna = $('#comunaModel').val();
	var region = $(document.getElementById("regionesModel.idRegionModel")).val();
	var nacionalidad = $(document.getElementById("nacionalidadModel.idNacionalidadModel")).val();
	var tipoAccionista = $(document.getElementById("tipoAccionistaModel.idTipoAccionistaModel")).val();
	var accionistaAHeredar = $(document.getElementById("accionistaModel.idAccionistaModel")).val();
	
	var data = {id: id, nombreModel: nombre, rutModel: rut, empresaModel: empresa, apellido_paternoModel: apellidoP, apellido_maternoModel: apellidoM,
			cantidad_acciones_serie_aModel: accionA,  cantidad_acciones_serie_bModel: accionB, fecha_nacimientoModel: fechaN, es_validoModel: esValido, 
			direccionModel: direccion, emailModel: email, fonoModel: fono, ciudadModel: ciudad,	comunaModel: comuna, regionesModel: region, 
			nacionalidadModel: nacionalidad, tipoAccionistaModel: tipoAccionista, accionistaModel: accionistaAHeredar};
			
	console.log(data);
	
	$.get("/heredero/addheredero", data, function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Se agrego un heredero correctamente'
			});
			break;
		case "2": 
			iziToast.warning({
			    title: 'Advertencia',
			    message: 'Rellene todos los campos'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se pudo agregar un heredero'
			});
			break;
		}			
	});

}

function alertaAgregarRepresentante(){	
	
	var id = $('#idRepresentanteModel').val();
	var rut = $('#rutModel').val();
	var empresa = $('#empresaList').val();
	var nombre = $('#nombreModel').val();
	var apellidoP = $('#apellido_paternoModel').val();
	var apellidoM = $('#apellido_maternoModel').val();
	var direccion = $('#direccionModel').val();
	var email = $('#emailModel').val();
	var fono = $('#telefonoModel').val();
	var region = $(document.getElementById("regionesModel.idRegionModel")).val();
	
	var data = {id: id, nombreModel: nombre, rutModel: rut, empresaModel: empresa, apellido_paternoModel: apellidoP, apellido_maternoModel: apellidoM,
			direccionModel: direccion, emailModel: email, fonoModel: fono, regionesModel: region};
			
	console.log(data);
	
	$.get("/representantes/addrepresentante", data, function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Se agrego un representante correctamente'
			});
			break;
		case "3": 
			iziToast.warning({
			    title: 'Advertencia',
			    message: 'Rellene todos los campos'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se pudo agregar un representante'
			});
			break;
		}			
	});

}

function alertaAgregarJunta(){	
	
	var id = $('#idJuntaAccionistaModel').val();
	var ubicacion = $('#ubicacionModel').val();
	var fechaR = $('#fecha_realizacionModel').val();
	var horaI = $('#hora_inicioModel').val();
	var horaT = $('#hora_terminoModel').val();
	var direccion = $('#direccionModel').val();
	var fechaD = $('#fecha_difusionModel').val();
	
	var data = {id: id, ubicacionModel: ubicacion, fecha_realizacionModel: fechaR, hora_inicioModel: horaI, hora_terminoModel: horaT, direccionModel: direccion,
			fecha_difusionModel: fechaD};
			
	console.log(data);
	
	$.get("/juntaaccionista/addjuntaaccionista", data, function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Se agrego una junta correctamente'
			});
			break;
		case "3": 
			iziToast.warning({
			    title: 'Advertencia',
			    message: 'Rellene todos los campos'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se pudo agregar una junta'
			});
			break;
		}			
	});

}

function alertaAgregarAcciones(){	
	
	var id = $('#idJuntaAccionistaModel').val();
	var ubicacion = $('#ubicacionModel').val();
	var fechaR = $('#fecha_realizacionModel').val();
	var horaI = $('#hora_inicioModel').val();
	var horaT = $('#hora_terminoModel').val();
	var direccion = $('#direccionModel').val();
	var fechaD = $('#fecha_difusionModel').val();
	
	var data = {id: id, ubicacionModel: ubicacion, fecha_realizacionModel: fechaR, hora_inicioModel: horaI, hora_terminoModel: horaT, direccionModel: direccion,
			fecha_difusionModel: fechaD};
			
	console.log(data);
	
	$.get("/juntaaccionista/addjuntaaccionista", data, function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Se agrego una junta correctamente'
			});
			break;
		case "3": 
			iziToast.warning({
			    title: 'Advertencia',
			    message: 'Rellene todos los campos'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se pudo agregar una junta'
			});
			break;
		}			
	});

}