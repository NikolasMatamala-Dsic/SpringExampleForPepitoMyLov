var juntaActual = 0;
function actaBuscaDatos(junta){	
	event.preventDefault();	
	var idJunta = junta.options[junta.selectedIndex].value;
	var data ={idJunta: idJunta};
	if(idJunta == 0){
		$('#contadorA').val("");
		$('#contadorB').val("");
		$('#fechai').val("");
		$('#fechat').val("");
		$('#empresaSerieA').val("");
		$('#empresaSerieB').val("");
		juntaActual = 0;
		return false;
	}
	if(idJunta == juntaActual){
		return false;
	}
	 $.get("/acta/getActaValues", data, function(e) {		 
		var respuesta = jQuery.parseJSON(e);		
		juntaActual = idJunta;
		$('#contadorA').val(respuesta[0]);
		$('#contadorB').val(respuesta[1]);
		$('#fechai').val(respuesta[2]);
		$('#fechat').val(respuesta[3]);
		$('#empresaSerieA').val(respuesta[4]);
		$('#empresaSerieB').val(respuesta[5]);		
	});
		
		
}

