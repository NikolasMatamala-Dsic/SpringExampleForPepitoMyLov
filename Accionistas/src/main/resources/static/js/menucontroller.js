$(document).ready(function () {
	
	var isClosed = true;	
	if($( window ).width() < 990){
		$("#estilos").attr("href", "/css/movil_style.css");		
		var init_state = $('#wrapper').attr("class");

		if(init_state == "toggled"){
			$('#wrapper').removeClass('toggled');		

		}else{

		}
				
	}else{
		$("#estilos").attr("href", "/css/normal_style.css");		
		if(init_state != "toggled"){
			$('#wrapper').toggleClass('toggled');		
		}		
	}

	$(window).resize(function() {
	  	var state = $('#wrapper').attr("class");
	   		console.log(state);
		if($( window ).width() < 990){
			$("#estilos").attr("href", "/css/movil_style.css");		
			if(state == "toggled"){
	  			$('#wrapper').removeClass('toggled');		
	  		}						
		}else{
			$("#estilos").attr("href", "/css/normal_style.css");		
			if(state != "toggled"){
				$('#wrapper').toggleClass('toggled');		
			}		
	}
	});
    $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
	});  
    
    $('.dropdown').on('show.bs.dropdown', function(e){
    	  $(this).find('.dropdown-menu').first().stop(true, true).slideDown(500);
    	  $("#sidebar-wrapper").animate({scrollTop: $('ul.sidebar-nav li:last').offset().top+150});
    	});

    	// Add slideUp animation to dropdown
    	$('.dropdown').on('hide.bs.dropdown', function(e){
    	  $(this).find('.dropdown-menu').first().stop(true, true).slideUp(500);
    	});
    
});